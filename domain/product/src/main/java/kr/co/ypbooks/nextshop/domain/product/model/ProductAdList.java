package kr.co.ypbooks.nextshop.domain.product.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Entity
public class ProductAdList {
    /**
     * 상품 고유 Id
     */
    @EmbeddedId
    private ProductId productId;

    /**
     * 광고 시작 일시
     */
    private LocalDateTime startDate;

    /**
     * 광고 종료 일시
     */
    private LocalDateTime endDate;

    /**
     * 설명
     */
    private String desc;
}
