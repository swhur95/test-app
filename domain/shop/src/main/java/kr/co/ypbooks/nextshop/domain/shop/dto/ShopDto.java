package kr.co.ypbooks.nextshop.domain.shop.dto;

import kr.co.ypbooks.nextshop.domain.shop.model.Shop;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder
public class ShopDto {
    private Long id;
    private String name;
    private String address;

    //page
    private int pageNum;
    private int pageSize;
    private Long totalSize;

    private List<Shop> dataList;
    private Shop shop;

}
