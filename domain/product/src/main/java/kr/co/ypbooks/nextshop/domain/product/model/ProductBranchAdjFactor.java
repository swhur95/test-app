package kr.co.ypbooks.nextshop.domain.product.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
@Entity
public class ProductBranchAdjFactor {
    /**
     * 상품 Id
     */
    @EmbeddedId
    private ProductBranchAdjFactorId productBranchAdjFactorId;

    /**
     * 상품 식별 코드
     */
    private String productIdCd;

    /**
     * 상품명
     */
    private String productName;

    /**
     * 대분류 (카테고리 코드)
     */
    private String categoryCd1;

    /**
     * 계수/요인
     */
    private Integer factor;

    /**
     * 등록 일시
     */
    private LocalDateTime regDate;

}
