package kr.co.ypbooks.nextshop.domain.order.repository;

import kr.co.ypbooks.nextshop.domain.order.model.Consult;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ConsultRepository extends JpaRepository<Consult, Long> {
}
