package kr.co.ypbooks.nextshop.lib.common.jpa.common.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.stereotype.Component;

/**
 * general Repository
 *
 * @param <E> model
 * @param <T> id primary key type
 */
@NoRepositoryBean
@Component
public interface CommonRepository<E,T> extends JpaRepository<E,T>, JpaSpecificationExecutor<E> {

}
