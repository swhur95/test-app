package kr.co.ypbooks.nextshop.domain.account.repository;

import kr.co.ypbooks.nextshop.domain.account.model.Member;
import kr.co.ypbooks.nextshop.lib.common.jpa.common.repository.CommonRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MemberRepository extends CommonRepository<Member, String> {

}
