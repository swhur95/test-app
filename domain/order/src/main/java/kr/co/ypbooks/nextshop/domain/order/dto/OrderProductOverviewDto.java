package kr.co.ypbooks.nextshop.domain.order.dto;

public class OrderProductOverviewDto {
    /**
     * 주문 상품 코드
     */
    private Long orderProductCd;

    /**
     * 주문 상태
     */
    private String orderStateCd;

    /**
     * 처리 (처리 완료 )
     */

    /**
     * 상품 유형
     */
    private String productTypeCd;

    /**
     * 상품 코드
     */
    private String productCd;

    /**
     * 상품명
     */
    private String productName;

    /**
     * 재고 수량
     */
    private Integer stockQuantity;

    /**
     * 정가
     */
    private Long normalAmt;

    /**
     * 수량
     */
    private Integer quantity;

    /**
     * 주문 금액
     */
    private Long orderAmt;

    /**
     * 총 할인율
     */
    private Byte sumDiscRate;

    /**
     * 총 할인 금액
     */
    private Long sumDiscAmt;

    /**
     * 판매 금액
     */
    private Long saleAmt;

    /**
     * 공급 금액 (결제 금액 - 부가세 금액)
     */
    private Long supplyAmt;

    /**
     * 부가세 금액
     */
    private Long vatAmt;

    /**
     * 적립금
     */
    private Long reserveAmt;

    /**
     * 배송 조회 (운송장 번호)
     */
    private String delivNo;






}
