package kr.co.ypbooks.nextshop.domain.account.model.adminlog;

import io.swagger.annotations.ApiModel;
import lombok.*;

/**
 * 사용자권한설정 로그
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@ApiModel(value = "로그 타입 (6: 사용자 권한 설정)")
public class AdminActionHistAuthLogInfo {
    /**
     * 아이디
     */
    private String memberId;
    /**
     * 대메뉴
     */
    private String menuCat1;
    /**
     * 중메뉴
     */
    private String menuCat2;
    /**
     * 소메뉴
     */
    private String menuName;
    /**
     * 소메뉴 id
     */
    private Integer menuId;
    /**
     * 권한 액션
     */
    private String authAction;
}
