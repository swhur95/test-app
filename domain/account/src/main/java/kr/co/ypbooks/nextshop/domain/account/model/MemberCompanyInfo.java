package kr.co.ypbooks.nextshop.domain.account.model;

import lombok.*;

/**
 * COMPANY_INFO(회사 정보)
 */

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class MemberCompanyInfo {

    /**
     * 직장명
     */
    private String companyName;
    /**
     * 부서명
     */
    private String companyPostName;
    /**
     * 직장전화
     */
    private String companyPhone;
    /**
     * 직장팩스
     */
    private String companyFax;
    /**
     * 직장우편번호
     */
    private String companyPostNo;
    /**
     * 직장주소1
     */
    private String companyAddress1;
    /**
     * 직장주소2
     */
    private String companyAddress2;
}
