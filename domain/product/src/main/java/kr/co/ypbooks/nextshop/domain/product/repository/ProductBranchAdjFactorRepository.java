package kr.co.ypbooks.nextshop.domain.product.repository;

import kr.co.ypbooks.nextshop.domain.product.model.ProductBranchAdjFactor;
import kr.co.ypbooks.nextshop.domain.product.model.ProductBranchAdjFactorId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductBranchAdjFactorRepository extends JpaRepository<ProductBranchAdjFactor, ProductBranchAdjFactorId> {
}
