package kr.co.ypbooks.nextshop.domain.shop.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
@Entity
public class BranchDayOff {
    /**
     * 지점 휴무 고유 번호
     */
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long dayOffNo;

    /**
     * 지점 코드
     */
    private String branchCd;

    /**
     * 휴무일
     */
    private LocalDate dayOffDate;

    /**
     * 지점명
     */
    private String branchName;

    /**
     * 휴무 사유
     */
    private String dayOffReason;

    /**
     * 요일
     */
    private Byte dayOfWeek;

    /**
     * 등록한 관리자 Id
     */
    private String regAdminId;

    /**
     * 등록한 관리자명
     */
    private String regAdminName;

    /**
     * 등록 일시
     */
    private LocalDateTime regDate;

    /**
     * 수정 일시
     */
    private LocalDateTime updDate;
}
