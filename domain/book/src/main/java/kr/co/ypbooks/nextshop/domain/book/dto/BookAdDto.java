package kr.co.ypbooks.nextshop.domain.book.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.Getter;

import java.time.LocalDateTime;

@ApiModel
@Getter
@Builder
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class BookAdDto {
    /**
     * 도서 코드
     */
    private String bookCd;

    /**
     * 시작 일시
     */
    private LocalDateTime startDate;

    /**
     * 종료 일시
     */
    private LocalDateTime endDate;

    /**
     * 설명
     */
    private String desc;

    /**
     * 등록 일시
     */
    private LocalDateTime regDate;
}
