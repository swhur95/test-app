package kr.co.ypbooks.nextshop.app.shop.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import kr.co.ypbooks.nextshop.domain.shop.dto.ShopDto;
import kr.co.ypbooks.nextshop.domain.shop.model.Shop;
import kr.co.ypbooks.nextshop.domain.shop.service.ShopService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@Api(tags = "ShopCore")
@RequestMapping("/shopCore")
@RestController
public class ShopController {
    private final ShopService shopService;

    public ShopController(ShopService shopService) {
        this.shopService = shopService;
    }

    @ApiOperation(value = "add", notes = "addShop")
    @PostMapping("/add")
    public ShopDto addAccount(@RequestBody ShopDto param) {
        return shopService.addShop(param);
    }

    @ApiOperation(value = "find", notes = "findShop")
    @ApiImplicitParam(name = "id", paramType = "query", required = true, dataType = "Long", example = "1")
    @GetMapping(value = "/find")
    public ShopDto list(Long id) {
        Shop shop = shopService.selectById(id);
        ShopDto dto = ShopDto.builder().shop(shop).build();
        return dto;
    }

    @ApiOperation(value = "List", notes = "shopList")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "name", paramType = "query", required = true, dataType = "String"),
            @ApiImplicitParam(name = "pageNum", paramType = "query", required = true, dataType = "Integer", example = "1"),
            @ApiImplicitParam(name = "pageSize", paramType = "query", required = true, dataType = "Integer", example = "10")
    })
    @GetMapping(value = "/list")
    public ShopDto list(String name, Integer pageNum, Integer pageSize) {
        ShopDto param = ShopDto.builder().name(name).build();
        Page<Shop> page = shopService.findShopsByPage(pageNum, pageSize, param);
        List<Shop> list = page.getContent();

        Long totalSize = page.getTotalElements();
        ShopDto dto = ShopDto.builder().dataList(list).totalSize(totalSize).pageNum(pageNum).pageSize(pageSize).build();
        return dto;
    }
}
