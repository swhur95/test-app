package kr.co.ypbooks.nextshop.domain.account.repository;

import kr.co.ypbooks.nextshop.domain.account.model.adminlog.AdminActionHist;
import kr.co.ypbooks.nextshop.lib.common.jpa.common.repository.CommonRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdminActionHistRepository extends CommonRepository<AdminActionHist, Integer> {
}
