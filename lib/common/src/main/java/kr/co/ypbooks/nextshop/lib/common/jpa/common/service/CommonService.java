package kr.co.ypbooks.nextshop.lib.common.jpa.common.service;


import kr.co.ypbooks.nextshop.lib.common.jpa.common.pojo.PageCondition;
import kr.co.ypbooks.nextshop.lib.common.jpa.common.pojo.PageInfo;
import kr.co.ypbooks.nextshop.lib.common.jpa.common.pojo.Result;
import kr.co.ypbooks.nextshop.lib.common.jpa.common.repository.CommonRepository;
import kr.co.ypbooks.nextshop.lib.common.jpa.util.CopyUtil;
import org.hibernate.annotations.NotFound;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.util.StringUtils;

import javax.persistence.Id;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * general Service implements
 *
 * @param <V> Model Vo
 * @param <E> Model
 * @param <T> id Primary Key Type
 */
public class CommonService<V, E, T> {

    private Class<V> entityVoClass;//ModelVo

    private Class<E> entityClass;//Model

    @Autowired
    private CommonRepository<E, T> commonRepository;//inject model class

    public CommonService() {
        Type type = this.getClass().getGenericSuperclass();
        if(type instanceof ParameterizedType){
            ParameterizedType parameterizedType = (ParameterizedType) type;
            Type[] types = parameterizedType.getActualTypeArguments();
            this.entityVoClass = (Class<V>) types[0];
            this.entityClass = (Class<E>) types[1];
        }
    }

    public Result<PageInfo<V>> page(V entityVo) {
        //model don't have page info
        if (!(entityVo instanceof PageCondition)) {
            throw new RuntimeException("class " + entityVoClass.getName() + " don't extends PageCondition。");
        }
        PageCondition pageCondition = (PageCondition) entityVo;
        Page<E> page = commonRepository.findAll(Example.of(CopyUtil.copy(entityVo, entityClass)), pageCondition.getPageable());
        return Result.of(PageInfo.of(page, entityVoClass));
    }

    public Result<List<V>> list(V entityVo) {
        List<E> entityList = commonRepository.findAll(Example.of(CopyUtil.copy(entityVo, entityClass)));
        List<V> entityModelList = CopyUtil.copyList(entityList, entityVoClass);
        return Result.of(entityModelList);
    }

    public Result<V> get(T id) {
        Optional<E> optionalE = commonRepository.findById(id);
        if (!optionalE.isPresent()) {
            throw new RuntimeException("ID does not exist !");
        }
        return Result.of(CopyUtil.copy(optionalE.get(), entityVoClass));
    }

    public Result<V> save(V entityVo) {
        //전달받은 객체（속성값이 누락될수도 있음）
        E entity = CopyUtil.copy(entityVo, entityClass);

        //나중에 저장할 객체
        E entityFull = entity;

        //속성값이 비어있음，속성 무시，BeanUtils 복사시에 사용
        List<String> ignoreProperties = new ArrayList<String>();

        //최신 데이터 가져오기，부분 업데이트시 jpa의 다른필드값을 null로 설정하는 문제 해결
        try {
            //반사획득 Class의 속성（Field는 클래스 내의 멤버 변수를 나타냄）
            for (Field field : entity.getClass().getDeclaredFields()) {
                //라이선스 획득
                field.setAccessible(true);
                //속성 이름
                String fieldName = field.getName();
                //속성 값
                Object fieldValue = field.get(entity);

                //Id 마스터 키 찾기
                if (field.isAnnotationPresent(Id.class) && !StringUtils.isEmpty(fieldValue)) {
                    Optional<E> one = commonRepository.findById((T) fieldValue);
                    if (one.isPresent()) {
                        entityFull = one.get();
                    }
                }

                //값이 비어 있는 속성을 찾아，값이 비어 있으면 속성을 무시하거나 NotFound로 표시，복사할 때 값을 부여하지 않음.
                if(null == fieldValue || field.isAnnotationPresent(NotFound.class)){
                    ignoreProperties.add(fieldName);
                }
            }
            /*
                org.springframework.beans BeanUtils.copyProperties(A,B); A의 값을 B에게
                org.apache.commons.beanutils; BeanUtils.copyProperties(A,B); B의 값을 A에게
                entity의 값을 entityFull에 부여，세번째 파라미터는 무시할 속성값(값을 부여하지 않음)
             */
            BeanUtils.copyProperties(entity, entityFull, ignoreProperties.toArray(new String[0]));
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        E e = commonRepository.save(entityFull);
        return Result.of(CopyUtil.copy(e, entityVoClass));
    }

    public Result<T> delete(T id) {
        commonRepository.deleteById(id);
        return Result.of(id);
    }

    /*
    modelVo에 있는 속성들만 가지고 equal 조건만으로 검색시 공통페이징 검색 사용가능.

    member.setPage(pageNum);
    member.setRows(pageSize);
    member.setSidx("regDate");
    member.setSord("DESC");
    Result<PageInfo<MemberVo>> result = memberService.page(member);
    PageInfo<MemberVo> data = result.getData();
    List<MemberVo> rows = data.getRows();
    int records = data.getRecords();
     */
}
