package kr.co.ypbooks.nextshop.domain.shop.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;

import java.time.LocalDateTime;

@ApiModel
@Getter
@Builder
public class BranchOverviewDto {
    /**
     * 지점 코드 (고유값)
     */
    @ApiModelProperty(value = "지점 코드 (고유값)", example = "12345678")
    @JsonProperty("branch_cd")
    private String branchCd;

    /**
     * 지점명
     */
    @ApiModelProperty(value = "지점명", example = "종로 본점")
    @JsonProperty("branch_name")
    private String branchName;

    /**
     * 지점 지역 코드 (01: 서울, 02: 인천/경기, 03: 대전/충청 등)
     */
    @ApiModelProperty(value = "지점 지역 코드", example = "01")
    @JsonProperty("branch_region_cd")
    private String branchRegionCd;

    /**
     * 관리 유형 (직영, 롯데백화점, 이마트)
     */
    @ApiModelProperty(value = "지점 관리 유형", example = "직영")
    @JsonProperty("branch_mgmt_type")
    private String branchMgmtType;

    /**
     * 운영 유형 (임대, 수수료)
     */
    @ApiModelProperty(value = "지점 운영 유형", example = "임대")
    @JsonProperty("branch_opn_type")
    private String branchOpnType;

    /**
     * 지점 상태 코드 (0:개점, 1:폐점, 2: 개점 (O2O 제외)
     */
    @ApiModelProperty(value = "지점 상태 코드", example = "0")
    @JsonProperty("branch_state_cd")
    private String branchStateCd;


    /**
     * 등록/가입일자
     */
    @ApiModelProperty(value = "등록 일시", example = "2022-03-22 09:00:00")
    @JsonProperty("reg_date")
    private LocalDateTime regDate;

    /**
     * 수정일자
     */
    @ApiModelProperty(value = "수정 일시", example = "2022-03-22 09:00:00")
    @JsonProperty("upd_date")
    private LocalDateTime updDate;

    /**
     * 사업장명
     */
    @ApiModelProperty(value = "사업장명", example = "롯데 백화점")
    @JsonProperty("place_company_name")
    private String placeCompanyName;

    /**
     * 전화번호
     */
    @ApiModelProperty(value = "전화번호", example = "02-1234-5678")
    @JsonProperty("phone")
    private String phone;

    /**
     * 주소 (도/시/동)
     */
    @ApiModelProperty(value = "주소 (도/시/동)", example = "주소 (도/시/동)")
    @JsonProperty("address1")
    private String address1;

    /**
     * 주소 (기타 주소)
     */
    @ApiModelProperty(value = "주소 (기타 주소)", example = "주소 (기타 주소)")
    @JsonProperty("address2")
    private String address2;
}
