package kr.co.ypbooks.nextshop.lib.common.config;

import kr.co.ypbooks.nextshop.lib.common.property.JwtProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PropertiesConfig {

    @Bean(name = "jwtPropertyBean")
    @ConfigurationProperties(prefix = "ypbooks.jwt")
    public JwtProperty getJwtProperty() {
        return new JwtProperty();
    }
}
