package kr.co.ypbooks.nextshop.domain.account.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class Auth {

    //Google , Naver
    String access_token;
    String refresh_token;

    //Naver
    String token_type;
    String expires_in;
    String error;
    String error_description;

    //User profile
    String id;
    String email;
    String nickname;
    String name;
    String gender;
    String birthday;
    String profile_image;
    String mobile;
}
