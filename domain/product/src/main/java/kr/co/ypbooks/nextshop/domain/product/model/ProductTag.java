package kr.co.ypbooks.nextshop.domain.product.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
@Entity
public class ProductTag {
    /**
     * 상품 태그 Id
     */
    @EmbeddedId
    private ProductTagId productTagId;

    /**
     * 등록 일시
     */
    private LocalDateTime regDate;
}
