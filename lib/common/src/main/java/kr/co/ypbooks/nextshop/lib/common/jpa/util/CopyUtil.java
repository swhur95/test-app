package kr.co.ypbooks.nextshop.lib.common.jpa.util;

import org.apache.commons.beanutils.BeanMap;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

import java.util.ArrayList;
import java.util.List;

/**
 * 엔티티 변환 도구
 */
public class CopyUtil {

    /**
     * 형식 변환：model Vo <->model  例如：UserVo <-> User
     * 복잡한 객체 복사를 지원(1급)
     */
    public static <T> T copy(Object src, Class<T> targetType) {
        T target = null;
        try {
            //빈 객체 개체를 만들고 속성 채우기를 위한 BeanWrapperImpl을 가져옴.
            //BeanWrapperImpl은 내부에서 Spring된 BeanUtils 도구 클래스를 사용하여 Bean을 반사시키고 속성을 설정
            target = targetType.newInstance();
            BeanWrapper targetBean = new BeanWrapperImpl(target);

            //기존 객체의 BeanMap을 가져오고, 속성과 속성 값을 Map의 key-value 형식으로 직접 변환
            BeanMap srcBean = new BeanMap(src);
            for (Object key : srcBean.keySet()) {
                //기존 개체 속성 이름
                String srcPropertyName = key + "";
                //기존 객체 속성 값
                Object srcPropertyVal = srcBean.get(key);
                //기존 객체 속성 종류
                Class srcPropertyType = srcBean.getType(srcPropertyName);
                //목표 객체 속성 종류
                Class targetPropertyType = targetBean.getPropertyType(srcPropertyName);

                //기존객체 속성값 비어 있는지 판단、목표 체 속성 종류 비어 있는지 판단，비어있을시 다음 동작
                if ("class".equals(srcPropertyName) || targetPropertyType == null) {
                    continue;
                }

                //같은 종류일시 바로 값 설정, 예：String 과 String 혹은 User과User
                if (srcPropertyType == targetPropertyType) {
                    targetBean.setPropertyValue(srcPropertyName, srcPropertyVal);
                }
                //다른 종류, 예： User와 UserVo
                else {
                    /*     아래의 단계는 위의 단계와 거의 일치      */

                    //기존의 복잡한 개체가 null이면 바로 건너뛰기, 복사할 필요가 없음
                    if(srcPropertyVal == null){
                        continue;
                    }

                    Object targetPropertyVal = targetPropertyType.newInstance();
                    BeanWrapper targetPropertyBean = new BeanWrapperImpl(targetPropertyVal);

                    BeanMap srcPropertyBean = new BeanMap(srcPropertyVal);
                    for (Object srcPropertyBeanKey : srcPropertyBean.keySet()) {
                        String srcPropertyBeanPropertyName = srcPropertyBeanKey + "";
                        Object srcPropertyBeanPropertyVal = srcPropertyBean.get(srcPropertyBeanKey);
                        Class srcPropertyBeanPropertyType = srcPropertyBean.getType(srcPropertyBeanPropertyName);
                        Class targetPropertyBeanPropertyType = targetPropertyBean.getPropertyType(srcPropertyBeanPropertyName);

                        if ("class".equals(srcPropertyBeanPropertyName) || targetPropertyBeanPropertyType == null) {
                            continue;
                        }

                        if (srcPropertyBeanPropertyType == targetPropertyBeanPropertyType) {
                            targetPropertyBean.setPropertyValue(srcPropertyBeanPropertyName, srcPropertyBeanPropertyVal);
                        } else {
                            //복잡한 객체 안의 복잡한 객체는 더 이상 처리하지 않는다
                        }
                    }
                    //목표 대상 객체 속성 값 설정
                    targetBean.setPropertyValue(srcPropertyName, targetPropertyBean.getWrappedInstance());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return target;
    }

    /**
     * 형식 변환: 실체 Vo < - > 실체 예: List< UserVo > < - > List< User >
     */

    public static <T> List<T> copyList(List srcList, Class<T> targetType) {
        List<T> newList = new ArrayList<>();
        for (Object entity : srcList) {
            newList.add(copy(entity, targetType));
        }
        return newList;
    }

}
