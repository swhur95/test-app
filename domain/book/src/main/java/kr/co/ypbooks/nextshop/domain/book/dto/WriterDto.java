package kr.co.ypbooks.nextshop.domain.book.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Builder
public class WriterDto {
    /**
     * 작가 고유 번호
     */
    @ApiModelProperty(value = "작가 고유 번호", example = "12345678")
    @JsonProperty("writer_no")
    private Long writerNo;

    /**
     * 작가 명
     */
    @ApiModelProperty(value = "작가명", example = "작가명")
    @JsonProperty("writer_name")
    private String writerName;

    /**
     * 국내 여부
     */
    @ApiModelProperty(value = "국내 여부", example = "1")
    @JsonProperty("domestic_flag")
    private Boolean domesticFlag;

    /**
     * 출생지
     */
    @ApiModelProperty(value = "출생지", example = "한국")
    @JsonProperty("birth_place")
    private String birthPlace;

    /**
     * 작가 분야 코드 목록
     */
    @ApiModelProperty(value = "작가 분야 코드 목록", example = "[\"010201\", \"010301\"]")
    @JsonProperty("writer_category_cd_list")
    private List<String> writerCategoryCdList;

    /**
     * 작가 태그 단어 목록
     */
    @ApiModelProperty(value = "작가 태그 단어 목록", example = "[\"현대소설가\", \"신인작가\"]")
    @JsonProperty("writer_tag_word_list")
    private List<String> writerTagWordList;

    /**
     * 작가 정보
     */
    @ApiModelProperty(value = "작가 정보")
    @JsonProperty("writer_info")
    private WriterInfo writerInfo;

    /**
     * 등록 일시
     */
    @ApiModelProperty(value = "등록 일시", example = "2022-03-22 09:00:00")
    @JsonProperty("reg_date")
    private LocalDateTime regDate;

    /**
     * 수정 일시
     */
    @ApiModelProperty(value = "수정 일시", example = "2022-03-22 09:00:00")
    @JsonProperty("upd_date")
    private LocalDateTime updDate;

    @Getter
    public static class WriterInfo {
        /**
         * 작가명 (기타 언어)
         */
        @ApiModelProperty(value = "작가명 (기타 언어)", example = "작가명 (기타 언어")
        @JsonProperty("writer_name_lang")
        private String writerNameLang;

        /**
         * 작가 이미지명
         */
        @ApiModelProperty(value = "작가 이미지명", example = "작가 이미지명")
        @JsonProperty("writer_img_name")
        private String writerImgName;

        /**
         * 작가 이미지 경로
         */
        @ApiModelProperty(value = "작가 이미지 경로", example = "작가 이미지 경로")
        @JsonProperty("writer_img_path")
        private String writerImgPath;


        /**
         * 데뷔 연도
         */
        @ApiModelProperty(value = "작가 데뷔 연도", example = "2022")
        @JsonProperty("debut_year")
        private String debutYear;

        /**
         * 데뷔 내용 (작품 또는 직접 입력)
         */
        @ApiModelProperty(value = "작가 데뷔 내용 (작품)", example = "데뷔작품")
        @JsonProperty("debut_content")
        private String debutContent;

        /**
         * 생년월일
         */
        @ApiModelProperty(value = "생년월일", example = "20000222")
        @JsonProperty("birth_date")
        private LocalDate birthDate;

        /**
         * 작가 내용(설명)
         */
        @ApiModelProperty(value = "작가 내용(설명)", example = "작가 설명")
        @JsonProperty("writer_content")
        private String writerContent;
    }
}
