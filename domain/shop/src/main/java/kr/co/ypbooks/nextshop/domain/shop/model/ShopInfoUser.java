package kr.co.ypbooks.nextshop.domain.shop.model;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter

public class ShopInfoUser {

    private int age;
    private int sex;
    private String name;
    private String adress;
    private Long salary;
    private String chat;

}
