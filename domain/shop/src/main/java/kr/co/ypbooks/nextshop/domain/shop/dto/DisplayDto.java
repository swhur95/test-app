package kr.co.ypbooks.nextshop.domain.shop.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Builder;
import lombok.Getter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@ApiModel
@Getter
@Builder
public class DisplayDto {
    /**
     * 전시 번호 (고유 번호)
     */
    @ApiModelProperty(value = "전시 고유 번호", example = "123")
    @JsonProperty("display_no")
    private Integer displayNo;

    /**
     * 전시 제목
     */
    @ApiModelProperty(value = "전시 주제", example = "전시 주제", required = true)
    @JsonProperty("display_subject")
    private String displaySubject;

    /**
     * 관리자 Id (등록 관리자 Id)
     */
    @ApiModelProperty(value = "등록한 관리자 ID", example = "admin", required = true)
    @JsonProperty("reg_admin_id")
    private String regAdminId;

    /**
     * 관리자명 (등록자)
     */
    @ApiModelProperty(value = "등록한 관리자명", example = "관리자", required = true)
    @JsonProperty("reg_admin_name")
    private String regAdminName;

    /**
     * 노출 여부 (0: 비노출, 1: 노출)
     */
    @ApiModelProperty(value = "노출 여부", example = "1", required = true)
    @JsonProperty("expose_flag")
    private Boolean exposeFlag;

    /**
     * 전시 시작 일시
     */
    @ApiModelProperty(value = "전시 시작 일시", example = "2022-03-22 09:00:00", required = true)
    @ApiParam(value = "전시 시작 일시", required = true)
    @JsonProperty("display_start_date")
    private LocalDateTime displayStartDate;

    /**
     * 전시 종료 일시 (null인 경우 종료 없음)
     */
    @ApiModelProperty(value = "전시 종료 일시", example = "2022-09-22 09:00:00")
    @JsonProperty("display_end_date")
    private LocalDateTime displayEndDate;

    /**
     * 전시 정보 (Json)
     */
    @ApiModelProperty(value = "전시 정보")
    @JsonProperty("display_info")
    private DisplayInfo displayInfo;

    /**
     * 등록 일시
     */
    @ApiModelProperty(value = "등록 일시", example = "2022-03-22 09:00:00")
    @JsonProperty("reg_date")
    private LocalDateTime regDate;

    /**
     * 수정 일시
     */
    @ApiModelProperty(value = "수정 일시", example = "2022-03-22 09:00:00")
    @JsonProperty("upd_date")
    private LocalDateTime updDate;

    @ApiModel
    public static class DisplayInfo {
        /**
         * 전시 상세 정보들
         */
        @ApiModelProperty(value = "전시 상세 정보")
        @JsonProperty("display_detail_infos")
        private List<DisplayDetailInfo> displayDetailInfos;

        @ApiModel
        private static class DisplayDetailInfo {
            /**
             * 전시 제목
             */
            @ApiModelProperty(value = "전시 제목", example = "전시 제목", required = true)
            @JsonProperty("display_title")
            private String displayTitle;

            /**
             * 전시 순번
             */
            @ApiModelProperty(value = "전시 순번", example = "1", required = true)
            @JsonProperty("display_seq")
            private String displaySeq;

            /**
             * 전시 분류  (01: 인기 검색어, 02: 인기 태그, 03: 분야별 베스트,
             * 04: 분야별 화제의 신간, 05: 오늘의 상품, 06: 테마 상품,
             * 07: 커스텀 상품)
             */
            @ApiModelProperty(value = "전시 분류 코드", example = "01", required = true)
            @JsonProperty("display_category_cd")
            private String displayCategoryCd;

            /**
             * 전시 내용 정보
             */
            @ApiModelProperty(value = "전시 내용 정보", required = true)
            @JsonProperty("display_content_info")
            private DisplayContentInfo displayContentInfo;

            @ApiModel
            @Getter
            private static class DisplayContentInfo {
                /**
                 * 인기 검색어 정보
                 */
                @ApiModelProperty(value = "인기 검색어 정보")
                @JsonProperty("display_search_info")
                private DisplaySearchInfo searchInfo;

                /**
                 * 인기 태그 정보
                 */
                @ApiModelProperty(value = "인기 태그 정보")
                @JsonProperty("display_tag_info")
                private DisplayTagInfo tagInfo;

                /**
                 * 분야별 베스트 상품 정보
                 */
                @ApiModelProperty(value = "분야별 베스트 상품 정보")
                @JsonProperty("display_category_best_info")
                private DisplayCategoryBestInfo displayCategoryBestInfo;

                /**
                 * 분야별 화제의 새로운 상품 정보
                 */
                @ApiModelProperty(value = "분야별 화제의 새로운 상품 정보")
                @JsonProperty("display_category_new_info")
                private DisplayCategoryNewInfo displayCategoryNewInfo;

                /**
                 * 오늘의 상품 정보
                 */
                @ApiModelProperty(value = "오늘의 상품 정보")
                @JsonProperty("display_today_info")
                private DisplayTodayInfo displayTodayInfo;

                /**
                 * 테마 상품 정보
                 */
                @ApiModelProperty(value = "테마 상품 정보")
                @JsonProperty("display_reco_theme_info")
                private List<DisplayThemeInfo> displayThemeInfo;

                /**
                 * 커스텀 상품 정보
                 */
                @ApiModelProperty(value = "커스텀 상품 정보")
                @JsonProperty("display_custom_info")
                private List<DisplayCustomInfo> displayCustomInfo;

                @ApiModel
                private static class DisplayProductInfo {
                    /**
                     * 상품 유형 (B: 도서, E: E-Book)
                     */
                    @ApiModelProperty(value = "상품 유형", example = "B", required = true)
                    @JsonProperty("product_type")
                    private String productType;

                    /**
                     * 상품 고유 코드
                     */
                    @ApiModelProperty(value = "상품 고유 코드", example = "2598700091", required = true)
                    @JsonProperty("product_cd")
                    private String productCd;
                }

                @ApiModel
                private static class DisplayThemeInfo {
                    /**
                     * 테마 유형 코드 (01: MD 테마, 02: 회원 테마, 03: 지점 테마)
                     */
                    @ApiModelProperty(value = "테마 유형 코드", example = "01", required = true)
                    @JsonProperty("theme_type_cd")
                    private String themeTypeCd;

                    /**
                     * 테마 고유 코드
                     */
                    @ApiModelProperty(value = "테마 코드", example = "T00000001", required = true)
                    @JsonProperty("theme_cd")
                    private String themeCd;
                }

                @ApiModel
                private static class DisplayCustomInfo {
                    /**
                     * 커스텀 타입 코드 (01: 상품, 02: 테마)
                     */
                    @ApiModelProperty(value = "커스텀 타입 코드", example = "01", required = true)
                    @JsonProperty("display_custom_type_cd")
                    private String displayCustomType;

                    /**
                     * 상품 정보
                     */
                    @ApiModelProperty(value = "상품 정보")
                    @JsonProperty("display_product_info")
                    private DisplayProductInfo displayProductInfo;

                    /**
                     * 테마 정보
                     */
                    @ApiModelProperty(value = "테마 정보")
                    @JsonProperty("display_theme_info")
                    private DisplayThemeInfo displayThemeInfo;
                }

                @ApiModel
                private static class DisplaySearchInfo {
                    /**
                     * 인기 검색어 목록
                     */
                    @ApiModelProperty(value = "인기 검색어 목록", example = "[검색어1, 검색어2]", required = true)
                    @JsonProperty("search_word_list")
                    private List<String> searchWordList;
                }

                @ApiModel
                private static class DisplayCategoryProductInfo {
                    /**
                     * 카테고리명
                     */
                    @ApiModelProperty(value = "카테고리명", example = "문학", required = true)
                    @JsonProperty("category_name")
                    private String categoryName;

                    /**
                     * 상품 정보
                     */
                    @ApiModelProperty(value = "상품 정보", required = true)
                    @JsonProperty("product_infos")
                    private List<DisplayProductInfo> displayProductInfos;
                }

                @ApiModel
                private static class DisplayTagInfo {
                    /**
                     * 인기 태그 목록
                     */
                    @ApiModelProperty(value = "인기 태그 목록", example = "[태그1, 태그2]", required = true)
                    @JsonProperty("tag_word_list")
                    private List<String> tagWordList;
                }

                @ApiModel
                private static class DisplayCategoryBestInfo {
                    /**
                     * 분야별 베스트 상품 정보
                     */
                    @ApiModelProperty(value = "분야별 베스트 상품 정보", required = true)
                    @JsonProperty("display_category_best_infos")
                    private List<DisplayCategoryProductInfo> displayCategoryBestInfos;
                }

                @ApiModel
                private static class DisplayCategoryNewInfo {
                    /**
                     * 분야별 화제의 신규 상품 정보
                     */
                    @ApiModelProperty(value = "분야별 화제의 신규 상품 정보", required = true)
                    @JsonProperty("display_category_new_infos")
                    private List<DisplayCategoryProductInfo> displayCategoryNewInfos;
                }

                @ApiModel
                private static class DisplayTodayInfo {
                    /**
                     * 기준 일
                     */
                    @ApiModelProperty(value = "기준일", example = "2022-03-22", required = true)
                    @JsonProperty("criteria_date")
                    private LocalDate criteriaDate;

                    /**
                     * 상품 정보
                     */
                    @ApiModelProperty(value = "상품 정보", required = true)
                    @JsonProperty("display_product_infos")
                    private List<DisplayProductInfo> displayProductInfos;
                }
            }
        }
    }
}
