package kr.co.ypbooks.nextshop.domain.shop.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;

import java.time.LocalDateTime;
import java.util.List;

@ApiModel
@Getter
@Builder
public class BranchDto {
    /**
     * 지점 코드 (고유값)
     */
    @ApiModelProperty(value = "지점 코드 (고유값)", example = "12345678", required = true)
    @JsonProperty("branch_cd")
    private String branchCd;

    /**
     * 지점명
     */
    @ApiModelProperty(value = "지점명", example = "종로 본점", required = true)
    @JsonProperty("branch_name")
    private String branchName;

    /**
     * 지점 지역 코드 (01: 서울, 02: 인천/경기, 03: 대전/충청 등)
     */
    @ApiModelProperty(value = "지점 지역 코드", example = "01", required = true)
    @JsonProperty("branch_region_cd")
    private String branchRegionCd;

    /**
     * 관리 유형 (직영, 롯데백화점, 이마트)
     */
    @ApiModelProperty(value = "지점 관리 유형", example = "직영", required = true)
    @JsonProperty("branch_mgmt_type")
    private String branchMgmtType;

    /**
     * 운영 유형 (임대, 수수료)
     */
    @ApiModelProperty(value = "지점 운영 유형", example = "임대", required = true)
    @JsonProperty("branch_opn_type")
    private String branchOpnType;

    /**
     * 지점 상태 코드 (0:개점, 1:폐점, 2: 개점 (O2O 제외)
     */
    @ApiModelProperty(value = "지점 상태 코드", example = "0", required = true)
    @JsonProperty("branch_state_cd")
    private String branchStateCd;

    /**
     * 지점 정보
     */
    @ApiModelProperty(value = "지점 정보", required = true)
    @JsonProperty("branch_info")
    private BranchInfo branchInfo;

    /**
     * 관리자 정보
     */
    @ApiModelProperty(value = "관리자 정보")
    @JsonProperty("admin_info")
    private AdminInfo adminInfo;

    /**
     * 영업 시간 정보
     */
    @ApiModelProperty(value = "영업 시간 정보")
    @JsonProperty("biz_time_info")
    private BizTimeInfo bizTimeInfo;

    /**
     * 맵 정보
     */
    @ApiModelProperty(value = "맵 정보")
    @JsonProperty("map_info")
    private MapInfo mapInfo;

    /**
     * 연락처 정보
     */
    @ApiModelProperty(value = "연락처 정보")
    @JsonProperty("contact_info")
    private ContactInfo contactInfo;

    /**
     * 오시는 길 정보
     */
    @ApiModelProperty(value = "오시는 길 정보")
    @JsonProperty("directions_info")
    private DirectionsInfo directionsInfo;

    /**
     * 가이드 맵 정보
     */
    @ApiModelProperty(value = "가이드 맵 정보")
    @JsonProperty("guide_map_info")
    private GuideMapInfo guideMapInfo;

    /**
     * 등록/가입일자
     */
    @ApiModelProperty(value = "등록 일시", example = "2022-03-22 09:00:00", required = true)
    @JsonProperty("reg_date")
    private LocalDateTime regDate;

    /**
     * 수정일자
     */
    @ApiModelProperty(value = "수정 일시", example = "2022-03-22 09:00:00")
    @JsonProperty("upd_date")
    private LocalDateTime updDate;

    @Getter
    public static class BranchInfo {
        /**
         * 개점시간 (HHMM)
         */
        @ApiModelProperty(value = "개점시간 (HHMM)", example = "0900", required = true)
        @JsonProperty("open_time")
        private String openTime;

        /**
         * 폐점시간 (HHMM)
         */
        @ApiModelProperty(value = "폐점시간 (HHMM)", example = "2100", required = true)
        @JsonProperty("close_time")
        private String closeTime;

        /**
         * 문의 연결 지점 (고객센터 또는 관리자 전화번호)
         */
        @ApiModelProperty(value = "문의 연결 지점 (고객센터 또는 관리자 전화번호)", example = "고객센터")
        @JsonProperty("connect_point")
        private String connectPoint;

        /**
         * 소개 링크
         */
        @ApiModelProperty(value = "소개 링크", example = "소개 링크")
        @JsonProperty("intro_url")
        private String introUrl;

        /**
         * 대표 이미지명
         */
        @ApiModelProperty(value = "대표 이미지명", example = "대표 이미지명")
        @JsonProperty("rep_img_name")
        private String repImgName;

        /**
         * 대표 이이지 경로
         */
        @ApiModelProperty(value = "대표 이이지 경로", example = "대표 이이지 경로")
        @JsonProperty("rep_img_path")
        private String repImgPath;

        /**
         * 대표 이미지 등록일시
         */
        @ApiModelProperty(value = "대표 이이지 등록일시", example = "2022-03-22 09:00:00")
        @JsonProperty("rep_img_reg_date")
        private LocalDateTime repImgRegDate;

        /**
         * 영업 시간 설명
         */
        @ApiModelProperty(value = "영업 시간 설명", example = "오전 10:30 ~ 오후 9:00 (휴무 : 백화점 정기휴일)")
        @JsonProperty("biz_hour_desc")
        private String bizHourDesc;

        /**
         * 우편번호
         */
        @ApiModelProperty(value = "우편번호", example = "우편번호")
        @JsonProperty("post_no")
        private String postNo;

        /**
         * 주소 (도/시/동)
         */
        @ApiModelProperty(value = "주소 (도/시/동)", example = "주소 (도/시/동)")
        @JsonProperty("address1")
        private String address1;

        /**
         * 주소 (기타 주소)
         */
        @ApiModelProperty(value = "주소 (기타 주소)", example = "주소 (기타 주소)")
        @JsonProperty("address2")
        private String address2;

        /**
         * 전화번호
         */
        @ApiModelProperty(value = "전화번호", example = "02-1234-5678")
        @JsonProperty("phone")
        private String phone;

        /**
         * 채용 구분
         */
        @ApiModelProperty(value = "채용구분", example = "0")
        @JsonProperty("recruit_flag")
        private Boolean recruitFlag;

        /**
         * 사업장 명
         */
        @ApiModelProperty(value = "사업장명", example = "롯데 백화점")
        @JsonProperty("place_company_name")
        private String placeCompanyName;

        /**
         * 이력서 파일명
         */
        @ApiModelProperty(value = "이력서 파일명", example = "이력서 파일명")
        @JsonProperty("resume_file_name")
        private String resumeFilename;

        /**
         * 지점 내용
         */
        @ApiModelProperty(value = "지점 내용", example = "지점 내용")
        @JsonProperty("branch_content")
        private String branchContent;
    }

    @Getter
    public static class AdminInfo {
        /**
         * 관리자 상세 정보들
         */
        @ApiModelProperty(value = "관리자 상세 정보들", required = true)
        @JsonProperty("admin_detail_infos")
        private List<AdminDetailInfo> adminDetailInfos;

        private static class AdminDetailInfo {
            /**
             * 관리자명
             */
            @ApiModelProperty(value = "관리자명", example = "관리자", required = true)
            @JsonProperty("admin_name")
            private String adminName;

            /**
             * 관리자 전화번호
             */
            @ApiModelProperty(value = "관리자 전화번호", example = "010-1234-5678", required = true)
            @JsonProperty("admin_phone")
            private String adminPhone;

            /**
             * SORT_SEQ
             */
            @ApiModelProperty(value = "정렬 순번", example = "1", required = true)
            @JsonProperty("sort_seq")
            private Byte sortSeq;
        }
    }

    @Getter
    public static class BizTimeInfo {
        /**
         * 영업 시간 정보들
         */
        @ApiModelProperty(value = "영업 시간 정보들", required = true)
        @JsonProperty("biz_time_detail_infos")
        private List<BizTimeDetailInfo> bizTimeDetailInfos;

        private static class BizTimeDetailInfo {
            /**
             * 일 유형 코드 ("WD: weekday, WE: weekend, PHOL: public holiday, THOL: traditional holiday, DTHOL: the day of traditional holiday")
             */
            @ApiModelProperty(value = "일 유형 코드", example = "WE", required = true)
            @JsonProperty("day_type_cd")
            private String dayTypeCd;

            /**
             * 개점 여부
             */
            @ApiModelProperty(value = "개점 여부", example = "1", required = true)
            @JsonProperty("open_flag")
            private Boolean openFlag;

            /**
             * 개점시간 (HHMM)
             */
            @ApiModelProperty(value = "개점시간", example = "0900", required = true)
            @JsonProperty("open_time")
            private String openTime;

            /**
             * 영업 종료 시간
             */
            @ApiModelProperty(value = "폐점시간", example = "2100", required = true)
            @JsonProperty("close_time")
            private String closeTime;
        }
    }

    @Getter
    public static class MapInfo {
        /**
         * Map 공급자 코드 (GE: GOOGLE, NV: NAVER, KO: KAKAO)
         */
        @ApiModelProperty(value = "Map 공급자 코드", example = "GE", required = true)
        @JsonProperty("map_provider_cd")
        private String mapProviderCd;

        /**
         * Map url
         */
        @ApiModelProperty(value = "Map url", example = "Map url", required = true)
        @JsonProperty("map_url")
        private String mapUrl;

        /**
         * map 좌표 정보
         */
        @ApiModelProperty(value = "map 좌표", example = "map 좌표", required = true)
        @JsonProperty("map_coordinate")
        private String mapCoordinate;
    }

    @Getter
    public static class ContactInfo {
        /**
         * 연락처 정보들
         */
        @ApiModelProperty(value = "연락처 정보들", required = true)
        @JsonProperty("contact_detail_infos")
        private List<ContactDetailInfo> contactDetailInfos;

        private static class ContactDetailInfo {
            /**
             * 연락처 유형 코드 (TEL, FAX, MAIL)
             */
            @ApiModelProperty(value = "연락처 유형 코드", example = "TEL", required = true)
            @JsonProperty("contact_type_cd")
            private String contactTypeCd;

            /**
             * 연락처 라벨
             */
            @ApiModelProperty(value = "연락처 라벨", example = "문구", required = true)
            @JsonProperty("contact_label")
            private String contactLabel;

            /**
             * 연락처 내용
             */
            @ApiModelProperty(value = "연락처 내용", example = "02-1234-5678", required = true)
            @JsonProperty("contact_content")
            private String contactContent;
        }
    }

    @Getter
    public static class DirectionsInfo {
        /**
         * 오시는 길 상세 정보들
         */
        @ApiModelProperty(value = "오시는 길 상세 정보들", required = true)
        @JsonProperty("directions_detail_infos")
        private List<DirectionsDetailInfo> directionsDetailInfos;

        private static class DirectionsDetailInfo {
            /**
             * 교통 수단 유형 코드 (SUB: Subway, BUS: Bus, CAR: Car)
             */
            @ApiModelProperty(value = "교통 수단 유형 코드", example = "SUB", required = true)
            @JsonProperty("trans_type_cd")
            private String transTypeCd;

            /**
             * 오시는 길 내용
             */
            @ApiModelProperty(value = "오시는 길 내용", example = "오시는 길 내용", required = true)
            @JsonProperty("directions_content")
            private String directionsContent;
        }
    }

    @Getter
    public static class GuideMapInfo {
        /**
         * 가이드 맵 이미지명 (파일명)
         */
        @ApiModelProperty(value = "가이드 맵 이미지명", example = "가이드 맵 이미지명", required = true)
        @JsonProperty("guide_map_img_name")
        private String guideMapImgName;

        /**
         * 가이드 맵 이미지 경로
         */
        @ApiModelProperty(value = "가이드 맵 이미지 경로", example = "가이드 맵 이미지 경로", required = true)
        @JsonProperty("guide_map_img_path")
        private String guideMapImgPath;

        /**
         * 가이드 맵 도서 책장 정보
         */
        @ApiModelProperty(value = "가이드 맵 도서 책장 정보", required = true)
        @JsonProperty("bookshelf_info")
        private BookshelfInfo bookshelfInfo;

        private static class BookshelfInfo {
            /**
             * 가이드 맵 도서 책장 상세 정보들
             */
            @ApiModelProperty(value = "가이드 맵 도서 책장 상세 정보들", required = true)
            @JsonProperty("bookshelf_detail_infos")
            private List<BookshelfDetailInfo> bookshelfDetailInfos;

            private static class BookshelfDetailInfo {
                /**
                 * 도서 책장명
                 */
                @ApiModelProperty(value = "도서 책장명", example = "도서 책장명", required = true)
                @JsonProperty("bookshelf_name")
                private String bookselfName;

                /**
                 * 그룹 카테고리 코드
                 */
                @ApiModelProperty(value = "그룹 카테고리 코드", example = "01", required = true)
                @JsonProperty("group_category_cd")
                private String groupCategoryCd;

                /**
                 * 카테고리 코드 목록
                 */
                @ApiModelProperty(value = "카테고리 코드 목록", example = "[\"010203\", \"010204\"]", required = true)
                @JsonProperty("category_cd_list")
                private List<String> categoryCdList;
            }
        }
    }
}
