package kr.co.ypbooks.nextshop.domain.order.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
@Entity
public class DepositHist {
    /**
     * 예치금 이력 고유 번호
     */
    @Id
    private Long depositNo;

    /**
     * 주문 번호
     */
    private Long orderNo;

    /**
     * 회원 코드
     */
    private String memberCd;

    /**
     * 예치금 유형 코드
     */
    private String depositTypeCd;

    /**
     * 예치금 반영 전 금액
     */
    private Long beforeDepositAmt;

    /**
     * 예치금 반영 후 금액
     */
    private Long afterDepositAmt;

    /**
     * 예치금 반영 금액
     */
    private Long depositAmt;

    /**
     * 등록 일시
     */
    private LocalDateTime regDate;
}