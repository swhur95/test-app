package kr.co.ypbooks.nextshop.domain.product.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
@Entity
public class ProductLogistStock {
    /**
     * 아이템 고유 번호
     */
    @Id
    private Long itemNo;

    /**
     * 상품 유형 코드
     */
    private String productTypeCd;

    /**
     * 상품 코드
     */
    private String productCd;

    /**
     * 상품 식별 코드
     */
    private String productIdCd;

    /**
     * 상품 정가
     */
    private Long productPrice;

    /**
     * 상품 수량
     */
    private Integer quantity;

    /**
     * 재고 상태 코드
     */
    private String stockStatusCd;

    /**
     * 배송 필요 시간
     */
    private Integer delivNeedHour;

    /**
     * 등록 일시
     */
    private LocalDateTime regDate;

    /**
     * 수정 일시
     */
    private LocalDateTime updDate;


}
