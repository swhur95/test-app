package kr.co.ypbooks.nextshop.domain.account.dto;

import io.swagger.annotations.ApiModelProperty;
import kr.co.ypbooks.nextshop.domain.account.model.adminlog.*;
import kr.co.ypbooks.nextshop.lib.common.jpa.common.pojo.PageCondition;
import kr.co.ypbooks.nextshop.domain.account.model.adminlog.*;
import lombok.*;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import java.io.Serializable;
import java.time.LocalDateTime;


@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter

public class AdminActionHistVo extends PageCondition implements Serializable {
    /**
     * 로그 번호
     */
    @ApiModelProperty(value = "로그 번호", required = true, example = "1")
    private Integer logNo;
    /**
     * 회원번호
     */
    @ApiModelProperty(value = "회원번호", required = true, example = "memberCd-AA001")
    private String memberCd;
    /**
     * 관리자 ID
     */
    @ApiModelProperty(value = "관리자 ID", required = true, example = "adminIdUUUID-001")
    private String adminId;
    /**
     * 관리자 IP
     */
    @ApiModelProperty(value = "관리자 IP", required = true, example = "10.20.15.203")
    private String adminIp;
    /**
     * 등록/가입일자
     */
    @ApiModelProperty(example = "2022-03-01 12:18:48")
    private LocalDateTime regDate;
    /**
     * 로그 타입 (1: 관리자 메뉴 조회, 2: 관리자 로그인, 3: 관리자 페이지 접근, 4: 개인정보 검색, 5: IP 관리, 6: 사용자 권한 설정, 7:  전시 설정)
     */
    @ApiModelProperty(value = "로그 타입 (1: 관리자 메뉴 조회, 2: 관리자 로그인, 3: 관리자 페이지 접근, 4: 개인정보 검색, 5: IP 관리, 6: 사용자 권한 설정, 7:  전시 설정)", required = true, example = "1")
    private String logType;
    /**
     * 로그 컨텐츠
     */
    @Type(type = "json")
    @Column(columnDefinition = "json")
    private String logContent;

    private AdminActionHistViewMenuLogInfo viewMenuLogInfo;
    private AdminActionHistLoginLogInfo loginLogInfo;
    private AdminActionHistAccessLogInfo accessLogInfo;
    private AdminActionHistSearchLogInfo searchLogInfo;
    private AdminActionHistIpMgmtLogInfo ipMgmtLogInfo;
    private AdminActionHistAuthLogInfo authLogInfo;
    private AdminActionHistDisplaySettingLog displaySettingLog;

}
