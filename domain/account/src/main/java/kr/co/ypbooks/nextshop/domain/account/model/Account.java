package kr.co.ypbooks.nextshop.domain.account.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class Account {
    public Account(String userId, String name, String passwd) {
        this.userId = userId;
        this.name = name;
        this.passwd = passwd;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private String userId;

    private String name;
    private String passwd;

    private String countryCd;
}
