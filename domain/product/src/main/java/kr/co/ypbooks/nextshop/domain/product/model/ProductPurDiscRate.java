package kr.co.ypbooks.nextshop.domain.product.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
@Entity
public class ProductPurDiscRate {
    /**
     * 상품 매입율 Id
     */
    @EmbeddedId
    private ProductPurDiscRateId productPurDiscRateId;

    /**
     * 할인율
     */
    private Byte discRate;

    /**
     * 적립율
     */
    private Byte reserveRate;

    /**
     * 대분류 (카테고리 코드1)
     */
    private String categoryCd1;

    /**
     * 등록 일시
     */
    private LocalDateTime regDate;

    /**
     * 수정 일시
     */
    private LocalDateTime updDate;
}
