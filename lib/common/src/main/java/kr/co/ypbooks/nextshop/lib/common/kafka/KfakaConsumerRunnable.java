package kr.co.ypbooks.nextshop.lib.common.kafka;

import kr.co.ypbooks.nextshop.lib.common.config.KafkaConfig;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.Collections;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

@Slf4j
@Getter
@Setter
@Service
public class KfakaConsumerRunnable extends Thread {

    private final AtomicBoolean closed = new AtomicBoolean(false);

    private Map<String, Object> consumerConfigs;
    private String topicName;
    private String groupId;
    //business interface，Implementation interface to handle your own business
    private ConsumerMessageHelper consumerMessageHelper;

    /**
     * run a consumer with consumerConfigs
     */
    @Override
    public void run() {

        if(StringUtils.isNotBlank(groupId)) {
            consumerConfigs.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        }
        log.info("start regist KfakaConfigRunnable => {}-{} ", topicName, groupId);

        KafkaConsumer<String, String> consumer = new KafkaConsumer<String, String>(consumerConfigs);

        consumer.subscribe(Collections.singletonList(topicName));
        try {
            while (true) {
                // Pulling data from server
                ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(1500));
                if (records.isEmpty()) {
                    log.info("{} Consumers are not consuming data------->", topicName);
                    consumerMessageHelper.noRecordsConsumer(topicName);
                    Thread.sleep(5000);
                    continue;
                }else {
                    //do bussiness
                    //log.info("offset is " + record.offset() + "data:-->" + record.value().toString());
                    log.info("do bussiness topic-group : {}-{}", topicName, groupId);
                    consumerMessageHelper.myConsumerWork(records, topicName);

                    consumer.commitAsync();
                }
            }
        } catch (Exception e) {
            log.error("KfakaConfigRunnable consuming Exception :{} ", topicName, e);
        } finally {
            try {
                consumer.commitSync();
            } finally {
                //Exception first shutdown.
                consumer.close();
                String poolKey = topicName;
                if(StringUtils.isNotBlank(groupId)){
                    poolKey = topicName + groupId;
                }
                KafkaConfig.KFAKACONSUMERRUNNABLE_POOL.remove(poolKey);
            }
        }
    }

    /**
     * Shutdown hook which can be called from a separate thread
     */
    public void shutdown(KafkaConsumer<String, String> consumer) {
        closed.set(true);
        consumer.wakeup();
    }
}

