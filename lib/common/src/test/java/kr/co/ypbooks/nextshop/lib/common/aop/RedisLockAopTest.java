package kr.co.ypbooks.nextshop.lib.common.aop;

import kr.co.ypbooks.nextshop.lib.common.operation.JedisLock;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import redis.clients.jedis.Jedis;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@ActiveProfiles("alpha")
public class RedisLockAopTest {
    @Resource(name = "jedisConfig")
    private Jedis jedisConfig;

    @Test
    public void testJedisLock() throws InterruptedException {

        JedisLock lock = new JedisLock(jedisConfig, "lockTest", 10000, 60000);
        int size = 10;
        List<Thread> threads = new ArrayList<Thread>();
        for (int i = 0; i < size; i++) {
            final int j = i;
            Thread t = new Thread() {
                public void run() {
                    try {
                        lock.acquire();
                        System.out.println("this thread is using lock: the thread num " + j);
                        lock.release();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                };
            };

            threads.add(t);
        }

        for (Thread thread : threads) {
            thread.start();
            thread.join();
        }

    }
}
