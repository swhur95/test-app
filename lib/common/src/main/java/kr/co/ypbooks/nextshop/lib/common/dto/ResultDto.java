package kr.co.ypbooks.nextshop.lib.common.dto;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;

@Data
public class ResultDto {
    /**
     * 결과 코드
     */
    @ApiModelProperty(example = "SUCCESS")
    @ApiParam(value = "결과 코드", required = true)
    private String result;

    /**
     * 결과 메시지
     */
    @ApiModelProperty(example = "성공")
    @ApiParam(value = "결과 메시지")
    private String resultMessage;

    /**
     * 결과 데이터
     */
    @ApiParam(value = "결과 데이터")
    private Object resultData;
}
