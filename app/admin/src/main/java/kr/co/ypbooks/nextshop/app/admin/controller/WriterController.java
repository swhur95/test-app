package kr.co.ypbooks.nextshop.app.admin.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import kr.co.ypbooks.nextshop.domain.book.dto.WriterDto;
import kr.co.ypbooks.nextshop.domain.book.dto.WriterSearchDto;
import kr.co.ypbooks.nextshop.domain.book.service.WriterService;
import kr.co.ypbooks.nextshop.lib.common.dto.ResultDto;


import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@Slf4j
@Api(tags = {"Writer"})
@RequestMapping("/admin/writer")
@RestController
public class WriterController {
    private final WriterService writerService;

    public WriterController(WriterService writerService) {
        this.writerService = writerService;
    }

    @ApiOperation(value = "작가 정보 개요 목록" , notes = "작가 정보 개요 목록을 얻어옵니다.")
    @GetMapping("")
    public ResultDto getWriterOverview(@RequestBody WriterSearchDto writerSearchDto) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    @ApiOperation(value = "작가 정보 조회" , notes = "작가 정보를 얻어옵니다.")
    @GetMapping("/{writer_no}")
    public ResultDto getWriter(@PathVariable("writer_no") Long writerNo) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    @ApiOperation(value = "작가 정보 추가" , notes = "작가 정보를 추가합니다.")
    @PostMapping("")
    public ResultDto insertWriter(@RequestBody WriterDto writerDto) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    @ApiOperation(value = "작가 정보 수정" , notes = "작가 정보를 수정합니다.")
    @PutMapping("/{writer_no}")
    public ResultDto updateWriter(@PathVariable("writer_no") Long writerNo, @RequestBody WriterDto writerDto) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    @ApiOperation(value = "작가 정보 삭제" , notes = "작가 정보를 삭제합니다.")
    @DeleteMapping("/{writer_no}")
    public ResultDto deleteWriter(@PathVariable("writer_no") Long writerNo) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }
}
