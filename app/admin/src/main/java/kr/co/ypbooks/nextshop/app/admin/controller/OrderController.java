package kr.co.ypbooks.nextshop.app.admin.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import kr.co.ypbooks.nextshop.lib.common.dto.ResultDto;
import kr.co.ypbooks.nextshop.domain.order.dto.OrderProductSearchDto;
import kr.co.ypbooks.nextshop.domain.order.dto.OrderSearchDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@Api(tags = {"Order"})
@RequestMapping("/admin/order")
@RestController
public class OrderController {
    @ApiOperation(value = "주문 정보 개요 목록" , notes = "주문 정보 개요 목록을 얻어옵니다.")
    @GetMapping("")
    public ResultDto getOrderOverview(@RequestBody OrderSearchDto writerSearchDto) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    @ApiOperation(value = "주문 상품 정보 개요 목록" , notes = "주문 상품 정보 개요 목록을 얻어옵니다.")
    @GetMapping("/{order_no}/product")
    public ResultDto getOrderProductOverview(@RequestBody OrderProductSearchDto orderProductSearchDto) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }
}
