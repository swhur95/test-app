package kr.co.ypbooks.nextshop.domain.shop.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@ApiModel
@Getter
@Builder
public class BranchDayOffDto {
    /**
     * 지점 휴무 고유 코드
     */
    @ApiModelProperty(value = "지점 휴무 고유 번호", example = "1")
    @JsonProperty("branch_day_off_no")
    private Long branchDayOffNo;

    /**
     * 지점 코드
     */
    @ApiModelProperty(value = "지점코드", example = "1234567")
    @JsonProperty("branch_cd")
    private String branchCd;

    /**
     * 지점명
     */
    @ApiModelProperty(value = "지점명", example = "지점명")
    @JsonProperty("branch_name")
    private String branchName;

    /**
     * 휴무일
     */
    @ApiModelProperty(value = "휴무일", example = "2022-03-22")
    @JsonProperty("day_off_date")
    private LocalDate dayOffDate;

    /**
     * 휴무 사유
     */
    @ApiModelProperty(value = "휴무 사유", example = "백화점 휴무로 인하여 휴무")
    @JsonProperty("day_off_reason")
    private String dayOffReason;

    /**
     * 요일
     */
    @ApiModelProperty(value = "요일", example = "2")
    @JsonProperty("day_of_week")
    private Byte dayOfWeek;

    /**
     * 등록한 관리자 Id
     */
    @ApiModelProperty(value = "등록한 관리자 ID", example = "admin")
    @JsonProperty("reg_admin_id")
    private String regAdminId;

    /**
     * 등록한 관리자명
     */
    @ApiModelProperty(value = "등록한 관리자명", example = "관리자")
    @JsonProperty("reg_admin_name")
    private String regAdminName;

    /**
     * 등록 일시
     */
    @ApiModelProperty(value = "등록 일시", example = "2022-03-22 09:00:00")
    @JsonProperty("reg_date")
    private LocalDateTime regDate;

    /**
     * 수정 일시
     */
    @ApiModelProperty(value = "수정 일시", example = "2022-03-22 09:00:00")
    @JsonProperty("upd_date")
    private LocalDateTime updDate;
}
