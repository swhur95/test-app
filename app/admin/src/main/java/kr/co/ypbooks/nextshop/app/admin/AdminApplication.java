package kr.co.ypbooks.nextshop.app.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = {"kr.co.ypbooks.nextshop.app.admin",
        "kr.co.ypbooks.nextshop.domain.account",
        "kr.co.ypbooks.nextshop.lib.common",
        "kr.co.ypbooks.nextshop.domain.product",
        "kr.co.ypbooks.nextshop.domain.book",
        "kr.co.ypbooks.nextshop.domain.order",
        "kr.co.ypbooks.nextshop.domain.shop"})

@EnableJpaRepositories(basePackages = {"kr.co.ypbooks.nextshop.domain.account.repository",
        "kr.co.ypbooks.nextshop.domain.product.repository",
        "kr.co.ypbooks.nextshop.domain.book.repository",
        "kr.co.ypbooks.nextshop.domain.order.repository",
        "kr.co.ypbooks.nextshop.domain.shop.repository"})
@EntityScan(basePackages = {"kr.co.ypbooks.nextshop.domain.account.model",
        "kr.co.ypbooks.nextshop.domain.product.model",
        "kr.co.ypbooks.nextshop.domain.book.model",
        "kr.co.ypbooks.nextshop.domain.order.model",
        "kr.co.ypbooks.nextshop.domain.shop.model"})


public class AdminApplication {
    public static void main(String[] args) {
        SpringApplication.run(AdminApplication.class, args);
    }
}
