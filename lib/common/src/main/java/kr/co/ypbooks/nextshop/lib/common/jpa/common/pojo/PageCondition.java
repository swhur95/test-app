package kr.co.ypbooks.nextshop.lib.common.jpa.common.pojo;

import lombok.Data;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

/**
 * Paging requirements
 */
@Data
public class PageCondition {
    private int page = 1;//Current Page Number
    private int rows = 10;//Page Size
    private String sidx;//Sort Field
    private String sord;//sort method

    /**
     * Get JPA's Paging Query Target
     */
    public Pageable getPageable() {
        //Handle illegal page numbers
        if (page < 0) {
            page = 1;
        }
        //Handle illegal page size
        if (rows < 0) {
            rows = 10;
        }

        if(sidx != null){
            Sort sort = null;
            if(sord != null && sord.equals("DESC")){
                sort = Sort.by(Sort.Direction.DESC, sidx);
            } else{
                sort = Sort.by(Sort.Direction.ASC, sidx);
            }
            return PageRequest.of(page - 1, rows, sort);
        } else {
            return PageRequest.of(page - 1, rows);
        }
    }
}
