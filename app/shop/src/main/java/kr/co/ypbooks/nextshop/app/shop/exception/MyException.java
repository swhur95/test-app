package kr.co.ypbooks.nextshop.app.shop.exception;

public class MyException extends Exception{

    private Integer code;

    public MyException(String msg){
        super(msg);
    }

    public MyException(Integer code, String msg){
        super(msg);
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }


}
