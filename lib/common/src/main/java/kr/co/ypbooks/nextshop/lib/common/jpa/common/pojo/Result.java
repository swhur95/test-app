package kr.co.ypbooks.nextshop.lib.common.jpa.common.pojo;


import lombok.Data;

import java.io.Serializable;

/**
 * Uniform Return Objects
 */

@Data
public class Result<T> implements Serializable {
    /**
     * communication data
     */
    private T data;
    /**
     * 通信状态
     */
    private boolean flag = true;
    /**
     * communication status
     */
    private String msg = "Operation successful";

    /**
     * instances using static methods
     */
    public static <T> Result<T> of(T data) {
        return new Result<>(data);
    }

    public static <T> Result<T> of(T data, boolean flag) {
        return new Result<>(data, flag);
    }

    public static <T> Result<T> of(T data, boolean flag, String msg) {
        return new Result<>(data, flag, msg);
    }

    @Deprecated
    public Result() {

    }

    private Result(T data) {
        this.data = data;
    }

    private Result(T data, boolean flag) {
        this.data = data;
        this.flag = flag;
    }

    private Result(T data, boolean flag, String msg) {
        this.data = data;
        this.flag = flag;
        this.msg = msg;
    }

}
