package kr.co.ypbooks.nextshop.lib.common.jpa.common.pojo;


import kr.co.ypbooks.nextshop.lib.common.jpa.util.CopyUtil;
import lombok.Data;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.support.PageableExecutionUtils;
import org.springframework.util.Assert;

import javax.persistence.Query;
import java.util.List;

/**
 * Page Objects
 */
@Data
public class PageInfo<M> {
    private int page;//Current Page Number
    private int pageSize;//Page Size
    private String sidx;//Sort Field
    private String sord;//sort method

    private List<M> rows;//Page Results
    private int records;//total number of records
    private int total;//Total number of pages

    /**
     * Get integrated failover objects
     */
    public static <M> PageInfo<M> of(Page page, Class<M> entityModelClass) {
        int records = (int) page.getTotalElements();
        int pageSize = page.getSize();
        int total = records % pageSize == 0 ? records / pageSize : records / pageSize + 1;

        PageInfo<M> pageInfo = new PageInfo<>();
        pageInfo.setPage(page.getNumber() + 1);//page number
        pageInfo.setPageSize(pageSize);//page size
        pageInfo.setRows(CopyUtil.copyList(page.getContent(), entityModelClass));//Page Results
        pageInfo.setRecords(records);//total number of records
        pageInfo.setTotal(total);//Total number of pages
        return pageInfo;
    }

    /**
     * Get the JPA's page target
     */
    public static Page readPage(Query query, Pageable pageable, Query countQuery) {
        if (pageable.isPaged()) {
            query.setFirstResult((int) pageable.getOffset());
            query.setMaxResults(pageable.getPageSize());
        }
        return PageableExecutionUtils.getPage(query.getResultList(), pageable, () -> executeCountQuery(countQuery));
    }

    private static Long executeCountQuery(Query countQuery) {
        Assert.notNull(countQuery, "TypedQuery must not be null!");

        List<Number> totals = countQuery.getResultList();
        Long total = 0L;
        for (Number number : totals) {
            if (number != null) {
                total += number.longValue();
            }
        }
        return total;
    }
}
