package kr.co.ypbooks.nextshop.app.admin.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import kr.co.ypbooks.nextshop.domain.account.dto.MemberDto;
import kr.co.ypbooks.nextshop.domain.account.dto.MemberVo;
import kr.co.ypbooks.nextshop.domain.account.model.Member;
import kr.co.ypbooks.nextshop.domain.account.service.MemberService;
import kr.co.ypbooks.nextshop.domain.account.utils.DateUtils;
import kr.co.ypbooks.nextshop.lib.common.jpa.common.pojo.PageInfo;
import kr.co.ypbooks.nextshop.lib.common.jpa.common.pojo.Result;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Date;
import java.util.List;

@Slf4j
@Api(tags = "Member")
@RequestMapping("/admin/member")
@RestController
public class MemberController {

    @Autowired
    MemberService memberService;

    @ApiOperation("memberList")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "startRegDate", paramType = "query",dataType = "String", example = "2022-03-01"),
            @ApiImplicitParam(name = "endRegDate", paramType = "query",dataType = "String", example = "2022-03-31"),
            @ApiImplicitParam(name = "memberState", paramType = "query", dataType = "String", example = "(0:탈퇴,1:일반,4:휴먼,null:오프라인회원)"),
            @ApiImplicitParam(name = "name", paramType = "query", dataType = "String", example = ""),
            @ApiImplicitParam(name = "memberId", paramType = "query", dataType = "String", example = ""),
            @ApiImplicitParam(name = "memberCd", paramType = "query", dataType = "String", example = ""),
            @ApiImplicitParam(name = "phone", paramType = "query", dataType = "String", example = ""),
            @ApiImplicitParam(name = "email", paramType = "query", dataType = "String", example = ""),
            @ApiImplicitParam(name = "pageNum", paramType = "query", required = true, dataType = "Integer", example = "1"),
            @ApiImplicitParam(name = "pageSize", paramType = "query", required = true, dataType = "Integer", example = "10")
    })
    @RequestMapping(value = "/memberList", method = RequestMethod.GET)
    public MemberDto memberList(@ApiIgnore MemberDto memberDto, Integer pageNum, Integer pageSize){

        Page<Member> result = memberService.getMemberList(memberDto, pageNum, pageSize);

        return MemberDto.builder().pageNum(pageNum).pageSize(pageSize)
                .totalSize((int) result.getTotalElements())
                .data(result.getContent()).build();

    }

    @ApiOperation("memberBlock")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "memberIds", paramType = "query", dataType = "String", example = "")
    })
    @RequestMapping(value = "/memberBlock", method = RequestMethod.GET)
    public boolean memberBlock(String memberIds, Integer pageNum, Integer pageSize){

        // 차단 테이블 생성후 작업

        return true;

    }


    @ApiOperation("getMember")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "memberId", paramType = "path", required = true, dataType = "String", example = "")
    })
    @RequestMapping(value = "/getMember/{memberId}", method = RequestMethod.GET)
    public MemberDto getMember(@PathVariable String memberId){

        Result<MemberVo> memberVoResult = memberService.get(memberId);

        return MemberDto.builder().data(memberVoResult.getData()).build();

    }

    @ApiOperation(value = "member" , notes = "update")
    @ApiImplicitParam(name = "member", value = "memberInfo", dataType = "MemberVo", required = true)
    @RequestMapping(value = "/updateMember", method = RequestMethod.PUT)
    public MemberDto updateMember(@RequestBody MemberVo member){

        Result<MemberVo> result = memberService.save(member);
        MemberVo data = result.getData();

        return MemberDto.builder().data(data).build();
    }

    @ApiOperation(value = "member" , notes = "insert")
    @ApiImplicitParam(name = "member", value = "memberInfo", dataType = "MemberVo", required = true)
    @RequestMapping(value = "/insertMember", method = RequestMethod.POST)
    public MemberDto insertMember(@RequestBody MemberVo member){

        member.setRegDate(DateUtils.asLocalDateTime(new Date()));
        Result<MemberVo> result = memberService.save(member);
        MemberVo data = result.getData();

        return MemberDto.builder().data(data).build();
    }
}
