package kr.co.ypbooks.nextshop.domain.account.oauth.cache;

public class AuthCacheConfig {

    public static long timeout = 3 * 60 * 1000;

    public static boolean schedulePrune = true;
}