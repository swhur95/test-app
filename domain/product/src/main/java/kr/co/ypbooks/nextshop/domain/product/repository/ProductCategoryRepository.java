package kr.co.ypbooks.nextshop.domain.product.repository;

import kr.co.ypbooks.nextshop.domain.product.model.ProductCategory;
import kr.co.ypbooks.nextshop.domain.product.model.ProductCategoryId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductCategoryRepository extends JpaRepository<ProductCategory, ProductCategoryId> {
}
