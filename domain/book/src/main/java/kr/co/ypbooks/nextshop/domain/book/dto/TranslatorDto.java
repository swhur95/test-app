package kr.co.ypbooks.nextshop.domain.book.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public class TranslatorDto {
    /**
     * 역자 고유 번호
     */
    @ApiModelProperty(value = "역자 고유 번호", example = "12345678")
    @JsonProperty("trans_no")
    private Long transNo;

    /**
     * 역자 명
     */
    @ApiModelProperty(value = "역자명", example = "역자명")
    @JsonProperty("trans_name")
    private String transName;

    /**
     * 역자 분야 코드 목록
     */
    @ApiModelProperty(value = "역자 분야 코드 목록", example = "[\"010201\", \"010301\"]")
    @JsonProperty("trans_category_cd_list")
    private List<String> transCategoryCdList;

    /**
     * 역자 태그 단어 목록
     */
    @ApiModelProperty(value = "역자 태그 단어 목록", example = "[\"현대소설가\", \"신인역자\"]")
    @JsonProperty("trans_tag_word_list")
    private List<String> transTagWordList;

    /**
     * 역자 정보
     */
    @ApiModelProperty(value = "역자 정보")
    @JsonProperty("trans_info")
    private TranslatorInfo transInfo;

    /**
     * 등록 일시
     */
    @ApiModelProperty(value = "등록 일시", example = "2022-03-22 09:00:00")
    @JsonProperty("reg_date")
    private LocalDateTime regDate;

    /**
     * 수정 일시
     */
    @ApiModelProperty(value = "수정 일시", example = "2022-03-22 09:00:00")
    @JsonProperty("upd_date")
    private LocalDateTime updDate;

    @Getter
    public static class TranslatorInfo {
        /**
         * 역자 이미지명
         */
        @ApiModelProperty(value = "역자 이미지명", example = "역자 이미지명")
        @JsonProperty("trans_img_name")
        private String transImgName;

        /**
         * 역자 이미지 경로
         */
        @ApiModelProperty(value = "역자 이미지 경로", example = "역자 이미지 경로")
        @JsonProperty("trans_img_path")
        private String transImgPath;

        /**
         * 데뷔 연도
         */
        @ApiModelProperty(value = "역자 데뷔 연도", example = "2022")
        @JsonProperty("debut_year")
        private String debutYear;

        /**
         * 데뷔 내용 (작품 또는 직접 입력)
         */
        @ApiModelProperty(value = "역자 데뷔 내용 (작품)", example = "데뷔작품")
        @JsonProperty("debut_content")
        private String debutContent;

        /**
         * 생년월일
         */
        @ApiModelProperty(value = "생년월일", example = "20000222")
        @JsonProperty("birth_date")
        private LocalDate birthDate;

        /**
         * 역자 내용(설명)
         */
        @ApiModelProperty(value = "역자 내용(설명)", example = "역자 설명")
        @JsonProperty("trans_content")
        private String transContent;
    }
}
