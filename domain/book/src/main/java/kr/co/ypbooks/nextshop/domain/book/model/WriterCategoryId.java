package kr.co.ypbooks.nextshop.domain.book.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class WriterCategoryId implements Serializable {
    /**
     * 작가 고유 번호
     */
    private Long writerNo;

    /**
     * 작가 분야 코드
     */
    private String writerCategoryCd;
}
