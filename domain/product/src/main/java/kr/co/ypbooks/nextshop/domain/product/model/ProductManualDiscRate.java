package kr.co.ypbooks.nextshop.domain.product.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
@Entity
public class ProductManualDiscRate {
    /**
     * 상품 Id
     */
    @EmbeddedId
    private ProductId productId;

    /**
     * 등록한 관리자 Id
     */
    private String adminId;

    /**
     * 시작 일시
     */
    private LocalDateTime startDate;

    /**
     * 종료 일시
     */
    private LocalDateTime endDate;

    /**
     * 할인율
     */
    private Byte discRate;

    /**
     * 적립율
     */
    private Byte reserveRate;

    /**
     * 등록 일시
     */
    private LocalDateTime regDate;

    /**
     * 수정 일시
     */
    private LocalDateTime updDate;
}
