package kr.co.ypbooks.nextshop.domain.shop.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
@Entity
public class Branch {
    /**
     * 지점 코드 (고유값)
     */
    @Id
    private String branchCd;

    /**
     * 지점명
     */
    private String branchName;

    /**
     * 지점 지역 코드 (01: 서울, 02: 인천/경기, 03: 대전/충청 등)
     */
    private String branchRegionCd;

    /**
     * 관리 유형 (직영, 롯데백화점, 이마트)
     */
    private String branchMgmtType;

    /**
     * 운영 유형 (임대, 수수료)
     */
    private String branchOpnType;

    /**
     * 지점 상태 코드 (0:개점, 1:폐점, 2: 개점 (O2O 제외)
     */
    private String branchStateCd;

    /**
     * 지점 정보
     */
    private String branchInfo;

    /**
     * 관리자 정보
     */
    private String adminInfo;

    /**
     * 영업 시간 정보
     */
    private String bizTimeInfo;

    /**
     * Map 정보
     */
    private String mapInfo;

    /**
     * 연락처 정보
     */
    private String contactInfo;

    /**
     * 오시는 길 정보
     */
    private String directionsInfo;

    /**
     * 가이드 맵 정보
     */
    private String guideMapInfo;

    /**
     * 등록/가입일자
     */
    private LocalDateTime regDate;

    /**
     * 수정일자
     */
    private LocalDateTime updDate;
}
