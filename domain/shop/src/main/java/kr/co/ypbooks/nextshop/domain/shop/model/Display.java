package kr.co.ypbooks.nextshop.domain.shop.model;

import com.vladmihalcea.hibernate.type.json.JsonStringType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Entity

@Table(name = "DISPLAY")
@DynamicUpdate
@TypeDef(name = "json", typeClass = JsonStringType.class)
public class Display {
    /**
     * 전시 번호 (고유키)
     */
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer displayNo;

    /**
     * 전시 제목 (고유키)
     */
    private String displaySubject;

    /**
     * 등록한 관리자 ID
     */
    private String regAdminId;

    /**
     * 등록한 관리자명
     */
    private String regAdminName;

    /**
     * 노출 여부 (0: 비노출, 1: 노출)
     */
    private Boolean exposeFlag;

    /**
     * 전시 시작 일시
     */
    private LocalDateTime displayStartDate;

    /**
     * 전시 종료 일시 (null인 경우 종료 없음)
     */
    private LocalDateTime displayEndDate;

    /**
     * 전시 정보 (Json)
     */
    @Type(type = "json")
    @Column(columnDefinition = "json")
    private String displayInfo;

    /**
     * 등록 일시
     */
    private LocalDateTime regDate;

    /**
     * 수정 일시
     */
    private LocalDateTime updDate;
}
