package kr.co.ypbooks.nextshop.domain.shop.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;

import java.time.LocalDateTime;

@ApiModel
@Getter
@Builder
public class BranchImageDto {
    /**
     * 지점 이미지 번호 (고유 번호)
     */
    @ApiModelProperty(value = "지점 이미지 번호", example = "1")
    @JsonProperty("branch_img_no")
    private Long branchImgNo;

    /**
     * 지점 코드
     */
    @ApiModelProperty(value = "지점 코드 (고유값)", example = "12345678", required = true)
    @JsonProperty("branch_cd")
    private String branchCd;

    /**
     * 지점명
     */
    @ApiModelProperty(value = "지점명", example = "종로 본점", required = true)
    @JsonProperty("branch_name")
    private String branchName;

    /**
     * 지점 이미지 파일명
     */
    @ApiModelProperty(value = "지점 이미지명", example = "지점 이미지명", required = true)
    @JsonProperty("branch_img_name")
    private String branchImgName;

    /**
     * 지점 이미지 경로
     */
    @ApiModelProperty(value = "지점 이미지 경로", example = "지점 이미지 경로", required = true)
    @JsonProperty("branch_img_path")
    private String branchImgPath;

    /**
     * 지점 이미지 내용 (설명)
     */
    @ApiModelProperty(value = "지점 이미지 내용", example = "지점 이미지 내용")
    @JsonProperty("branch_img_content")
    private String branchImgContent;

    /**
     * 등록한 관리자 ID
     */
    @ApiModelProperty(value = "등록한 관리자 ID", example = "admin")
    @JsonProperty("reg_admin_id")
    private String regAdminId;

    /**
     * 등록한 관리자명
     */
    @ApiModelProperty(value = "등록한 관리자명", example = "관리자")
    @JsonProperty("reg_admin_name")
    private String regAdminName;

    /**
     * 등록 일시
     */
    @ApiModelProperty(value = "등록 일시", example = "2022-03-22 09:00:00", required = true)
    @JsonProperty("reg_date")
    private LocalDateTime regDate;

    /**
     * 수정 일시
     */
    @ApiModelProperty(value = "수정 일시", example = "2022-03-22 09:00:00")
    @JsonProperty("upd_date")
    private LocalDateTime updDate;
}
