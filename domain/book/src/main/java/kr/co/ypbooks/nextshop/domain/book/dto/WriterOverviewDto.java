package kr.co.ypbooks.nextshop.domain.book.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Builder
public class WriterOverviewDto {
    /**
     * 작가 고유 번호
     */
    @ApiModelProperty(value = "작가 고유 번호", example = "12345678")
    @JsonProperty("writer_no")
    private Long writerNo;

    /**
     * 작가 명
     */
    @ApiModelProperty(value = "작가명", example = "작가명")
    @JsonProperty("writer_name")
    private String writerName;

    /**
     * 작가명 (기타 언어)
     */
    @ApiModelProperty(value = "작가명 (기타 언어)", example = "작가명 (기타 언어")
    @JsonProperty("writer_name_lang")
    private String writerNameLang;

    /**
     * 작가 분야 코드 목록
     */
    @ApiModelProperty(value = "작가 분야 코드 목록", example = "[\"010201\", \"010301\"]")
    @JsonProperty("writer_category_cd_list")
    private List<String> writerCategoryCdList;

    /**
     * 국내 여부
     */
    @ApiModelProperty(value = "국내 여부", example = "1")
    @JsonProperty("domestic_flag")
    private Boolean domesticFlag;

    /**
     * 출생지
     */
    @ApiModelProperty(value = "출생지", example = "한국")
    @JsonProperty("birth_place")
    private String birthPlace;

    /**
     * 생년월일
     */
    @ApiModelProperty(value = "생년월일", example = "20000222")
    @JsonProperty("birth_date")
    private LocalDate birthDate;

    /**
     * 등록 일시
     */
    @ApiModelProperty(value = "등록 일시", example = "2022-03-22 09:00:00")
    @JsonProperty("reg_date")
    private LocalDateTime regDate;

    /**
     * 수정 일시
     */
    @ApiModelProperty(value = "수정 일시", example = "2022-03-22 09:00:00")
    @JsonProperty("upd_date")
    private LocalDateTime updDate;
}
