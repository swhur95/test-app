package kr.co.ypbooks.nextshop.domain.product.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class ProductId implements Serializable {
    /**
     * 상품 유형 코드 (B: 도서, E: E-Book)
     */
    private String productTypeCd;

    /**
     * 상품 코드
     */
    private String productCd;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductId productId = (ProductId) o;
        return Objects.equals(productTypeCd, productId.productTypeCd) && Objects.equals(productCd, productId.productCd);
    }

    @Override
    public int hashCode() {
        return Objects.hash(productTypeCd, productCd);
    }
}
