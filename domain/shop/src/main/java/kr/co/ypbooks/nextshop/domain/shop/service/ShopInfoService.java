package kr.co.ypbooks.nextshop.domain.shop.service;

import kr.co.ypbooks.nextshop.domain.shop.model.ShopInfo;
import kr.co.ypbooks.nextshop.domain.shop.repository.ShopInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ShopInfoService {

    @Autowired
    private ShopInfoRepository shopInfoRepository;

    /**
     * selectById
     *
     * @param id
     */
    public ShopInfo selectById(Long id) {
        Optional<ShopInfo> optional = shopInfoRepository.findById(id);
        ShopInfo shopInfo = optional.get();
        return shopInfo;
    }

    /**
     * selectByKey
     *
     * @param key
     * @param value
     */
    public List<ShopInfo> selectByKey(String key, String value) {
        List<ShopInfo> list = shopInfoRepository.selectByKey(key, value);
        return list;
    }

    public ShopInfo addShopInfo(ShopInfo shopInfo) {
        return shopInfoRepository.save(shopInfo);
    }

}
