package kr.co.ypbooks.nextshop.lib.common.config;

import kr.co.ypbooks.nextshop.lib.common.kafka.KfakaConsumerRunnable;
import kr.co.ypbooks.nextshop.lib.common.util.JsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.KafkaAdmin;

import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@Configuration
public class KafkaConfig {

    public static Map<String, KfakaConsumerRunnable> KFAKACONSUMERRUNNABLE_POOL = new ConcurrentHashMap<String, KfakaConsumerRunnable>();

    @Value("${spring.kafka.bootstrap-servers}")
    private String bootstrapServers;

    @Value("${spring.kafka.consumer.groupId}")
    private String dataGroupConsumerGroupId;

    @Autowired
    ConsumerFactory consumerFactory;

    /**
     * consumerConfigs
     */
    public Map<String, Object> consumerConfigs() {
        Map<String, Object> propsMap = new  HashMap<String, Object>();

        propsMap.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        propsMap.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, false);
        propsMap.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, 1000);
        propsMap.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, 100000);
        propsMap.put(ConsumerConfig.REQUEST_TIMEOUT_MS_CONFIG, 110000);
        propsMap.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        propsMap.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        propsMap.put(ConsumerConfig.GROUP_ID_CONFIG, dataGroupConsumerGroupId);
        propsMap.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        propsMap.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, 100);
        propsMap.put(ConsumerConfig.FETCH_MAX_WAIT_MS_CONFIG, 5000);
        return propsMap;
    }

    /**
     * Create a kafka kafkaAdmin，equivalent to rabbitMQ Managing rabbitAdmin
     * Cannot create topic without this Bean using custom adminClient
     */
    @Bean
    public KafkaAdmin kafkaAdmin() {
        Map<String, Object> props = new HashMap<>();
        //Set connection address for Kafka instance
        props.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        KafkaAdmin admin = new KafkaAdmin(props);
        return admin;
    }

    /**
     * kafka client，create this Bean in spring you can inject and create topic
     */
    @Bean
    public AdminClient adminClient() {
        return AdminClient.create(kafkaAdmin().getConfigurationProperties());
    }

    /**
     * kafka consumer filter
     */
    @Bean
    public ConcurrentKafkaListenerContainerFactory filterContainerFactoryByIP() throws Exception{

        ConcurrentKafkaListenerContainerFactory factory = new ConcurrentKafkaListenerContainerFactory();
        String IP = InetAddress.getLocalHost().getHostAddress();

        factory.setConsumerFactory(consumerFactory);
        // Filtered messages are discarded
        factory.setAckDiscarded(true);
        // Message filtering policy

        factory.setRecordFilterStrategy(consumerRecord -> {

            //log.info("==value====> {}",consumerRecord.value().toString());
            Map jsonmap = null;
            try {
                jsonmap = JsonUtil.jsonToMap(consumerRecord.value().toString());
                if (IP.equals(jsonmap.get("IP"))) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            //True messages are filtered when returned
            return true;
        });
        return factory;
    }

}
