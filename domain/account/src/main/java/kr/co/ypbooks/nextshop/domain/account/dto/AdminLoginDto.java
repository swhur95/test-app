package kr.co.ypbooks.nextshop.domain.account.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;

@ApiModel
@Getter
@Builder
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class AdminLoginDto {
    /**
     * 관리자 ID
     */
    @ApiModelProperty(value = "관리자ID", example = "admin", required = true)
    private String adminId;

    /**
     * 관리자 패스워드
     */
    @ApiModelProperty(value = "관리자비밀번호", example = "password", required = true)
    private String adminPwd;
}
