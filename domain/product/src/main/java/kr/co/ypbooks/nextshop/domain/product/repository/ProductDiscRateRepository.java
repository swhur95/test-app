package kr.co.ypbooks.nextshop.domain.product.repository;

import kr.co.ypbooks.nextshop.domain.product.model.ProductDiscRate;
import kr.co.ypbooks.nextshop.domain.product.model.ProductId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductDiscRateRepository extends JpaRepository<ProductDiscRate, ProductId> {
}
