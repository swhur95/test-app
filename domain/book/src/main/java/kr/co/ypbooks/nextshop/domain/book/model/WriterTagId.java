package kr.co.ypbooks.nextshop.domain.book.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class WriterTagId implements Serializable {
    /**
     * 작가 고유 번호
     */
    private Long writerNo;

    /**
     * 태그 단어
     */
    private String tagWord;
}
