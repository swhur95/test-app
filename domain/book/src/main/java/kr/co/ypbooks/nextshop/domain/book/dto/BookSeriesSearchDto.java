package kr.co.ypbooks.nextshop.domain.book.dto;

import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.Getter;

@ApiModel
@Getter
@Builder
public class BookSeriesSearchDto {
    /**
     * 시리즈 번호
     */
    private Long seriesNo;

    /**
     * 시리즈명 조건
     */
    private String seriesName;
}
