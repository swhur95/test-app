package kr.co.ypbooks.nextshop.domain.account;

import kr.co.ypbooks.nextshop.domain.account.dto.AccountDto;
import kr.co.ypbooks.nextshop.domain.account.model.Account;
import kr.co.ypbooks.nextshop.domain.account.service.AccountService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("alpha")
public class AccountOperationTest {

    @Autowired
    private AccountService accountService;

    @Test
    public void testCreateUser() throws Exception{
        Account account = new Account(null, "Tom", "123");
        account.setId(1L);
        account.setCountryCd("UK");
        AccountDto accountDto = accountService.addAccountMock(account);
        System.err.println(accountDto.getUserId());
    }
}
