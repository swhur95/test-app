package kr.co.ypbooks.nextshop.domain.book.repository;

import kr.co.ypbooks.nextshop.domain.book.model.TranslatorCategory;
import kr.co.ypbooks.nextshop.domain.book.model.TranslatorCategoryId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TranslatorCategoryRepository extends JpaRepository<TranslatorCategory, TranslatorCategoryId> {
}
