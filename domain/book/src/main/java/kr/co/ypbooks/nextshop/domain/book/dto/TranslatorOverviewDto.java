package kr.co.ypbooks.nextshop.domain.book.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@ApiModel
@Getter
public class TranslatorOverviewDto {
    /**
     * 역자 고유 번호
     */
    @ApiModelProperty(value = "역자 고유 번호", example = "12345678")
    @JsonProperty("trans_no")
    private Long transNo;

    /**
     * 역자 명
     */
    @ApiModelProperty(value = "역자명", example = "역자명")
    @JsonProperty("trans_name")
    private String transName;

    /**
     * 역자 분야 코드 목록
     */
    @ApiModelProperty(value = "역자 분야 코드 목록", example = "[\"010201\", \"010301\"]")
    @JsonProperty("trans_category_cd_list")
    private List<String> transCategoryCdList;

    /**
     * 생년월일
     */
    @ApiModelProperty(value = "생년월일", example = "20000222")
    @JsonProperty("birth_date")
    private LocalDate birthDate;

    /**
     * 등록 일시
     */
    @ApiModelProperty(value = "등록 일시", example = "2022-03-22 09:00:00")
    @JsonProperty("reg_date")
    private LocalDateTime regDate;

    /**
     * 수정 일시
     */
    @ApiModelProperty(value = "수정 일시", example = "2022-03-22 09:00:00")
    @JsonProperty("upd_date")
    private LocalDateTime updDate;
}
