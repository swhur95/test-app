package kr.co.ypbooks.nextshop.app.admin.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import kr.co.ypbooks.nextshop.lib.common.dto.ResultDto;
import kr.co.ypbooks.nextshop.domain.shop.dto.*;
import kr.co.ypbooks.nextshop.domain.shop.service.BranchService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@Slf4j
@Api(tags = {"Branch"})
@RequestMapping("/admin/branch")
@RestController
public class BranchController {
    private final BranchService branchService;

    public BranchController(BranchService branchService) {
        this.branchService = branchService;
    }

    /**
     * 지점 정보 개요 목록을 얻어오는 함수
     * @param displaySearchDto  지점 정보 검색 조건
     * @return                  지점 정보 목록
     */
    @ApiOperation(value = "지점 정보 개요 목록" , notes = "지점 정보 개요 목록을 얻어옵니다.")
    @GetMapping("")
    public ResultDto getBranchOverview(@RequestBody BranchSearchDto displaySearchDto) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    @ApiOperation(value = "지점 정보" , notes = "지점 정보를 얻어옵니다.")
    @GetMapping("/{branch_cd}")
    public ResultDto getBranch(@PathVariable("branch_cd") String branchCd) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    @ApiOperation(value = "지점 정보 설정" , notes = "지점 정보를 설정합니다.")
    @PostMapping("")
    public ResultDto insertBranch(@RequestBody BranchDto branchDto) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    @ApiOperation(value = "지점 정보 수정" , notes = "지점 정보를 수정합니다.")
    @PutMapping("/{branch_cd}")
    public ResultDto updateBranch(@PathVariable("branch_cd") String branchCd) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    @ApiOperation(value = "지점 정보 삭제" , notes = "지점 정보를 삭제합니다.")
    @DeleteMapping("/{branch_cd}")
    public ResultDto deleteBranch(@PathVariable("branch_cd") String branchCd) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    @ApiOperation(value = "지점 휴무 정보" , notes = "지점 휴무 정보를 얻어옵니다.")
    @GetMapping("/dayoff")
    public ResultDto getBranchDayOff(@RequestBody BranchDayOffSearchDto branchDayOffSearchDto) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    @ApiOperation(value = "지점 휴무 정보" , notes = "지점 휴무 정보를 얻어옵니다.")
    @GetMapping("/dayoff/{branch_day_off_no}")
    public ResultDto getBranchDayOff(@PathVariable("branch_day_off_no") Long branchDayOffNo) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    @ApiOperation(value = "지점 휴무 정보 추가" , notes = "지점 휴무 정보를 추가합니다.")
    @PostMapping("/dayoff")
    public ResultDto insertBranchDayOff(@RequestBody BranchDayOffDto branchDayOffDto) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    @ApiOperation(value = "지점 휴무 정보 수정" , notes = "지점 휴무 정보를 수정합니다.")
    @PutMapping("/dayoff/{branch_day_off_no}")
    public ResultDto updateBranchDayOff(@PathVariable("branch_day_off_no") Long branchDayOffNo, BranchDayOffDto branchDayOffDto) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    @ApiOperation(value = "지점 휴무 정보 삭제" , notes = "지점 휴무 정보를 삭제합니다.")
    @DeleteMapping("/dayoff/{branch_day_off_no}")
    public ResultDto deleteBranchDayOff(@PathVariable("branch_day_off_no") Long branchDayOffNo) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    @ApiOperation(value = "지점 매장 사진 정보" , notes = "지점 매장 사진 정보를 얻어옵니다.")
    @GetMapping("/image")
    public ResultDto getBranchImage(@RequestBody BranchImageSearchDto branchImageSearchDto) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    @ApiOperation(value = "지점 매장 사진 정보" , notes = "지점 매장 사진 정보를 얻어옵니다.")
    @GetMapping("/image/{branch_img_no}")
    public ResultDto getBranchImage(@PathVariable("branch_img_no") Long branchImgNo) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    @ApiOperation(value = "지점 매장 사진 정보 추가" , notes = "지점 매장 사진 정보를 추가합니다.")
    @PostMapping("/image")
    public ResultDto insertBranchImage(@RequestBody BranchImageDto branchImageDto) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    @ApiOperation(value = "지점 매장 사진 정보 수정" , notes = "지점 매장 사진 정보를 수정합니다.")
    @PutMapping("/image/{branch_img_no}")
    public ResultDto updateBranchImage(@PathVariable("branch_img_no") Long branchImgNo, @RequestBody BranchImageDto branchImageDto) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    @ApiOperation(value = "지점 매장 사진 정보 삭제" , notes = "지점 매장 사진 정보를 삭제합니다.")
    @DeleteMapping("/image/{branch_img_no}")
    public ResultDto deleteBranchImage(@PathVariable("branch_img_no") Long branchImgNo) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    @ApiOperation(value = "지점 가이드 맵 조회" , notes = "지점 가이드 맵을 조회합니다.")
    @GetMapping("/guidemap/{branch_cd}")
    public ResultDto getBranchGuidemap(@PathVariable("branch_cd") String branchCd) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    @ApiOperation(value = "지점 가이드 맵 추가" , notes = "지점 가이드 맵을 추가합니다.")
    @PostMapping("/guidemap/{branch_cd}")
    public ResultDto insertBranchGuidemap(@PathVariable("branch_cd") String branchCd, BranchDto.GuideMapInfo guideMapInfo) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    @ApiOperation(value = "지점 가이드 맵 수정" , notes = "지점 가이드 맵을 수정합니다.")
    @PutMapping("/guidemap/{branch_cd}")
    public ResultDto updateBranchGuidemap(@PathVariable("branch_cd") String branchCd, BranchDto.GuideMapInfo guideMapInfo) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    @ApiOperation(value = "지점 가이드 맵 삭제" , notes = "지점 가이드 맵을 삭제합니다.")
    @DeleteMapping("/guidemap/{branch_cd}")
    public ResultDto deleteBranchGuidemap(@PathVariable("branch_cd") String branchCd) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }
}
