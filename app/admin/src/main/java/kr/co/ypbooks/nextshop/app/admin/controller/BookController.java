package kr.co.ypbooks.nextshop.app.admin.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import kr.co.ypbooks.nextshop.domain.book.dto.*;
import kr.co.ypbooks.nextshop.lib.common.dto.ResultDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@Slf4j
@Api(tags = {"Book"})
@RequestMapping("/admin/book")
@RestController
public class BookController {
    @ApiOperation(value = "도서 정보 개요 목록" , notes = "도서 정보 개요 목록을 얻어옵니다.")
    @GetMapping("")
    public ResultDto getBookOverview(@RequestBody BookSearchDto bookSearchDto) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    @ApiOperation(value = "도서 정보 조회" , notes = "도서 정보를 얻어옵니다.")
    @GetMapping("/{book_cd}")
    public ResultDto getBook(@PathVariable("book_cd") String bookCd) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    @ApiOperation(value = "도서 정보 등록" , notes = "도서 정보를 등록합니다.")
    @PostMapping("/{book_cd}")
    public ResultDto insertBook(@PathVariable("book_cd") String bookCd) {
        // book code로 SAP에서 도서 정보 조회
        // 얻어온 도서 정보로 상품 정보를 추가한다.
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    @ApiOperation(value = "도서 정보 수정" , notes = "도서 정보를 수정합니다.")
    @PutMapping("/{book_cd}")
    public ResultDto updateBook(@PathVariable("book_cd") String bookCd, @RequestBody BookDto bookDto) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    @ApiOperation(value = "도서 정보 삭제" , notes = "도서 정보를 삭제합니다.")
    @DeleteMapping("/{book_cd}")
    public ResultDto deleteBook(@PathVariable("book_cd") String bookCd) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    @ApiOperation(value = "도서 시리즈 개요 목록 조회" , notes = "도서 시리즈 개요 목록을 얻어옵니다.")
    @GetMapping("/series")
    public ResultDto getBookSeriesOverview(@RequestBody BookSeriesSearchDto bookSeriesSearchDto) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    @ApiOperation(value = "도서 시리즈 정보 추가" , notes = "도서 시리즈 정보를 추가합니다.")
    @PostMapping("/series")
    public ResultDto insertBookSeries(@RequestBody BookSeriesDto bookSeriesDto) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    @ApiOperation(value = "도서 시리즈 조회" , notes = "도서 시리즈 정보를 얻어옵니다.")
    @GetMapping("/series/{series_no}")
    public ResultDto getBookSeries(@PathVariable("series_no") Long seriesNo) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    @ApiOperation(value = "도서 시리즈 수정" , notes = "도서 시리즈 정보를 수정합니다.")
    @PutMapping("/series/{series_no}")
    public ResultDto updateBookSeries(@PathVariable("series_no") Long seriesNo, @RequestBody BookSeriesDto bookSeriesDto) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    @ApiOperation(value = "도서 시리즈에 도서 추가" , notes = "도서 시리즈 도서를 추가합니다.")
    @PostMapping("/series/{series_no}")
    public ResultDto insertBookSeries(@PathVariable("series_no") Long seriesNo, @RequestParam("book_cd") String bookCd) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    @ApiOperation(value = "도서 시리즈 삭제" , notes = "도서 시리즈를 삭제합니다.")
    @DeleteMapping("/series/{series_no}")
    public ResultDto deleteBookSeries(@PathVariable("series_no") Long seriesNo) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    @ApiOperation(value = "광고 도서 조회" , notes = "광고 도서 목록을 얻어옵니다.")
    @GetMapping("/ad")
    public ResultDto getBookAd(@RequestBody BookAdSearchDto bookAdSearchDto) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    @ApiOperation(value = "광고 도서 추가" , notes = "광고 도서를 추가합니다.")
    @PostMapping("/ad")
    public ResultDto insertBookAd(@RequestBody BookAdDto bookAdDto) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    @ApiOperation(value = "광고 도서 정보 수정" , notes = "광고 도서 정보를 수정합니다.")
    @PutMapping("/ad/{book_cd}")
    public ResultDto updateBookAd(@PathVariable("book_cd") String bookCd, @RequestBody BookAdDto bookAdDto) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    @ApiOperation(value = "광고 도서 정보 삭제" , notes = "광고 도서 정보를 삭제합니다.")
    @DeleteMapping("/ad/{book_cd}")
    public ResultDto deleteBookAd(@PathVariable("book_cd") String bookCd) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }
}
