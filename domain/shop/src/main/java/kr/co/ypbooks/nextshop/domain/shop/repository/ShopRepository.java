package kr.co.ypbooks.nextshop.domain.shop.repository;

import kr.co.ypbooks.nextshop.domain.shop.model.Shop;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ShopRepository extends JpaRepository<Shop, Long> {

    @Query(value = "SELECT * FROM shop WHERE name like CONCAT('%',:#{#sp.name},'%')",
            countQuery = "SELECT count(*) FROM shop WHERE name like CONCAT('%',:#{#sp.name},'%')",
            nativeQuery = true)
    Page<Shop> findShopsByPage(Shop sp, Pageable pageable);
}

