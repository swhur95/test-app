package kr.co.ypbooks.nextshop.domain.order.repository;

import kr.co.ypbooks.nextshop.domain.order.model.ReserveHist;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReserveRepository extends JpaRepository<ReserveHist, Long> {
}
