package kr.co.ypbooks.nextshop.domain.order.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;

import java.time.LocalDateTime;
import java.util.List;

@ApiModel
@Getter
@Builder
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ConsultDto {
    /**
     * 상담 고유 번호
     */
    @ApiModelProperty(value = "상담 고유 번호", example = "12345")
    private Long consultNo;

    /**
     * 회원 유형 코드 (1: 북클럽, 2: 계열사)
     */
    @ApiModelProperty(value = "회원 유형 코드", example = "1")
    private String memberTypeCd;

    /**
     * 회원 ID
     */
    @ApiModelProperty(value = "회원 ID", example = "회원 ID")
    private String memberId;

    /**
     * 회원 이름
     */
    @ApiModelProperty(value = "회원 이름", example = "회원 이름")
    private String memberName;

    /**
     * 회원 전화번호
     */
    @ApiModelProperty(value = "회원 집전화", example = "회원 집전화")
    private String memberPhone;

    /**
     * 회원 휴대전화
     */
    @ApiModelProperty(value = "회원 휴대전화", example = "회원 휴대전화")
    private String memberMobile;

    /**
     * 회원 이메일
     */
    @ApiModelProperty(value = "회원 이메일", example = "회원 이메일")
    private String memberEmail;

    /**
     * 주문 번호
     */
    @ApiModelProperty(value = "주문 번호", example = "12345")
    private Long orderNo;

    /**
     * 관리자 ID
     */
    @ApiModelProperty(value = "관리자 ID", example = "admin")
    private String adminId;

    /**
     * 관리자 이메일
     */
    @ApiModelProperty(value = "관리자 이메일", example = "admin@example.com")
    private String adminEmail;

    /**
     * 상담 분류 코드
     */
    @ApiModelProperty(value = "상담 분류 코드", example = "1")
    private String consultTypeCd;

    /**
     * 상담 제목
     */
    @ApiModelProperty(value = "상담 제목", example = "상담 제목")
    private String consultSubject;

    /**
     * 상담 내용
     */
    @ApiModelProperty(value = "상담 내용", example = "상담 내용")
    private String consultContent;

    /**
     * 상담 상태 코드
     */
    @ApiModelProperty(value = "상담 상태 코드", example = "상담 상태 코드")
    private String consultStateCd;

    /**
     * 상담 시간 (Second)
     */
    @ApiModelProperty(value = "상담 시간", example = "상담 시간")
    private Integer consultTime;

    /**
     * 상담 처리 수단 코드
     */
    @ApiModelProperty(value = "상담 처리 수단 코드", example = "상담 처리 수단 코드")
    private String consultProcMethodCd;

    /**
     * 상담 처리 내용
     */
    @ApiModelProperty(value = "상담 처리 내용", example = "상담 처리 내용")
    private String consultProcContent;

    /**
     * 상담 답변
     */
    @ApiModelProperty(value = "상담 답변", example = "상담 답변")
    private String consultAnswer;

    /**
     * 상담 완료 일시
     */
    @ApiModelProperty(value = "상담 완료 일시", example = "상담 완료 일시")
    private LocalDateTime consultCompDate;

    /**
     * 의견
     */
    @ApiModelProperty(value = "상담 의견", example = "상담 의견")
    private String comment;

    /**
     * 추가 정보
     */
    @ApiModelProperty(value = "추가 정보")
    private AddInfo addInfo;

    /**
     * 등록 일시
     */
    @ApiModelProperty(value = "등록 일시", example = "등록 일시")
    private LocalDateTime regDate;

    /**
     * 수정 일시
     */
    @ApiModelProperty(value = "수정 일시", example = "수정 일시")
    private LocalDateTime updDate;

    /**
     * 추가 정보
     */
    @Getter
    public static class AddInfo {
        /**
         * 상담 답변 이메일 수신 여부
         */
        @ApiModelProperty(value = "답변 이메일 수신 여부", example = "1")
        private Boolean answerEmailFlag;

        /**
         * 상담 완료 SMS 수신 여부
         */
        @ApiModelProperty(value = "SMS 수신 여부", example = "1")
        private Boolean notifySmsFlag;

        /**
         * 직장 전화 번호
         */
        @ApiModelProperty(value = "회사 전화 번호", example = "0212345678")
        private String companyPhone;

        /**
         * 상담 첨부 이미지 정보들
         */
        @ApiModelProperty(value = "첨부 이미지 정보들")
        private List<ConsultImgInfo> consultImgInfos;

        private static class ConsultImgInfo {
            /**
             * 상담 이미지명
             */
            @ApiModelProperty(value = "상담 이미지명", example = "상담 이미지명")
            private String consultImgName;

            /**
             * 상담 이미지 경로
             */
            @ApiModelProperty(value = "상담 이미지 경로", example = "상담 이미지 경로")
            private String consultImgPath;
        }
    }
}
