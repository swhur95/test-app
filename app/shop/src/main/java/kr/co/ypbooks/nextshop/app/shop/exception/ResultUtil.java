package kr.co.ypbooks.nextshop.app.shop.exception;

public class ResultUtil {

    /*
    * return error message
    * @param message
    * @return
    */
    public static ResultBean error(String message){
        return ResultBean.builder()
                .code(ResultEnum.FAIL.getCode())
                .message(message)
                .build();
    }

    /*
     * return error message
     * @param code
     * @param message
     * @return
     */
    public static ResultBean error(Integer code, String message){
        return ResultBean.builder()
                .code(code)
                .message(message)
                .build();
    }

    /*
     * return error message
     * @param ResultEnum
     * @return
     */
    public static ResultBean error(ResultEnum resultEnum){
        return ResultBean.builder()
                .code(resultEnum.getCode())
                .message(resultEnum.getMessage())
                .build();
    }

    /*
     * return error message
     * @return
     */
    public static ResultBean error(){
        return ResultBean.builder()
                .code(ResultEnum.FAIL.getCode())
                .message(ResultEnum.FAIL.getMessage())
                .build();
    }

    /*
     * return error message
     * @param message enum
     * @return
     */
    public static ResultBean success(String message){
        return ResultBean.builder()
                .code(ResultEnum.SUCCESS.getCode())
                .message(message)
                .build();
    }

    /*
     * return error message
     * @param code
     * @param message
     * @return
     */
    public static ResultBean success(Integer code, String message){
        return ResultBean.builder()
                .code(code)
                .message(message)
                .build();
    }

    /*
     * return error message
     * @param resultEnum
     * @param message
     * @return
     */
    public static ResultBean success(ResultEnum resultEnum){
        return ResultBean.builder()
                .code(resultEnum.getCode())
                .message(resultEnum.getMessage())
                .build();
    }

    /*
     * return error message
     * @return
     */
    public static ResultBean success(){
        return ResultBean.builder()
                .code(ResultEnum.SUCCESS.getCode())
                .message(ResultEnum.SUCCESS.getMessage())
                .build();
    }

    /*
     * return error message
     * @param data
     * @return
     */
    public static ResultBean success(Object data){
        return ResultBean.builder()
                .code(ResultEnum.SUCCESS.getCode())
                .message(ResultEnum.SUCCESS.getMessage())
                .data(data)
                .build();
    }

    /*
     * return error message
     * @param message
     * @return
     */
    public static ResultBean exception(String message){
        return ResultBean.builder()
                .code(ResultEnum.EXCEPTION.getCode())
                .message(message)
                .build();
    }

    /*
     * return error message
     * @param resultEnum
     * @return
     */
    public static ResultBean exception(ResultEnum resultEnum){
        return ResultBean.builder()
                .code(resultEnum.getCode())
                .message(resultEnum.getMessage())
                .build();
    }

    /*
     * return error message
     * @return
     */
    public static ResultBean exception(){
        return ResultBean.builder()
                .code(ResultEnum.EXCEPTION.getCode())
                .message(ResultEnum.EXCEPTION.getMessage())
                .build();
    }
}
