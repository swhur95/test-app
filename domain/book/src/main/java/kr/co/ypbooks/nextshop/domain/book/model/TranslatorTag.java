package kr.co.ypbooks.nextshop.domain.book.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
@Entity
public class TranslatorTag {
    /**
     * 작가 태그 고유 Id
     */
    @EmbeddedId
    private TranslatorTagId translatorTagId;

    /**
     * 등록 일시
     */
    private LocalDateTime regDate;
}
