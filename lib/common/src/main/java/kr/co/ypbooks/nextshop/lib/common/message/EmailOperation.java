package kr.co.ypbooks.nextshop.lib.common.message;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import kr.co.ypbooks.nextshop.lib.common.message.job.RegularSendEmailsJob;
import kr.co.ypbooks.nextshop.lib.common.operation.QuartzOperation;
import kr.co.ypbooks.nextshop.lib.common.property.EmailProperty;
import kr.co.ypbooks.nextshop.lib.common.property.ReceiveMailAddress;
import kr.co.ypbooks.nextshop.lib.common.property.TaskDefineProperty;
import lombok.extern.slf4j.Slf4j;
import org.quartz.JobDataMap;
import org.quartz.JobKey;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;

@Slf4j
@Component
public class EmailOperation {

    private QuartzOperation quartzOperation;

    public EmailOperation(QuartzOperation quartzOperation) {
        this.quartzOperation = quartzOperation;
    }

    /**
     * Call the api to send mail
     * @param emailProperty
     * @return
     */
    public ApiResult sendMail(EmailProperty emailProperty) {

        if (emailProperty == null || emailProperty.getReceiverList().size() == 0) {
            log.error("The recipient cannot be empty");
            return null;
        }

        JsonObject requestBody = new JsonObject();
        // from
        requestBody.addProperty("senderAddress", emailProperty.getSenderAddress());
        requestBody.addProperty("senderName", emailProperty.getSenderName());

        requestBody.addProperty("templateId", emailProperty.getTemplateId());

        Gson gson = new Gson();
        requestBody.addProperty("templateParameter", gson.toJson(emailProperty.getTemplateParameter()));

        // to
        JsonArray receiverList = new JsonArray();
        for (ReceiveMailAddress receiveMailAddress : emailProperty.getReceiverList()) {
            receiverList.add(gson.toJson(receiveMailAddress));
        }

        requestBody.addProperty("receiverList", gson.toJson(receiverList));

        //todo email api

        return null;
    }

    /**
     * Send a specified number of emails at a specified interval
     * @param intervalSecondTime
     * @param everyTimeNumbers
     * @param emailPropertyList
     * @return
     */
    public HashMap<String, ApiResult> intervalSendEmail(int intervalSecondTime, int everyTimeNumbers, EmailProperty emailPropertyList) {

        if (intervalSecondTime < 0 || everyTimeNumbers < 1 || emailPropertyList.getReceiverList().size() < 1) {
            return null;
        }

        HashMap<String, ApiResult> apiResultMap = new HashMap<>();

        // if intervalSecondTime=0 Send once
        if (intervalSecondTime == 0) {
            ApiResult apiResult = sendMail(emailPropertyList);
            apiResultMap.put("1", apiResult);
        }

        int totalNums = emailPropertyList.getReceiverList().size();

        // if totalNums <= everyTimeNumbers Send it all at once
        if (totalNums <= everyTimeNumbers) {
            ApiResult apiResult = sendMail(emailPropertyList);
            apiResultMap.put("1", apiResult);
            return apiResultMap;
        }

        int requestNums = 0;
        List<ReceiveMailAddress> totalMailAddressList = emailPropertyList.getReceiverList();
        // If totalMailAddressList hasn't data, end loop
        while (totalMailAddressList.size() > 0) {
            int remainNums = totalMailAddressList.size();
            if (remainNums < everyTimeNumbers) {
                emailPropertyList.setReceiverList(totalMailAddressList);
            }else {
                List<ReceiveMailAddress> receiveMailAddressList = totalMailAddressList.subList(0, everyTimeNumbers);
                emailPropertyList.setReceiverList(receiveMailAddressList);
            }
            // Request email api
            ApiResult apiResult = sendMail(emailPropertyList);
            requestNums ++;
            apiResultMap.put(String.valueOf(requestNums), apiResult);

            int removeNums = Math.min(remainNums, everyTimeNumbers);
            // Information already requested is removed from the pending request list
            totalMailAddressList.subList(0, removeNums).clear();
            try {
                Thread.sleep(1000L * intervalSecondTime);
            } catch (InterruptedException e) {
                log.debug("IntervalSend occurs an InterruptedException", e);
            }
        }
        return apiResultMap;
    }

    /**
     * Create a scheduled task to send an email at a specific time
     * For example, 2022/3/28 send an email at 9 am
     */
    public boolean timingSendEmail(EmailProperty emailProperty, String cronSchedule, String jobName, String jobGroup, String description) {

        JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put("emailProperty", emailProperty);
        JobKey jobKey = JobKey.jobKey(jobName, jobGroup);
        TaskDefineProperty taskDefineProperty = new TaskDefineProperty(jobKey,
                description,
                cronSchedule,
                jobDataMap,
                RegularSendEmailsJob.class);
        return quartzOperation.createScheduleJob(taskDefineProperty);
    }

}
