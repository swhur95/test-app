package kr.co.ypbooks.nextshop.domain.order.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
@Entity
public class OrderProductHist {
    /**
     * 주문 상품 번호
     */
    @Id
    private Long orderProductNo;

    /**
     * 주문 번호
     */
    private Long orderNo;

    /**
     * 판매 번호 (SAP 로그 번호)
     */
    private Long orderLogNo;

    /**
     * 주문 상태 코드
     */
    private String orderStateCd;

    /**
     * 물류사 코드
     */
    private String logistCompanyCd;

    /**
     * 상품 유형 코드 (B: 도서, E: E-Book)
     */
    private String productTypeCd;

    /**
     * 상품 코드
     */
    private String productCd;

    /**
     * 상품 정보
     */
    private String productInfo;

    /**
     * 결제 정보
     */
    private String payInfo;

    /**
     * 물류 정보
     */
    private String logistInfo;

    /**
     * 배송 정보
     */
    private String delivInfo;

    /**
     * 지점 정보
     */
    private String branchInfo;

    /**
     * 등록 일시
     */
    private LocalDateTime regDate;

    /**
     * 수정 일시
     */
    private LocalDateTime updDate;


}
