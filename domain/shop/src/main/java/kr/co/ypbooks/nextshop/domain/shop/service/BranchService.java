package kr.co.ypbooks.nextshop.domain.shop.service;

import kr.co.ypbooks.nextshop.lib.common.util.JsonUtil;
import kr.co.ypbooks.nextshop.domain.shop.dto.BranchDayOffDto;
import kr.co.ypbooks.nextshop.domain.shop.dto.BranchDto;
import kr.co.ypbooks.nextshop.domain.shop.dto.BranchOverviewDto;
import kr.co.ypbooks.nextshop.domain.shop.model.Branch;
import kr.co.ypbooks.nextshop.domain.shop.model.BranchDayOff;
import kr.co.ypbooks.nextshop.domain.shop.repository.BranchDayOffRepository;
import kr.co.ypbooks.nextshop.domain.shop.repository.BranchImageRepository;
import kr.co.ypbooks.nextshop.domain.shop.repository.BranchRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service
@Transactional
public class BranchService {
    /**
     * 지점 정보 저장소
     */
    private final BranchRepository branchRepository;

    /**
     * 지점 휴무 정보 저장소
     */
    private final BranchDayOffRepository branchDayOffRepository;

    /**
     * 지점 매장 사진 저장소
     */
    private final BranchImageRepository branchImageRepository;

    /**
     * 생성자
     * @param branchRepository 지점 저장소
     * @param branchDayOffRepository 지점 휴무 저장소
     * @param branchImageRepository 지점 매장 사진 저장소
     */
    public BranchService(BranchRepository branchRepository, BranchDayOffRepository branchDayOffRepository, BranchImageRepository branchImageRepository) {
        this.branchRepository = branchRepository;
        this.branchDayOffRepository = branchDayOffRepository;
        this.branchImageRepository = branchImageRepository;
    }

    private BranchOverviewDto convertToBranchOverviewDto(Branch branch) {
        BranchDto.BranchInfo branchInfo = new BranchDto.BranchInfo();

        try {
            // 지점 정보가 존재하는 경우
            if (branch.getBranchInfo() != null) {
                branchInfo = (BranchDto.BranchInfo) JsonUtil.jsonToObject(branch.getBranchInfo(), branchInfo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return BranchOverviewDto.builder()
                .branchCd(branch.getBranchCd())
                .branchName(branch.getBranchName())
                .branchRegionCd(branch.getBranchRegionCd())
                .branchMgmtType(branch.getBranchMgmtType())
                .branchOpnType(branch.getBranchOpnType())
                .branchStateCd(branch.getBranchStateCd())
                .regDate(branch.getRegDate())
                .updDate(branch.getUpdDate())
                .placeCompanyName(branchInfo.getPlaceCompanyName())
                .phone(branchInfo.getPhone())
                .address1(branchInfo.getAddress1())
                .address2(branchInfo.getAddress2()).build();
    }

    private BranchDto convertToBranchDto(Branch branch) {
        BranchDto.BranchInfo branchInfo = JsonUtil.jsonToObjectNoException(branch.getBranchInfo(), BranchDto.BranchInfo.class);
        BranchDto.AdminInfo adminInfo = JsonUtil.jsonToObjectNoException(branch.getAdminInfo(), BranchDto.AdminInfo.class);
        BranchDto.BizTimeInfo bizTimeInfo = JsonUtil.jsonToObjectNoException(branch.getBizTimeInfo(), BranchDto.BizTimeInfo.class);
        BranchDto.MapInfo mapInfo = JsonUtil.jsonToObjectNoException(branch.getMapInfo(), BranchDto.MapInfo.class);
        BranchDto.ContactInfo contactInfo = JsonUtil.jsonToObjectNoException(branch.getContactInfo(), BranchDto.ContactInfo.class);
        BranchDto.DirectionsInfo directionsInfo = JsonUtil.jsonToObjectNoException(branch.getDirectionsInfo(), BranchDto.DirectionsInfo.class);

        return BranchDto.builder()
                .branchCd(branch.getBranchCd())
                .branchName(branch.getBranchName())
                .branchRegionCd(branch.getBranchRegionCd())
                .branchMgmtType(branch.getBranchMgmtType())
                .branchOpnType(branch.getBranchOpnType())
                .branchStateCd(branch.getBranchStateCd())
                .branchInfo(branchInfo)
                .adminInfo(adminInfo)
                .bizTimeInfo(bizTimeInfo)
                .mapInfo(mapInfo)
                .contactInfo(contactInfo)
                .directionsInfo(directionsInfo)
                .regDate(branch.getRegDate())
                .updDate(branch.getUpdDate()).build();

    }

    private Branch convertToBranch(BranchDto branchDto) {
        String branchInfo = JsonUtil.objectToJsonNoException(branchDto.getBranchInfo());
        String adminInfo = JsonUtil.objectToJsonNoException(branchDto.getAdminInfo());
        String bizTimeInfo = JsonUtil.objectToJsonNoException(branchDto.getBizTimeInfo());;
        String mapInfp = JsonUtil.objectToJsonNoException(branchDto.getMapInfo());
        String contactInfo = JsonUtil.objectToJsonNoException(branchDto.getContactInfo());
        String directionInfo = JsonUtil.objectToJsonNoException(branchDto.getDirectionsInfo());

        return Branch.builder()
                .branchCd(branchDto.getBranchCd())
                .branchName(branchDto.getBranchName())
                .branchRegionCd(branchDto.getBranchRegionCd())
                .branchMgmtType(branchDto.getBranchMgmtType())
                .branchOpnType(branchDto.getBranchOpnType())
                .branchStateCd(branchDto.getBranchStateCd())
                .branchInfo(branchInfo)
                .adminInfo(adminInfo)
                .bizTimeInfo(bizTimeInfo)
                .mapInfo(mapInfp)
                .contactInfo(contactInfo)
                .directionsInfo(directionInfo)
                .regDate(branchDto.getRegDate())
                .updDate(branchDto.getUpdDate()).build();
    }

    private BranchDayOffDto convertToBranchDayOffDto(BranchDayOff branchDayOff) {
        return BranchDayOffDto.builder()
                .branchDayOffNo(branchDayOff.getDayOffNo())
                .branchCd(branchDayOff.getBranchCd())
                .dayOffDate(branchDayOff.getDayOffDate())
                .branchName(branchDayOff.getBranchName())
                .dayOffReason(branchDayOff.getDayOffReason())
                .dayOfWeek(branchDayOff.getDayOfWeek())
                .regAdminId(branchDayOff.getRegAdminId())
                .regAdminName(branchDayOff.getRegAdminName())
                .regDate(branchDayOff.getRegDate())
                .updDate(branchDayOff.getUpdDate()).build();
    }

    private BranchDayOff convertToBranchDayOff(BranchDayOffDto branchDayOffDto) {
        return convertToBranchDayOff(branchDayOffDto, null, LocalDateTime.now(), null);
    }

    private BranchDayOff convertToBranchDayOff(BranchDayOffDto branchDayOffDto, Long dayOffNo, LocalDateTime regDate, LocalDateTime updDate) {
        return BranchDayOff.builder()
                .dayOffNo(dayOffNo)
                .branchCd(branchDayOffDto.getBranchCd())
                .dayOffDate(branchDayOffDto.getDayOffDate())
                .branchName(branchDayOffDto.getBranchName())
                .dayOffReason(branchDayOffDto.getDayOffReason())
                .dayOfWeek(branchDayOffDto.getDayOfWeek())
                .regAdminId(branchDayOffDto.getRegAdminId())
                .regAdminName(branchDayOffDto.getRegAdminName())
                .regDate(regDate)
                .updDate(updDate).build();
    }
}
