package kr.co.ypbooks.nextshop.app.shop.exception;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public enum ResultEnum {

    SUCCESS(200,"operation success"),
    FAIL(500,"operation faile"),
    EXCEPTION(400,"system error"),
    NOPERMISS(401,"no permiss");

    private Integer code;
    private String message;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }




}
