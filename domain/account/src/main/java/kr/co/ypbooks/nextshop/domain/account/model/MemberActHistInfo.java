package kr.co.ypbooks.nextshop.domain.account.model;

import lombok.*;

import java.time.LocalDateTime;

/**
 * ACT_HIST_INFO(활동 이력)
 */

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class MemberActHistInfo {

    /**
     * 회원 가입 IP
     */
    private String regIp;
    /**
     * 최종 접속일자
     */
    private LocalDateTime lastConnectDate;
    /**
     * 최초 주문일자
     */
    private LocalDateTime firstOrderDate;
    /**
     * 최종 주문일자
     */
    private LocalDateTime lastOrderDate;
    /**
     * 총접속횟수
     */
    private Integer totConnectDate;
    /**
     * 최종접속 IP
     */
    private String lastConnectIp;
    /**
     * 메일 이벤트 번호
     */
    private Integer mailEventNo;
    /**
     * 관리대상 구분
     */
    private String issueType;
    /**
     * 관리대상 사유
     */
    private String issueComment;
    /**
     * 최종 장바구니 번호
     */
    private String lastCartNo;
    /**
     * 최종 장바구니 날짜
     */
    private LocalDateTime lastCartDate;
    /**
     * 로그인 실패 횟수
     */
    private Integer loginCnt;
}
