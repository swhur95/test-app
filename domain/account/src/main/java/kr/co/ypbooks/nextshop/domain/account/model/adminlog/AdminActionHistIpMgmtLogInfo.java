package kr.co.ypbooks.nextshop.domain.account.model.adminlog;

import io.swagger.annotations.ApiModel;
import lombok.*;

/**
 * IP 관리 로그
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@ApiModel(value = "로그 타입 (5: IP 관리)")
public class AdminActionHistIpMgmtLogInfo {
    /**
     * 회원 ip
     */
    private String memberIp;
    /**
     * 관리값
     */
    private String mgmtValue;
    /**
     * 관리 액션 (등록/수정/삭제)
     */
    private String mgmtAction;
}
