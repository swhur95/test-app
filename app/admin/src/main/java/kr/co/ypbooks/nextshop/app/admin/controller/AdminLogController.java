package kr.co.ypbooks.nextshop.app.admin.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import kr.co.ypbooks.nextshop.domain.account.dto.AdminActionHistVo;
import kr.co.ypbooks.nextshop.domain.account.dto.MemberDto;
import kr.co.ypbooks.nextshop.domain.account.model.Member;
import kr.co.ypbooks.nextshop.domain.account.model.adminlog.AdminActionHistViewMenuLogInfo;
import kr.co.ypbooks.nextshop.domain.account.service.AdminActionHistService;
import kr.co.ypbooks.nextshop.domain.account.utils.DateUtils;
import kr.co.ypbooks.nextshop.lib.common.jpa.common.pojo.PageInfo;
import kr.co.ypbooks.nextshop.lib.common.jpa.common.pojo.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import java.util.Date;

@Slf4j
@Api(tags = "AdminLog")
@RequestMapping("/adminLog")
@RestController
public class AdminLogController {

    @Autowired
    AdminActionHistService actionHistService;


    @ApiOperation(value = "AdminActionHist" , notes = "getAdminActionHist")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "logNo", paramType = "path", required = true, dataType = "Integer", example = "1")
    })
    @RequestMapping(value = "/getAdminActionHist/{logNo}", method = RequestMethod.GET)
    public AdminActionHistVo getAdminActionHist(@PathVariable Integer logNo){

        Result<AdminActionHistVo> result = actionHistService.get(logNo);
        AdminActionHistVo data = result.getData();
        return data;

    }


    @ApiOperation(value = "AdminActionHist" , notes = "insert")
    @ApiImplicitParam(name = "actionHistVo", value = "actionHistInfo", dataType = "AdminActionHistVo", required = true)
    @RequestMapping(value = "/insertAdminActionHist", method = RequestMethod.POST)
    public AdminActionHistVo insertAdminActionHist(@RequestBody AdminActionHistVo actionHistVo){

        /*
        {
              "adminId": "adminIdUUUID-001",
              "adminIp": "10.20.15.203",
              "logContent": "logContent====string",
              "logNo": 2,
              "logType": 2,
              "loginLogInfo": {
                "loginResult": "loginResult-------------",
                "proxyConnect": "proxyConnect====string",
                "serverUrl": "serverUrl======",
                "userAgent": "userAgent====="
              },
              "memberCd": "memberCd-AA001"
            }
        */

        actionHistVo.setRegDate(DateUtils.asLocalDateTime(new Date()));

        AdminActionHistVo adminActionHistVo = null;
        try {
            adminActionHistVo = actionHistService.insertAdminActionHist(actionHistVo);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return adminActionHistVo;
    }


    @ApiOperation(value = "AdminActionHist" , notes = "list")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "logType", paramType = "query", dataType = "String", example = "1"),
            @ApiImplicitParam(name = "page", paramType = "query", required = true, dataType = "Integer", example = "1"),
            @ApiImplicitParam(name = "rows", paramType = "query", required = true, dataType = "Integer", example = "10")
    })
    @RequestMapping(value = "/adminActionHistList", method = RequestMethod.GET)
    public Result<PageInfo<AdminActionHistVo>> adminActionHistList(@ApiIgnore AdminActionHistVo actionHistVo){

        Result<PageInfo<AdminActionHistVo>> page = actionHistService.page(actionHistVo);

        return page;

    }
}
