package kr.co.ypbooks.nextshop.domain.book.repository;

import kr.co.ypbooks.nextshop.domain.book.model.Writer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WriterRepository extends JpaRepository<Writer, Long> {
}
