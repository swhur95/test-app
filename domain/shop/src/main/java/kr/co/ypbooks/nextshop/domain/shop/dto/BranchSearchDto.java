package kr.co.ypbooks.nextshop.domain.shop.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

import java.time.LocalDateTime;

@ApiModel
@Getter
public class BranchSearchDto {
    /**
     * 등록일 조건 (시작 일시)
     */
    @ApiModelProperty(value = "등록일 조건 (시작일)", example = "2022-03-01 09:00:00")
    @JsonProperty("start_reg_date")
    private LocalDateTime startRegDate;

    /**
     * 등록일 조건 (종료 일시)
     */
    @ApiModelProperty(value = "등록일 조건 (종료일)", example = "2022-04-01 09:00:00")
    @JsonProperty("end_reg_date")
    private LocalDateTime endRegDate;

    /**
     * 지점명 조건
     */
    @ApiModelProperty(value = "지점명 조건", example = "종로")
    @JsonProperty("branch_name")
    private String branchName;

    /**
     * 지점 지역 코드들 조건
     */
    @ApiModelProperty(value = "지점 지역 코드들 조건", example = "[\"01\", \"02\"]")
    @JsonProperty("branch_region_cd_list")
    private String branchRegionCdList;

    /**
     * 지점 상태 코드 조건 (01:정상, 02:폐점)
     */
    @ApiModelProperty(value = "지점 상태 코드 조건", example = "01")
    @JsonProperty("branch_state_cd")
    private String branchStateCd;
}
