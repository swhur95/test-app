package kr.co.ypbooks.nextshop.app.admin.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import kr.co.ypbooks.nextshop.lib.common.dto.ResultDto;
import kr.co.ypbooks.nextshop.domain.order.dto.ConsultDto;
import kr.co.ypbooks.nextshop.domain.order.dto.ConsultSearchDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@Slf4j
@Api(tags = {"Consulting"})
@RequestMapping("/admin/consult")
@RestController
public class ConsultController {
    @ApiOperation(value = "상담 개요 목록 조회", notes = "상담 개요 목록을 얻어옵니다.")
    @GetMapping("")
    public ResultDto getConsultOverview(@RequestBody ConsultSearchDto consultSearchDto) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    @ApiOperation(value = "상담 정보 등록", notes = "상담 정보를 등록합니다.")
    @PostMapping("")
    public ResultDto insertConsult(@RequestBody ConsultDto consultDto) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    @ApiOperation(value = "상담 정보 조회", notes = "상담 정보를 얻어옵니다.")
    @GetMapping("/{consult_no}")
    public ResultDto getConsult(@PathVariable("consult_no") Long consultNo) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    @ApiOperation(value = "상담 정보 수정", notes = "상담 정보를 수정합니다.")
    @PutMapping("/{consult_no}")
    public ResultDto updateConsult(@PathVariable("consult_no") Long consultNo, @RequestBody ConsultDto consultDto) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    @ApiOperation(value = "상담 정보 삭제", notes = "상담 정보를 삭제합니다.")
    @DeleteMapping("/{consult_no}")
    public ResultDto deleteConsult(@PathVariable("consult_no") Long consultNo) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }
}
