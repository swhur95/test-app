package kr.co.ypbooks.nextshop.domain.shop.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import kr.co.ypbooks.nextshop.lib.common.jpa.common.pojo.PageCondition;
import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;
import java.time.LocalDateTime;

@ApiModel
@Getter
@Builder
public class DisplayOverviewDto extends PageCondition implements Serializable {
    /**
     * 전시 번호 (고유 번호)
     */
    @ApiModelProperty(example = "전시 고유 번호입니다.")
    private Integer displayNo;

    /**
     * 전시 제목
     */
    @ApiModelProperty(example = "전시 제목입니다")
    @ApiParam(value = "전시 제목", required = true)
    private String displaySubject;

    /**
     * 관리자명 (등록자)
     */
    @ApiModelProperty(example = "등록한 관리자명 입니다.")
    @ApiParam(value = "등록한 관리자명", required = true)
    private String regAdminName;

    /**
     * 노출 여부 (0: 비노출, 1: 노출)
     */
    @ApiModelProperty(example = "1")
    @ApiParam(value = "노출 여부", required = true)
    private Boolean exposeFlag;

    /**
     * 전시 시작 일시
     */
    @ApiModelProperty(example = "2022-03-22 09:00:00")
    @ApiParam(value = "전시 시작 일시")
    private LocalDateTime displayStartDate;

    /**
     * 전시 종료 일시 (null인 경우 종료 없음)
     */
    @ApiModelProperty(example = "2023-03-22 09:00:00")
    @ApiParam(value = "전시 종료 일시")
    private LocalDateTime displayEndDate;


    /**
     * 등록 일시
     */
    @ApiModelProperty(example = "2023-03-22 09:00:00")
    @ApiParam(value = "등록 일시", required = true)
    private LocalDateTime regDate;

    /**
     * 수정 일시
     */
    @ApiModelProperty(example = "2023-03-22 09:00:00")
    @ApiParam(value = "수정 일시")
    private LocalDateTime updDate;
}
