package kr.co.ypbooks.nextshop.domain.account.service;

import com.google.gson.JsonObject;
import kr.co.ypbooks.nextshop.domain.account.utils.AppleUtil;
import org.springframework.stereotype.Service;
import java.util.UUID;

@Service
public class AppleService {

    public void appleLogin(String identityToken) throws Exception{

        String randomStr = UUID.randomUUID().toString();
        //验证identityToken
        if(!AppleUtil.verify(identityToken)){
            throw new RuntimeException("identityToken验证失败");
        }
        //对identityToken解码
        JsonObject json = AppleUtil.parserIdentityToken(identityToken);
        if(json == null){
            throw new RuntimeException("identityToken解码失败");
        }

        System.out.println("==============>" + json);
//            //苹果用户唯一标识
//            String appleId = new MD5().digestHex16(json.get("sub").toString()) ;
//            SysUser user = baseMapper.selectOne(new MpUtil<SysUser>().queryNormal().eq(SysUser.APPLE_ID,appleId));
//            //判断该账户是否被禁用
//            if(user!=null && user.getDataActive().equals(StatusEnum.STATUS_DISABLE.getStrCode())){
//                throw new RuntimeException("该账户已被禁用，请联系管理员！");
//            }
    }
}
