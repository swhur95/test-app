package kr.co.ypbooks.nextshop.domain.book.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;

import java.time.LocalDate;
import java.util.List;

@ApiModel
@Getter
@Builder
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class BookDto {
    /**
     * ISBN
     */
    @ApiModelProperty(value = "ISBN", example = "12345678")
    private String isbn;

    /**
     * 도서 제목
     */
    @ApiModelProperty(value = "도서 제목", example = "도서 제목")
    private String bookName;

    /**
     * 도서 부제목(도서 주제)
     */
    @ApiModelProperty(value = "도서 부제목", example = "도서 부제목")
    private String bookTitle;

    /**
     * 저자 번호 목록
     */
    @ApiModelProperty(value = "저자 번호 목록", example = "[\"1234\", \"1235\"]")
    private List<Long> writerNoList;

    /**
     * 역자 번호 목록
     */
    @ApiModelProperty(value = "역자 번호 목록", example = "[\"1234\", \"1235\"]")
    private List<Long> transNoList;

    /**
     * 조/서/가 (카테고리 코드)
     */
    @ApiModelProperty(value = "조/서/가 카테고리 코드", example = "120920")
    private String primaryCategoryCd;

    /**
     * 출판 종류
     */
    @ApiModelProperty(value = "출판 종류", example = "1")
    private String bookPubKind;

    /**
     * 출판일
     */
    @ApiModelProperty(value = "출판일", example = "2022-03-22")
    private LocalDate bookPubDate;

    /**
     * 출판사 코드
     */
    @ApiModelProperty(value = "출판사 코드", example = "151179")
    private String pubCompanyCd;

    /**
     * 총 페이지 수
     */
    @ApiModelProperty(value = "총 페이지수", example = "357")
    private Integer bookTotPage;

    /**
     * 판형 
     */
    @ApiModelProperty(value = "판형", example = "2")
    private String bookSize;

    /**
     * 판수
     */
    @ApiModelProperty(value = "판수", example = "1")
    private Byte bookEdnNo;

    /**
     * 정가
     */
    @ApiModelProperty(value = "정가", example = "12000")
    private Long bookPrice;

    /**
     * 매입율
     */
    @ApiModelProperty(value = "정가", example = "12000")
    private Byte bookPurRate;

    /**
     * 화폐 구분
     */
    @ApiModelProperty(value = "화폐구분", example = "W")
    private String ccyTypeCd;

    /**
     * 초록 유무
     * TODO: 초록 유무가 뭘 의미하는지?
     */
//    @ApiModelProperty(value = "초록유무", example = "내용")
//    private String rmkgub;

    /**
     * 도서 표시 상태
     */
    @ApiModelProperty(value = "도서 표시 여부", example = "1")
    private Boolean dispFlag;

    /**
     * 성인 도서 여부
     */
    @ApiModelProperty(value = "성인 도서 여부", example = "0")
    private Boolean adultFlag;

    /**
     * 어린이 안전 인증 번호
     */
    @ApiModelProperty(value = "어린이 안전 인증 번호", example = "")
    private String childSafeCertCd;

    /**
     * 과세 여부
     */
    @ApiModelProperty(value = "과세 여부", example = "0")
    private Boolean vatFlag;

    /**
     * 카테고리 코드 목록
     */
    @ApiModelProperty(value = "카테고리 코드 정보", example = "[\"120902\", \"120903\"]")
    private List<String> categoryCdList;

    /**
     * 시리즈 번호
     */
    @ApiModelProperty(value = "시리즈 번호", example = "1234")
    private Long seriesNo;

    /**
     * 키워드 목록 (태그)
     */
    @ApiModelProperty(value = "태그 단어 정보", example = "[\"문학\", \"신인작가\"]")
    private List<String> tagWordList;

    /**
     * 책 소개
     */
    @ApiModelProperty(value = "책 소개", example = "책 소개")
    private String bookIntro;

    /**
     * 목차
     */
    @ApiModelProperty(value = "목차", example = "목차")
    private String tableOfContents;

    /**
     * 도서 본문 내용 일부
     */
    @ApiModelProperty(value = "도서 본문 내용 일부", example = "도서 본문 내용 일부")
    private String partOfBody;

    /**
     * 추천 리뷰
     */
    @ApiModelProperty(value = "추천 리뷰", example = "추천 리뷰")
    private String recoRev;

    /**
     * 출판사 리뷰
     */
    @ApiModelProperty(value = "출판사 리뷰", example = "출판사 리뷰")
    private String pubCompanyRev;

    /**
     * 미디어 서평
     */
    @ApiModelProperty(value = "미디어 리뷰", example = "미디어 리뷰")
    private String mediaRev;

    /**
     * 개정 상세 정보
     */
    @ApiModelProperty(value = "개정 정보", example = "개정 정보")
    private String revisionDesc;

    /**
     * 도서 기타 참고 사항
     */
    @ApiModelProperty(value = "도서 기타 참고 사항", example = "도서 기타 참고 사항")
    private String bookAddContent;

    /**
     * 미리보기 이미지명
     */
    @ApiModelProperty(value = "미리보기 이미지명", example = "미리보기 이미지명")
    private String prevImgName;

    /**
     * 미리보기 이미지 경로
     */
    @ApiModelProperty(value = "미리보기 이미지 경로", example = "미리보기 이미지 경로")
    private String prevImgPath;
}
