package kr.co.ypbooks.nextshop.domain.product.model;

import lombok.*;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
@Entity
public class ProductCategory {
    /**
     * 상품 카테고리 Id
     */
    @EmbeddedId
    private ProductCategoryId productCategoryId;

    /**
     * 등록 일시
     */
    private LocalDateTime regDate;
}

