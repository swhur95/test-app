package kr.co.ypbooks.nextshop.domain.product.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class ProductTagId implements Serializable {
    /**
     * 상품 유형 코드 (B: 도서, E: E-Book)
     */
    private String productTypeCd;

    /**
     * 상품 코드
     */
    private String productCd;

    /**
     * 태그 단어
     */
    private String tagWord;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductTagId that = (ProductTagId) o;
        return Objects.equals(productTypeCd, that.productTypeCd) && Objects.equals(productCd, that.productCd) && Objects.equals(tagWord, that.tagWord);
    }

    @Override
    public int hashCode() {
        return Objects.hash(productTypeCd, productCd, tagWord);
    }
}
