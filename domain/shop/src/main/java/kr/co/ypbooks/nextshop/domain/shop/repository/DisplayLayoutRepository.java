package kr.co.ypbooks.nextshop.domain.shop.repository;

import kr.co.ypbooks.nextshop.domain.shop.model.DisplayLayout;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DisplayLayoutRepository extends JpaRepository<DisplayLayout, Byte> {
}
