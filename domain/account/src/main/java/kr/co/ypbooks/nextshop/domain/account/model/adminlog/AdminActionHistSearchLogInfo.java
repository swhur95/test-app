package kr.co.ypbooks.nextshop.domain.account.model.adminlog;

import io.swagger.annotations.ApiModel;
import lombok.*;

import java.time.LocalDateTime;

/**
 * 검색 로그 정보
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@ApiModel(value = "로그 타입 (4: 개인정보 검색)")
public class AdminActionHistSearchLogInfo {
    /**
     * 관리자 메뉴
     */
    private String adminMenu;
    /**
     * 관리자 파일명
     */
    private String adminFile;
    /**
     * 검색내용
     */
    private String searchContent;
    /**
     * 시작일시
     */
    private LocalDateTime startDate;
    /**
     * 종료일시
     */
    private LocalDateTime endDate;
}
