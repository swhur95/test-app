package kr.co.ypbooks.nextshop.domain.order.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
@Entity
public class Consult {
    /**
     * 상담 고유 번호
     */
    @Id
    private Long consultNo;

    /**
     * 회원 유형 코드 (1: 북클럽, 2: 계열사)
     */
    private String memberTypeCd;

    /**
     * 회원 ID
     */
    private String memberId;

    /**
     * 회원 이름
     */
    private String memberName;

    /**
     * 회원 전화번호
     */
    private String memberPhone;

    /**
     * 회원 휴대전화
     */
    private String memberMobile;

    /**
     * 회원 이메일
     */
    private String memberEmail;

    /**
     * 주문 번호
     */
    private Long orderNo;

    /**
     * 관리자 ID
     */
    private String adminId;

    /**
     * 관리자 이메일
     */
    private String adminEmail;

    /**
     * 상담 분류 코드
     */
    private String consultTypeCd;

    /**
     * 상담 제목
     */
    private String consultSubject;

    /**
     * 상담 내용
     */
    private String consultContent;

    /**
     * 상담 상태
     */
    private String consultStateCd;

    /**
     * 상담 시간 (Second)
     */
    private Integer consultTime;

    /**
     * 상담 처리 수단 코드
     */
    private String consultProcMethodCd;

    /**
     * 상담 처리 내용
     */
    private String consultProcContent;

    /**
     * 상담 답변
     */
    private String consultAnswer;

    /**
     * 상담 완료 일시
     */
    private LocalDateTime consultCompDate;

    /**
     * 의견
     */
    private String comment;

    /**
     * 추가 정보
     */
    private String addInfo;

    /**
     * 등록 일시
     */
    private LocalDateTime regDate;

    /**
     * 수정 일시
     */
    private LocalDateTime updDate;
}
