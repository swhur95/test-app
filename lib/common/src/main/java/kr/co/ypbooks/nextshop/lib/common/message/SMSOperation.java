package kr.co.ypbooks.nextshop.lib.common.message;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import kr.co.ypbooks.nextshop.lib.common.message.job.RegularSendEmailsJob;
import kr.co.ypbooks.nextshop.lib.common.operation.QuartzOperation;
import kr.co.ypbooks.nextshop.lib.common.property.SMSProperty;
import kr.co.ypbooks.nextshop.lib.common.property.SMSRecipient;
import kr.co.ypbooks.nextshop.lib.common.property.TaskDefineProperty;
import lombok.extern.slf4j.Slf4j;
import org.quartz.JobDataMap;
import org.quartz.JobKey;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Component
public class SMSOperation {

    private QuartzOperation quartzOperation;

    public SMSOperation(QuartzOperation quartzOperation) {
        this.quartzOperation = quartzOperation;
    }

    /**
     * Call the api to send SMS
     * @param smsProperty
     * @return
     */
    public ApiResult sendSMS(SMSProperty smsProperty) {
        JsonObject requestBody = new JsonObject();
        requestBody.addProperty("templateId", smsProperty.getTemplatedId());
        Gson gson = new Gson();
        String recipientList = gson.toJson(getRecipientList(smsProperty.getRecipientList()));
        requestBody.addProperty("recipientList", recipientList);
        //todo sms api
        return null;
    }

    private JsonArray getRecipientList(List<SMSRecipient> recipientList) {

        if (recipientList == null || recipientList.size() == 0) {
            log.error("The recipient cannot be empty");
            return null;
        }

        JsonArray arr = new JsonArray();
        for (SMSRecipient smsRecipient : recipientList) {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("recipientNo", smsRecipient.getRecipientNo());
            jsonObject.addProperty("countryCode", smsRecipient.getCountryCode());
            JsonObject semdTemplateParameter = new JsonObject();
            Map<String, String> recipientTemplateParameter = smsRecipient.getTemplateParameter();
            for (String key : recipientTemplateParameter.keySet()) {
                semdTemplateParameter.addProperty(key, recipientTemplateParameter.get(key));
            }
            jsonObject.add("templateParameter", semdTemplateParameter);
            arr.add(jsonObject);
        }
        return arr;
    }

    /**
     * Send a specified number of SMS at a specified interval
     * @param intervalSecondTime
     * @param everyTimeNumbers
     * @param smsPropertyList
     * @return
     */
    public HashMap<String, ApiResult> intervalSendSms(int intervalSecondTime, int everyTimeNumbers, SMSProperty smsPropertyList) {

        if (intervalSecondTime < 0 || everyTimeNumbers < 1 || smsPropertyList.getRecipientList().size() < 1) {
            return null;
        }

        HashMap<String, ApiResult> apiResultMap = new HashMap<>();

        // if intervalSecondTime=0 Send once
        if (intervalSecondTime == 0) {
            ApiResult apiResult = sendSMS(smsPropertyList);
            apiResultMap.put("1", apiResult);
        }

        int totalNums = smsPropertyList.getRecipientList().size();

        // if totalNums <= everyTimeNumbers Send it all at once
        if (totalNums <= everyTimeNumbers) {
            ApiResult apiResult = sendSMS(smsPropertyList);
            apiResultMap.put("1", apiResult);
            return apiResultMap;
        }

        int requestNums = 0;
        List<SMSRecipient> totalMailAddressList = smsPropertyList.getRecipientList();
        // If totalMailAddressList hasn't data, end loop
        while (totalMailAddressList.size() > 0) {
            int remainNums = totalMailAddressList.size();
            if (remainNums < everyTimeNumbers) {
                smsPropertyList.setRecipientList(totalMailAddressList);
            }else {
                List<SMSRecipient> receiveMailAddressList = totalMailAddressList.subList(0, everyTimeNumbers);
                smsPropertyList.setRecipientList(receiveMailAddressList);
            }
            // Request sms api
            ApiResult apiResult = sendSMS(smsPropertyList);
            requestNums ++;
            apiResultMap.put(String.valueOf(requestNums), apiResult);

            int removeNums = Math.min(remainNums, everyTimeNumbers);
            // Information already requested is removed from the pending request list
            totalMailAddressList.subList(0, removeNums).clear();
            try {
                Thread.sleep(1000L * intervalSecondTime);
            } catch (InterruptedException e) {
                log.debug("IntervalSend occurs an InterruptedException", e);
            }
        }
        return apiResultMap;
    }

    /**
     * Create a scheduled task to send SMS at a specific time
     * For example, 2022/3/28 send SMS at 9 am
     * @param smsProperty
     * @param cronSchedule
     * @param jobKey
     * @param description
     * @return
     */
    public boolean timingSendSms(SMSProperty smsProperty, String cronSchedule, JobKey jobKey, String description) {

        JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put("smsProperty", smsProperty);

        TaskDefineProperty taskDefineProperty = new TaskDefineProperty(jobKey,
                description,
                cronSchedule,
                jobDataMap,
                RegularSendEmailsJob.class);
        return quartzOperation.createScheduleJob(taskDefineProperty);
    }

}
