package kr.co.ypbooks.nextshop.domain.book.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class TranslatorCategoryId implements Serializable {
    /**
     * 역자 고유 번호
     */
    private Long transNo;

    /**
     * 역자 분야 코드
     */
    private String transCategoryCd;
}
