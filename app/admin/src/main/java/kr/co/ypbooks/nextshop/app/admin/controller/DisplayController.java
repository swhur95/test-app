package kr.co.ypbooks.nextshop.app.admin.controller;

import io.swagger.annotations.*;
import kr.co.ypbooks.nextshop.lib.common.dto.ResultDto;
import kr.co.ypbooks.nextshop.domain.shop.dto.DisplayDto;
import kr.co.ypbooks.nextshop.domain.shop.dto.DisplayLayoutDto;
import kr.co.ypbooks.nextshop.domain.shop.dto.DisplaySearchDto;
import kr.co.ypbooks.nextshop.domain.shop.service.DisplayService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@Api(tags = {"Display"})
@RequestMapping("/admin/display")
@RestController
public class DisplayController {
    /**
     * 전시 서비스
     */
    private final DisplayService displayService;

    /**
     * 생성자
     * @param displayService 전시 서비스
     */
    public DisplayController(DisplayService displayService) {
        this.displayService = displayService;
    }

    /**
     * 전시 layout 정보를 얻어오는 함수
     * @return 결과 응답
     */
    @ApiOperation(value = "전시 layout 정보" , notes = "전시 layout 정보를 얻어옵니다.")
    @GetMapping("/layout")
    public ResultDto getDisplayLayout() {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");
        result.setResultData(this.displayService.getDisplayLayout());

        return result;
    }

    /**
     * 전시 layout 정보를 얻어오는 함수
     * @param displaySeq 전시 순번
     * @return 결과 응답
     */
    @ApiOperation(value = "전시 layout 정보" , notes = "전시 layout 정보를 얻어옵니다.")
    @GetMapping("/layout/{display_seq}")
    public ResultDto getDisplayLayout(@PathVariable("display_seq") Byte displaySeq) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");
        result.setResultData(this.displayService.getDisplayLayout(displaySeq));

        return result;
    }

    /**
     * 전시 layout을 설정하는 함수
     * @param displayLayoutDtoList 전시 layout 정보 List
     * @return 결과 응답
     */
    @ApiOperation(value = "전시 layout 설정" , notes = "전시 layout 정보를 설정합니다.")
    @PostMapping("/layout")
    public ResultDto insertDisplayLayout(@RequestBody List<DisplayLayoutDto> displayLayoutDtoList) {
        this.displayService.insertDisplayLayout(displayLayoutDtoList);
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    /**
     * 전시 layout 정보를 추가하는 함수
     * @param displaySeq            전시 순번
     * @param displayLayoutDto      전시 정보
     * @return                      결과 응답
     */
    @ApiOperation(value = "전시 layout 설정" , notes = "전시 layout 정보를 설정합니다.")
    @PostMapping("/layout/{display_seq}")
    public ResultDto insertDisplayLayout(@PathVariable("display_seq") Byte displaySeq, @RequestBody DisplayLayoutDto displayLayoutDto) {
        this.displayService.insertDisplayLayout(displaySeq, displayLayoutDto);
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    /**
     * 전시 layout 정보를 수정하는 함수
     * @param displaySeq 전시 순번
     * @param displayLayoutDto      전시 정보
     * @return 결과 응답
     */
    @ApiOperation(value = "전시 layout 수정" , notes = "전시 layout 정보를 수정합니다.")
    @PutMapping("/layout/{display_seq}")
    public ResultDto updateDisplayLayout(@PathVariable("display_seq") Byte displaySeq, @RequestBody DisplayLayoutDto displayLayoutDto) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");
        result.setResultData(this.displayService.updateDisplayLayout(displaySeq, displayLayoutDto));

        return result;
    }


    /**
     * 전시 개요 목록을 얻어오는 함수
     * @param displaySearchDto 전시 개요 검색 조건
     * @return 전시 개요 목록
     */
    @ApiOperation(value = "전시 정보 개요 목록" , notes = "전시 정보 개요 목록을 얻어옵니다.")
    @GetMapping("")
    public ResultDto getDisplayOverview(@RequestBody DisplaySearchDto displaySearchDto) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");
        result.setResultData(this.displayService.getDisplayOverview(displaySearchDto));

        return result;
    }

    /**
     * 전시 정보를 얻어온다.
     * @param displayNo 전시 고유 번호
     * @return          전시 정보
     */
    @ApiOperation(value = "전시 정보" , notes = "전시 정보를 얻어옵니다.")
    @GetMapping("/{display_no}")
    public ResultDto getDisplay(@PathVariable("display_no") Integer displayNo) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");
        result.setResultData(this.displayService.getDisplay(displayNo));

        return result;
    }

    /**
     *
     * @param display 전시 정보
     * @return 결과 응답
     */
    @ApiOperation(value = "전시 정보 설정" , notes = "전시 정보를 설정합니다.")
    @ApiImplicitParam(name = "display", value = "display", dataType = "DisplayDto", required = true)
    @PostMapping("")
    public ResultDto insertDisplay(@RequestBody DisplayDto display) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");
        result.setResultData(this.displayService.addDisplay(display));

        return result;
    }

    /**
     * 전시 정보를 수정하는 함수
     * @param displayNo 전시 고유 번호
     * @param display   전시 정보
     * @return          전시 정보
     */
    @ApiOperation(value = "전시 정보 설정" , notes = "전시 정보를 설정합니다.")
    @ApiImplicitParam(name = "display", value = "display", dataType = "DisplayDto", required = true)
    @PutMapping("/{display_no}")
    public ResultDto updateDisplay(@PathVariable("display_no") Integer displayNo, @RequestBody DisplayDto display) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");
        result.setResultData(this.displayService.updateDisplay(displayNo, display));

        return result;
    }


    /**
     * 전시 정보를 삭제하는 함수
     * @param displayNo 전시 고유 번호
     * @return          결과
     */
    @ApiOperation(value = "전시 정보 설정" , notes = "전시 정보를 설정합니다.")
    @DeleteMapping("/{display_no}")
    public ResultDto deleteDisplay(@PathVariable("display_no") Integer displayNo) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");
        result.setResultData(this.displayService.deleteDisplay(displayNo));

        return result;
    }
}
