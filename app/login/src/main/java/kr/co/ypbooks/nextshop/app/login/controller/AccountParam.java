package kr.co.ypbooks.nextshop.app.login.controller;

import lombok.Data;

@Data
public class AccountParam {
    private String userId;
    private String name;
    private String passwd;

    public AccountParam(String userId, String name, String passwd) {
        this.userId = userId;
        this.name = name;
        this.passwd = passwd;
    }
}
