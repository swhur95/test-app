package kr.co.ypbooks.nextshop.lib.common.kafka;

import org.apache.kafka.clients.consumer.ConsumerRecords;

public interface ConsumerMessageHelper {

    String myConsumerWork(ConsumerRecords<String, String> records, String topic);

    void noRecordsConsumer(String topic);

}
