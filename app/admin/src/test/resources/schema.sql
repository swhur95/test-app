drop table if exists display_layout;

create table display_layout comment '전시 layout' (
    display_seq         tinyint     not null    comment '표시 순번',
    display_category_cd varchar(4)  not null    comment '전시 분류 (0: 인기 검색어, 1: 인기 태그, 2: 분야별 베스트)',
    default_title       varchar(60) not null    comment '기본 제목',
    display_product_cnt tinyint     not null    comment '전시 상품 개수',
    layout_sub_info     longtext    not null    comment '서브 layout 정보',
    reg_date            datetime    not null    comment '등록 일시',
    upd_date            datetime    null        comment '수정 일시',
    primary key (display_seq)
);

drop table if exists display;

create table display
(
    display_subject    varchar(60) not null,
    expose_flag        boolean     not null,
    display_start_date datetime null,
    display_end_date   datetime null,
    display_info       longtext null,
    reg_date           datetime not null,
    upd_date           datetime null,
    primary key (display_subject)
);