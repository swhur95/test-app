package kr.co.ypbooks.nextshop.domain.book.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

import java.time.LocalDateTime;

@ApiModel
@Getter
public class BookSearchDto {
    /**
     * 출판일 조건 (시작 일시)
     */
    @ApiModelProperty(value = "출판일 조건 (시작일)", example = "2022-03-01 09:00:00")
    @JsonProperty("start_pub_date")
    private LocalDateTime startPubDate;

    /**
     * 출판일 조건 (종료 일시)
     */
    @ApiModelProperty(value = "출판일 조건 (종료일)", example = "2022-04-01 09:00:00")
    @JsonProperty("end_pub_date")
    private LocalDateTime endPubDate;

    /**
     * 도서 코드 조건
     */
    @ApiModelProperty(value = "도서 코드 조건", example = "도서 코드")
    @JsonProperty("book_cd")
    private String bookCd;

    /**
     * 도서명 조건
      */
    @ApiModelProperty(value = "도서명 조건", example = "도서명")
    @JsonProperty("book_name")
    private String bookName;

    /**
     * 출판사 코드 조건
     */
    @ApiModelProperty(value = "출판사 코드 조건", example = "출판사 코드")
    @JsonProperty("pub_company_cd")
    private String pubCompanyCd;

    /**
     * 작가 번호 조건
     */
    @ApiModelProperty(value = "작가 번호 조건", example = "작가 번호")
    @JsonProperty("writer_no")
    private String writerNo;

    /**
     * 역자 번호 조건
     */
    @ApiModelProperty(value = "역자 번호 조건", example = "역자 번호")
    @JsonProperty("trans_no")
    private String transNo;
}
