package kr.co.ypbooks.nextshop.domain.account.config;

import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * google登录授权配置
 */
@Configuration
@Slf4j
public class GoogleAuthorizationConfig {

    private static final String clientId = "434099646390-1qvln5ao33dtij7in0j5iodi320gd51u.apps.googleusercontent.com";
    private static final String clientSecret = "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";
    private static final String applicationName = "ypbook2022";
    private static final String redirectUrl = "https://localhost:8081/login/account/googleAuthorizing";


    @Bean(name = "googleAuthorization")
    public GoogleAuthorization googleFeed() {
        GoogleClientSecrets clientSecrets = null;
        try {
            GoogleClientSecrets.Details details = new GoogleClientSecrets.Details();
            details.setClientId(clientId);
            details.setClientSecret(clientSecret);
            clientSecrets = new GoogleClientSecrets();
            clientSecrets.setInstalled(details);
        } catch (Exception e) {
            log.error("authorization configuration error:{}", e.getMessage());
        }
        // 构建bean
        return GoogleAuthorization.builder()
                .googleClientSecrets(clientSecrets)
                .applicationName(applicationName)
                .redirectUrl(redirectUrl)
                .build();
    }
}
