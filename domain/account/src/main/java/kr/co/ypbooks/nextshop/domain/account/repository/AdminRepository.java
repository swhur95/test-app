package kr.co.ypbooks.nextshop.domain.account.repository;

import kr.co.ypbooks.nextshop.domain.account.model.Admin;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdminRepository extends JpaRepository<Admin, String> {
}
