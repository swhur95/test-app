package kr.co.ypbooks.nextshop.domain.shop.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Builder;
import lombok.Getter;

import java.time.LocalDateTime;

@ApiModel
@Getter
@Builder
public class DisplaySearchDto {
    /**
     * 전시 제목 조건
     */
    @ApiModelProperty(example = "전시 제목 조건 입니다.")
    @ApiParam(value = "전시 제목 조건")
    private String displaySubject;

    /**
     * 등록 일시 조건 (시작 일시)
     */
    @ApiModelProperty(example = "등록 일시 조건 입니다. (시작 일시)")
    @ApiParam(value = "등록 일시 조건 (시작 일시)")
    private LocalDateTime startRegDate;

    /**
     * 등록 일시 조건 (종료 일시)
     */
    @ApiModelProperty(example = "등록 일시 조건 입니다. (종료 일시)")
    @ApiParam(value = "등록 일시 조건 (종료 일시)")
    private LocalDateTime endRegDate;

    /**
     * 전시 시작 일시 조건 (시작 일시)
     */
    @ApiModelProperty(example = "전시 기간 조건 입니다. (시작 일시)")
    @ApiParam(value = "전시 기간 조건 (시작 일시)")
    private LocalDateTime startDisplayStartDate;

    /**
     * 전시 시작 일시 조건 (종료 일시)
     */
    @ApiModelProperty(example = "전시 기간 조건 입니다. (종료 일시)")
    @ApiParam(value = "전시 기간 조건 (종료 일시)")
    private LocalDateTime endDisplayStartDate;

    /**
     * 페이저 번호
     */
    @ApiModelProperty(example = "page 번호 입니다.")
    @ApiParam(value = "page 번호")
    private Integer pageNum;

    /**
     * 페이지내 요청 개수
     */
    @ApiModelProperty(example = "page 내 개수 입니다.")
    @ApiParam(value = "page 내 개수")
    private Integer pageSize;
}
