package kr.co.ypbooks.nextshop.domain.account.model.adminlog;

import com.vladmihalcea.hibernate.type.json.JsonStringType;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@Entity

@Table(name = "admin_action_hist")
@DynamicUpdate
@TypeDef(name = "json", typeClass = JsonStringType.class)

public class AdminActionHist implements Serializable {
    /**
     * 로그 번호
     */
    @Id
    private Integer logNo;
    /**
     * 회원번호
     */
    private String memberCd;
    /**
     * 관리자 ID
     */
    private String adminId;
    /**
     * 관리자 IP
     */
    private String adminIp;
    /**
     * 등록/가입일자
     */
    private LocalDateTime regDate;
    /**
     * 로그 타입 (1: 관리자 메뉴 조회, 2: 관리자 로그인, 3: 관리자 페이지 접근, 4: 개인정보 검색, 5: IP 관리, 6: 사용자 권한 설정, 7:  전시 설정)
     */
    private String logType;
    /**
     * 로그 컨텐츠
     */
    @Type(type = "json")
    @Column(columnDefinition = "json")
    private String logContent;

}
