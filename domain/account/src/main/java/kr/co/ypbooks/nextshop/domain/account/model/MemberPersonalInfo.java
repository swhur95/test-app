package kr.co.ypbooks.nextshop.domain.account.model;

import lombok.*;

import java.time.LocalDateTime;

/**
 * PERSONAL_INFO(개인 정보)
 */

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class MemberPersonalInfo {

    /**
     * 한국이름
     */
    private String krName;
    /**
     * 영문이름
     */
    private String enName;
    /**
     * 주민등록상 생년월일
     */
    private String rrnBirthDate;
    /**
     * 성별
     */
    private String sex;
    /**
     * 결혼 기념일
     */
    private LocalDateTime wedDate;
    /**
     * 생년월일
     */
    private LocalDateTime birthDate;
    /**
     * 음력 여부 (0: 양력, 1: 음력)
     */
    private String lunarFlag;
    /**
     * 직업
     */
    private String occupation;
    /**
     * 국가
     */
    private String nation;
    /**
     * 전화번호
     */
    private String phone;
    /**
     * 휴대전화
     */
    private String mobile;
    /**
     * 우편번호
     */
    private String postNo;
    /**
     * 주소 (도/시/동)
     */
    private String address1;
    /**
     * 주소 (기타 주소)
     */
    private String address2;
    /**
     * 이메일
     */
    private String email;
    /**
     * 연령대
     */
    private String ageGroup;
    /**
     * 관심 분야1
     */
    private String interestCat1;
    /**
     * 관심 분야2
     */
    private String interestCat2;
    /**
     * 관심 분야3
     */
    private String interestCat3;
    /**
     * 영수증 여부
     */
    private String receiptFlag;
    /**
     * 마케팅 활용 동의 구분
     */
    private String mkgFlag;
    /**
     * 개인정보동의사항 필수사항 동의여부
     */
    private String reqAgreeFlag;
    /**
     * 개인정보동의사항 선택사항 동의여부
     */
    private String optAgreeFlag;
}
