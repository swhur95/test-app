package kr.co.ypbooks.nextshop.domain.book.repository;

import kr.co.ypbooks.nextshop.domain.book.model.BookWriter;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookWriterRepository extends JpaRepository<BookWriter, String> {
}
