package kr.co.ypbooks.nextshop.domain.order.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;

import java.time.LocalDateTime;

@ApiModel
@Getter
@Builder
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ConsultSearchDto {
    /**
     * 상담 완료 여부
     */
    @ApiModelProperty(value = "상담 완료 여부", example = "0")
    private Boolean consultCompFlag;

    /**
     * 상담 분류 코드 조건
     */
    @ApiModelProperty(value = "상담 분류 조건", example = "1")
    private String consultTypeCd;

    /**
     * 등록 일시 조건 (시작)
     */
    @ApiModelProperty(value = "등록 일시 조건 (시작)", example = "2022-03-30 09:00:00")
    private LocalDateTime startRegDate;

    /**
     * 등록 일시 조건 (종료)
     */
    @ApiModelProperty(value = "주문 일시 조건 (종료)", example = "2022-03-31 09:00:00")
    private LocalDateTime endRegDate;

    /**
     * 상담 상태 조건
     */
    @ApiModelProperty(value = "상담 상태 코드", example = "1")
    private String consultStateCd;

    /**
     * 회원 이름 조건
     */
    @ApiModelProperty(value = "회원 이름", example = "회원 이름")
    private String memberName;

    /**
     * 회원 아이디 조건
     */
    @ApiModelProperty(value = "회원 ID", example = "회원 ID")
    private String memberId;

    /**
     * 회원 이메일 조건
     */
    @ApiModelProperty(value = "회원 이메일", example = "회원 이메일")
    private String memberEmail;

    /**
     * 회원 연락처
     */
    @ApiModelProperty(value = "회원 연락처", example = "회원 연락처")
    private String memberPhone;

    /**
     * 회원 연락처
     */
    @ApiModelProperty(value = "회원 핸드폰", example = "회원 핸드폰")
    private String memberMobile;

    /**
     * 관리자 ID 조건
     */
    @ApiModelProperty(value = "관리자 ID", example = "admin")
    private String adminId;

    /**
     * 상담 제목 조건
     */
    @ApiModelProperty(value = "상담 제목", example = "배송 지연")
    private String consultSubject;

    /**
     * 주문 번호 조건
     */
    @ApiModelProperty(value = "주문 번호", example = "12345678")
    private Long orderId;

    /**
     * 상담 번호 조건
     */
    @ApiModelProperty(value = "상담 번호", example = "12345678")
    private Long consultNo;
}
