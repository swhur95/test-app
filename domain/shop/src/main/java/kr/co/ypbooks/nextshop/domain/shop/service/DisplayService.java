package kr.co.ypbooks.nextshop.domain.shop.service;

import kr.co.ypbooks.nextshop.lib.common.util.JsonUtil;
import kr.co.ypbooks.nextshop.domain.shop.dto.DisplayDto;
import kr.co.ypbooks.nextshop.domain.shop.dto.DisplayLayoutDto;
import kr.co.ypbooks.nextshop.domain.shop.dto.DisplayOverviewDto;
import kr.co.ypbooks.nextshop.domain.shop.dto.DisplaySearchDto;
import kr.co.ypbooks.nextshop.domain.shop.model.Display;
import kr.co.ypbooks.nextshop.domain.shop.model.DisplayLayout;
import kr.co.ypbooks.nextshop.domain.shop.repository.DisplayLayoutRepository;
import kr.co.ypbooks.nextshop.domain.shop.repository.DisplayRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.Predicate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class DisplayService {
    /**
     * 전시 정보 저장소
     */
    private final DisplayRepository displayRepository;

    /**
     * 전시 layout 정보 저장소
     */
    private final DisplayLayoutRepository displayLayoutRepository;

    /**
     * 생성자
     *
     * @param displayRepository       전시 정보 저장소
     * @param displayLayoutRepository 전시 layout 정보 저장소
     */
    public DisplayService(DisplayRepository displayRepository, DisplayLayoutRepository displayLayoutRepository) {
        this.displayRepository = displayRepository;
        this.displayLayoutRepository = displayLayoutRepository;
    }

    public List<DisplayLayoutDto> getDisplayLayout() {
        Sort sort = Sort.by(Sort.Direction.ASC, "displaySeq");
        return displayLayoutRepository.findAll(sort).stream().map(this::convertToDisplayLayoutDto).collect(Collectors.toList());
    }

    public DisplayLayoutDto getDisplayLayout(Byte displaySeq) {
        Optional<DisplayLayout> displayLayoutOptional = displayLayoutRepository.findById(displaySeq);

        return displayLayoutOptional.map(this::convertToDisplayLayoutDto).orElse(null);
    }

    public Boolean updateDisplayLayout(Byte displaySeq, DisplayLayoutDto displayLayoutDto) {
        Optional<DisplayLayout> displayLayoutOptional = displayLayoutRepository.findById(displaySeq);
        if(displayLayoutOptional.isEmpty()) {
            return false;
        }

        DisplayLayout displayLayout = displayLayoutOptional.get();
        this.displayLayoutRepository.save(convertToDisplayLayout(displayLayoutDto, displaySeq, displayLayout.getRegDate(), LocalDateTime.now()));
        return true;
    }

    public Boolean insertDisplayLayout(Byte displaySeq, DisplayLayoutDto displayLayoutDto) {
        Optional<DisplayLayout> displayLayoutOptional = this.displayLayoutRepository.findById(displaySeq);

        if(displayLayoutOptional.isPresent()) {
            return false;
        }

        this.displayLayoutRepository.save(convertToDisplayLayout(displayLayoutDto, displaySeq, LocalDateTime.now(), null));
        return true;
    }

    public void insertDisplayLayout(List<DisplayLayoutDto> displayLayoutDtoList) {
        // 기존 정보를 삭제하고, 새로운 정보를 추가한다.
        this.displayLayoutRepository.deleteAll();
        this.displayLayoutRepository.saveAll(displayLayoutDtoList
                .stream().map(this::convertToDisplayLayout).collect(Collectors.toList()));
    }

    public Page<DisplayOverviewDto> getDisplayOverview(DisplaySearchDto displaySearchDto) {
        Sort sort = Sort.by(Sort.Direction.DESC, "");

        Pageable pageAble = PageRequest.of(displaySearchDto.getPageNum() - 1, displaySearchDto.getPageSize(), sort);

        Specification<Display> spec = (root, query, cb) -> {
            List<Predicate> pr = new ArrayList<>();

            if (displaySearchDto.getStartRegDate() != null && displaySearchDto.getEndRegDate() != null) {
                pr.add(cb.between(root.get("regDate"), displaySearchDto.getStartRegDate(), displaySearchDto.getEndRegDate()));
            }

            if (displaySearchDto.getStartDisplayStartDate() != null && displaySearchDto.getEndDisplayStartDate() != null) {
                pr.add(cb.between(root.get("displayStartDate"), displaySearchDto.getStartDisplayStartDate(), displaySearchDto.getEndDisplayStartDate()));
            }

            if (displaySearchDto.getDisplaySubject() != null) {
                pr.add(cb.like(root.get("displaySubject"), "%" + displaySearchDto.getDisplaySubject() + "%"));

            }

            Predicate[] predicates = new Predicate[pr.size()];
            return cb.and(pr.toArray(predicates));
        };

        return displayRepository.findAll(spec, pageAble).map(this::convertToDisplayOverviewDto);
    }

    public DisplayDto getDisplay(Integer displayNo) {
        Optional<Display> displayOptional = this.displayRepository.findById(displayNo);

        return displayOptional.map(this::convertToDisplayDto).orElse(null);
    }

    public Integer addDisplay(DisplayDto displayDto) {
        return this.displayRepository.save(convertToDisplay(displayDto)).getDisplayNo();
    }

    public Boolean updateDisplay(Integer displayNo, DisplayDto displayDto) {
        Optional<Display> displayOptional = this.displayRepository.findById(displayNo);
        if(displayOptional.isEmpty()) {
            return false;
        }

        this.displayRepository.save(convertToDisplay(displayDto, displayNo, displayOptional.get().getRegDate(), LocalDateTime.now()));
        return true;
    }

    public Boolean deleteDisplay(Integer displayNo) {
        Optional<Display> displayOptional = this.displayRepository.findById(displayNo);
        if(displayOptional.isEmpty()) {
            return false;
        }

        this.displayRepository.delete(displayOptional.get());
        return true;
    }


    private DisplayLayoutDto convertToDisplayLayoutDto(DisplayLayout displayLayout) {
        DisplayLayoutDto.LayoutSubInfo layoutSubInfo = new DisplayLayoutDto.LayoutSubInfo();

        try {
            // 서브 정보가 존재하는 경우
            if (displayLayout.getLayoutSubInfo() != null) {
                layoutSubInfo = (DisplayLayoutDto.LayoutSubInfo) JsonUtil.jsonToObject(displayLayout.getLayoutSubInfo(), layoutSubInfo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return DisplayLayoutDto.builder()
                .displaySeq(displayLayout.getDisplaySeq())
                .displayCategoryCd(displayLayout.getDisplayCategoryCd())
                .defaultTitle(displayLayout.getDefaultTitle())
                .displayProductCnt(displayLayout.getDisplayProductCnt())
                .layoutSubInfo(layoutSubInfo)
                .regDate(displayLayout.getRegDate())
                .updDate(displayLayout.getUpdDate()).build();
    }

    private DisplayLayout convertToDisplayLayout(DisplayLayoutDto displayLayoutDto) {
        return convertToDisplayLayout(displayLayoutDto, displayLayoutDto.getDisplaySeq(), LocalDateTime.now(), null);
    }

    private DisplayLayout convertToDisplayLayout(DisplayLayoutDto displayLayoutDto, Byte displaySeq, LocalDateTime regDate, LocalDateTime updDate) {
        String layoutSubInfo = JsonUtil.objectToJsonNoException(displayLayoutDto.getLayoutSubInfo());

        return DisplayLayout.builder()
                .displaySeq(displaySeq)
                .displayCategoryCd(displayLayoutDto.getDisplayCategoryCd())
                .defaultTitle(displayLayoutDto.getDefaultTitle())
                .displayProductCnt(displayLayoutDto.getDisplayProductCnt())
                .layoutSubInfo(layoutSubInfo)
                .regDate(regDate)
                .updDate(updDate).build();
    }

    /**
     * Display(Entity)를 DisplayOverviewDto 로 변경하는 함수
     * @param display Display(Entity)
     * @return DisplayDto
     */
    private DisplayOverviewDto convertToDisplayOverviewDto(Display display) {
        return DisplayOverviewDto.builder()
                .displayNo(display.getDisplayNo())
                .displaySubject(display.getDisplaySubject())
                .regAdminName(display.getRegAdminName())
                .exposeFlag(display.getExposeFlag())
                .displayStartDate(display.getDisplayStartDate())
                .displayEndDate(display.getDisplayEndDate())
                .regDate(display.getRegDate())
                .updDate(display.getUpdDate()).build();
    }

    /**
     * Display(Entity)를 DisplayDto 로 변경하는 함수
     * @param display Display(Entity)
     * @return DisplayDto
     */
    private DisplayDto convertToDisplayDto(Display display) {
        DisplayDto.DisplayInfo displayInfo = JsonUtil.jsonToObjectNoException(display.getDisplayInfo(), DisplayDto.DisplayInfo.class);

        return DisplayDto.builder()
                .displayNo(display.getDisplayNo())
                .displaySubject(display.getDisplaySubject())
                .regAdminName(display.getRegAdminName())
                .exposeFlag(display.getExposeFlag())
                .displayStartDate(display.getDisplayStartDate())
                .displayEndDate(display.getDisplayEndDate())
                .displayInfo(displayInfo)
                .regDate(display.getRegDate())
                .updDate(display.getUpdDate()).build();
    }

    private Display convertToDisplay(DisplayDto displayDto) {
        return convertToDisplay(displayDto, null, LocalDateTime.now(), null);
    }

    private Display convertToDisplay(DisplayDto displayDto, Integer displayNo, LocalDateTime regDate, LocalDateTime updDate) {
        String displaySubInfo = JsonUtil.objectToJsonNoException(displayDto.getDisplayInfo());

        return Display.builder()
                .displayNo(displayNo)
                .displaySubject(displayDto.getDisplaySubject())
                .regAdminName(displayDto.getRegAdminName())
                .exposeFlag(displayDto.getExposeFlag())
                .displayStartDate(displayDto.getDisplayStartDate())
                .displayEndDate(displayDto.getDisplayEndDate())
                .displayInfo(displaySubInfo)
                .regDate(regDate)
                .updDate(updDate).build();
    }
}
