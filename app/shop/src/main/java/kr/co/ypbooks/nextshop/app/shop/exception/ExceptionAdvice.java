package kr.co.ypbooks.nextshop.app.shop.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionAdvice {
    private static Logger logger = LoggerFactory.getLogger(ExceptionAdvice.class);

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public ResultBean defaultException (Exception e) {
        logger.error("system error:{},please check it!",e.getMessage());
        if (null != e.getMessage()){
            if (e.getMessage().contains("Subject does not have permission")){
                return ResultBean.builder()
                        .code(ResultEnum.NOPERMISS.getCode())
                        .message(ResultEnum.NOPERMISS.getMessage())
                        .build();
            }
            return ResultBean.builder()
                    .code(ResultEnum.EXCEPTION.getCode())
                    .message(ResultEnum.EXCEPTION.getMessage())
                    .build();
        }
        return ResultBean.builder()
                .code(ResultEnum.EXCEPTION.getCode())
                .message("Null point Exception")
                .build();

    }

    @ExceptionHandler(value = MyException.class)
    @ResponseBody
    public ResultBean myException (MyException e) {
       e.printStackTrace();
       Integer code = e.getCode();
       String message = e.getMessage();

       if(null == e.getCode()){
           code = ResultEnum.EXCEPTION.getCode();
       }

       if(null == e.getMessage()){
           message = ResultEnum.EXCEPTION.getMessage();
       }

       return ResultBean.builder()
               .code(code)
               .message(message)
               .build();
    }
}

