package kr.co.ypbooks.nextshop.domain.book.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
@Entity
public class TranslatorCategory {
    /**
     * 역자 카테고리 고유 Id
     */
    @EmbeddedId
    private TranslatorCategoryId translatorCategoryId;

    /**
     * 등록 일시
     */
    private LocalDateTime regDate;
}
