package kr.co.ypbooks.nextshop.app.shop.controller;

import kr.co.ypbooks.nextshop.lib.common.operation.RedisOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/redis")
public class RedisTestController {

    private RedisOperation redisOperation;

    public RedisTestController(RedisOperation redisOperation) {
        this.redisOperation = redisOperation;
    }

    @GetMapping("/get/{key}")
    public Object get(@PathVariable String key) {
        return this.redisOperation.get(key);
    }

    @GetMapping("/setValue")
    public void setValue() {
        int count = 10;
        redisOperation.set("intTypeTest", count);
        ShopParam shopParam = new ShopParam("name","address");
        redisOperation.set("modelTypeTest", shopParam);
    }
}
