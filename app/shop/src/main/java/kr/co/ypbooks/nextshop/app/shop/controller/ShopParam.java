package kr.co.ypbooks.nextshop.app.shop.controller;

import lombok.Data;

@Data
public class ShopParam {
    private final String name;
    private final String address;

    public ShopParam(String name, String address) {
        this.name = name;
        this.address = address;
    }
}
