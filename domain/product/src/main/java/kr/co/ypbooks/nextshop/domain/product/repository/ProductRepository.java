package kr.co.ypbooks.nextshop.domain.product.repository;

import kr.co.ypbooks.nextshop.domain.product.model.Product;
import kr.co.ypbooks.nextshop.domain.product.model.ProductId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ProductRepository extends JpaRepository<Product, ProductId>, JpaSpecificationExecutor<Product> {
}
