package kr.co.ypbooks.nextshop.domain.book.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
@Entity
public class BookWriter {
    /**
     * 도서 코드 (Product Code)
     */
    @Id
    private String bookCd;

    /**
     * 작가 번호
     */
    private Long writerNo;

    /**
     * 등록 일시
     */
    private LocalDateTime regDate;
}
