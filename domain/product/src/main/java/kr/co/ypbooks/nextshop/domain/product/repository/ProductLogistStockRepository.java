package kr.co.ypbooks.nextshop.domain.product.repository;

import kr.co.ypbooks.nextshop.domain.product.model.ProductLogistStock;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductLogistStockRepository extends JpaRepository<ProductLogistStock, Long> {
}
