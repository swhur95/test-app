package kr.co.ypbooks.nextshop.domain.shop.repository;

import kr.co.ypbooks.nextshop.domain.shop.model.BranchDayOff;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface BranchDayOffRepository extends JpaRepository<BranchDayOff, Long>, JpaSpecificationExecutor<BranchDayOff> {
}
