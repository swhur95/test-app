package kr.co.ypbooks.nextshop.domain.book.service;

import kr.co.ypbooks.nextshop.domain.book.repository.TranslatorCategoryRepository;
import kr.co.ypbooks.nextshop.domain.book.repository.TranslatorRepository;
import kr.co.ypbooks.nextshop.domain.book.repository.TranslatorTagRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class TranslatorService {
    private final TranslatorRepository translatorRepository;

    private final TranslatorCategoryRepository translatorCategoryRepository;

    private final TranslatorTagRepository translatorTagRepository;

    public TranslatorService(TranslatorRepository translatorRepository,
                             TranslatorCategoryRepository translatorCategoryRepository,
                             TranslatorTagRepository translatorTagRepository) {
        this.translatorRepository = translatorRepository;
        this.translatorCategoryRepository = translatorCategoryRepository;
        this.translatorTagRepository = translatorTagRepository;
    }
}
