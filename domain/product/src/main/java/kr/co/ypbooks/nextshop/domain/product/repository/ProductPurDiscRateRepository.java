package kr.co.ypbooks.nextshop.domain.product.repository;

import kr.co.ypbooks.nextshop.domain.product.model.ProductPurDiscRate;
import kr.co.ypbooks.nextshop.domain.product.model.ProductPurDiscRateId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductPurDiscRateRepository extends JpaRepository<ProductPurDiscRate, ProductPurDiscRateId> {
}
