package kr.co.ypbooks.nextshop.domain.shop.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class BranchImage {
    /**
     * 지점 내부 이미지 번호 (고유 번호)
     */
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long branchImgNo;

    /**
     * 지점 코드
     */
    private String branchCd;

    /**
     * 지점명
     */
    private String branchName;

    /**
     * 지점 이미지 파일명
     */
    private String branchImgName;

    /**
     * 지점 이미지 경로
     */
    private String branchImgPath;

    /**
     * 지점 이미지 내용 (설명)
     */
    private String branchImgContent;

    /**
     * 등록한 관리자 ID
     */
    private String regAdminId;

    /**
     * 등록한 관리자명
     */
    private String regAdminName;

    /**
     * 등록 일시
     */
    private LocalDateTime regDate;

    /**
     * 수정 일시
     */
    private LocalDateTime updDate;
}
