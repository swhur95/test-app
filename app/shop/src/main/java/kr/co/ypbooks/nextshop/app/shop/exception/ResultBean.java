package kr.co.ypbooks.nextshop.app.shop.exception;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ResultBean {
    private Integer code; //action code
    private  String message; //return message
    private  Object data; // encapsulation data
}
