package kr.co.ypbooks.nextshop.domain.account.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class AdminDto {
    /**
     * 관리자 Id
     */
    @ApiModelProperty(value = "관리자ID", example = "admin", required = true)
    private String adminId;

    /**
     * 관리자 비밀번호
     */
    @ApiModelProperty(value = "관리자비밀번호", example = "password", required = true)
    private String adminPwd;

    /**
     * 관리자명
     */
    @ApiModelProperty(value = "관리자명", example = "관리자", required = true)
    private String adminName;

    /**
     * 부서명
     */
    @ApiModelProperty(value = "부서명", example = "지점", required = true)
    private String companyDeptName;

    /**
     * 직위
     */
    @ApiModelProperty(value = "직급", example = "지점", required = true)
    private String companyPosition;

    /**
     * 지점 코드
     */
    @ApiModelProperty(value = "지점 코드", example = "1000", required = true)
    private String branchCd;

    /**
     * 회사 전화번호
     */
    @ApiModelProperty(value = "전화 번호", example = "0212345678", required = true)
    private String companyPhone;

    /**
     * 회사 이메일
     */
    @ApiModelProperty(value = "이메일", example = "이메일", required = true)
    private String companyEmail;

    /**
     * 회사 담당 업무
     */
    @ApiModelProperty(value = "담당업무", example = "담당업무", required = true)
    private String companyAssignTask;
}
