package kr.co.ypbooks.nextshop.domain.account.dto;

import lombok.Getter;

@Getter
public class AccountDto {
    private final Long id;
    private final String userId;
    private final String name;

    public AccountDto(Long id, String userId, String name) {
        this.id = id;
        this.userId = userId;
        this.name = name;
    }
}
