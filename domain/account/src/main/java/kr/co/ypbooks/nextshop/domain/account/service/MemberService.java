package kr.co.ypbooks.nextshop.domain.account.service;

import kr.co.ypbooks.nextshop.domain.account.dto.MemberDto;
import kr.co.ypbooks.nextshop.domain.account.dto.MemberVo;
import kr.co.ypbooks.nextshop.domain.account.model.Member;
import kr.co.ypbooks.nextshop.domain.account.repository.MemberRepository;
import kr.co.ypbooks.nextshop.domain.account.utils.DateUtils;
import kr.co.ypbooks.nextshop.lib.common.jpa.common.service.CommonService;
import kr.co.ypbooks.nextshop.lib.common.operation.RedisOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class MemberService extends CommonService<MemberVo, Member, String> {

    @Autowired
    private RedisOperation redisOperation;

    @Autowired
    MemberRepository memberRepository;

    public Page<Member> getMemberList(MemberDto memberDto, int pageNum, int pageSize) {

        Sort sort = Sort.by(Sort.Direction.DESC, "regDate");
        Pageable pageAble = PageRequest.of(pageNum - 1, pageSize, sort);

        Specification<Member> spec = new Specification<Member>() {
            @Override
            public Predicate toPredicate(Root<Member> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                List<Predicate> pr = new ArrayList<>();
                if (memberDto != null) {
                    if (memberDto.getStartRegDate() != null && memberDto.getEndRegDate() != null) {
                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                        Date startDate = null;
                        Date endDate = null;
                        try {
                            startDate = format.parse(memberDto.getStartRegDate());
                            LocalDateTime localStartDate = DateUtils.asLocalDateTime(startDate);
                            endDate = format.parse(memberDto.getEndRegDate());
                            LocalDateTime localEndDate = DateUtils.asLocalDateTime(endDate);
                            pr.add(cb.between(root.get("regDate"), localStartDate, localEndDate));
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                    if (memberDto.getMemberState() != null) {
                        pr.add(cb.equal(root.get("memberState"), memberDto.getMemberState()));
                    }

                    //use for name like krName or like enName
                    List<Predicate> prName = new ArrayList<>();
                    if (memberDto.getName() != null) {
                        prName.add(
                                cb.like(
                                        cb.function(
                                                "JSON_EXTRACT",
                                                String.class,
                                                root.get("personalInfo"), cb.literal("$.krName")
                                        ),"%" + memberDto.getName() + "%")
                        );
                    }
                    if (memberDto.getName() != null) {
                        prName.add(
                                cb.like(
                                        cb.function(
                                                "JSON_EXTRACT",
                                                String.class,
                                                root.get("personalInfo"), cb.literal("$.enName")
                                        ), "%" + memberDto.getName() + "%")
                        );
                    }
                    if (prName.size() > 0) {
                        pr.add(cb.or(prName.toArray(new Predicate[0])));
                    }

                    if (memberDto.getMemberId() != null) {
                        pr.add(cb.equal(root.get("memberId"), memberDto.getMemberId()));
                    }
                    if (memberDto.getMemberCd() != null) {
                        pr.add(cb.equal(root.get("memberCd"), memberDto.getMemberCd()));
                    }
                    if (memberDto.getPhone() != null) {
                        pr.add(
                                cb.equal(
                                        cb.function(
                                                "JSON_EXTRACT",
                                                String.class,
                                                root.get("personalInfo"), cb.literal("$.phone")
                                        ), memberDto.getPhone())
                        );
                    }
                    if (memberDto.getEmail() != null) {
                        pr.add(
                                cb.equal(
                                        cb.function(
                                                "JSON_EXTRACT",
                                                String.class,
                                                root.get("personalInfo"), cb.literal("$.email")
                                        ), memberDto.getEmail())
                        );
                    }
                    Predicate[] pSize = new Predicate[pr.size()];
                    return cb.and(pr.toArray(pSize));
                }
                return null;
            }
        };
        return memberRepository.findAll(spec, pageAble);
    }


}
