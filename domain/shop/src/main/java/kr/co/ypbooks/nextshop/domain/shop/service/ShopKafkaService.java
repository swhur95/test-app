package kr.co.ypbooks.nextshop.domain.shop.service;

import kr.co.ypbooks.nextshop.lib.common.kafka.ConsumerMessageHelper;
import kr.co.ypbooks.nextshop.lib.common.kafka.KafkaAutoTableHandler;
import kr.co.ypbooks.nextshop.lib.common.property.KafkaProperty;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Slf4j
@Service
public class ShopKafkaService implements ConsumerMessageHelper {

    @Autowired
    KafkaAutoTableHandler kafkaConsumerHelper;

    @Override
    public String myConsumerWork(ConsumerRecords<String, String> records, String topic) {

        for (ConsumerRecord<String, String> record : records) {
            log.info("topic->" + record.topic() + " partition->" + record.partition() + "  key->" + record.key()
                            + "  value->" + record.value() + "  offset->" + record.offset());
        }
        return "myConsumerReturnString is finish";
    }

    @Override
    public void noRecordsConsumer(String topic) {

    }


    public void makeShopConsumer(ArrayList<KafkaProperty> kafkaProperties) throws Exception{

        //before send do something shop business

        kafkaConsumerHelper.initMyTopicListener(this, kafkaProperties);
    }
}
