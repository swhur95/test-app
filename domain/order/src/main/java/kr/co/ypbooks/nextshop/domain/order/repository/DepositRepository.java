package kr.co.ypbooks.nextshop.domain.order.repository;

import kr.co.ypbooks.nextshop.domain.order.model.DepositHist;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DepositRepository extends JpaRepository<DepositHist, Long> {
}
