package kr.co.ypbooks.nextshop.lib.common.kafka;

import kr.co.ypbooks.nextshop.lib.common.config.KafkaConfig;
import kr.co.ypbooks.nextshop.lib.common.property.KafkaProperty;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.admin.AdminClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Slf4j
@Service
public class KafkaAutoTableHandler {

    @Autowired
    AdminClient adminClient;

    @Autowired
    KafkaConfig kfakaConfig;

    /**
     * init Mytopic listener
     * @param consumerMessageHelper
     * @param kafkaProperties
     *
     */
    public void initMyTopicListener(ConsumerMessageHelper consumerMessageHelper, ArrayList<KafkaProperty> kafkaProperties) throws Exception{

        for (KafkaProperty kafkaProperty : kafkaProperties){
            //use group id
            if (kafkaProperty.getGroupIds().size() > 0){
                for (String groupId :kafkaProperty.getGroupIds()){
                    String poolKey = kafkaProperty.getTopicName().concat("-").concat(groupId);
                    if(!kfakaConfig.KFAKACONSUMERRUNNABLE_POOL.containsKey(poolKey)) {
                        initTopicListener(consumerMessageHelper,kafkaProperty.getTopicName(),groupId);
                    } else {
                        log.info("Kafka consumer poolKey(topic-groupId): "+poolKey+" is existed");
                    }
                }
            }else {
                //group id  is none
                if(!kfakaConfig.KFAKACONSUMERRUNNABLE_POOL.containsKey(kafkaProperty.getTopicName())) {
                    initTopicListener(consumerMessageHelper,kafkaProperty.getTopicName(),null);
                } else {
                    log.info("Kafka consumer topic(no group): "+kafkaProperty.getTopicName()+" is existed");
                }
            }
        }
    }

    /**
     * init topic listener
     * @param consumerMessageHelper
     * @param topic
     * @param groupId
     */
    public void initTopicListener(ConsumerMessageHelper consumerMessageHelper, String topic, String groupId){
        String poolKey = topic;
        KfakaConsumerRunnable consumerRunnable = new KfakaConsumerRunnable();
        consumerRunnable.setConsumerConfigs(kfakaConfig.consumerConfigs());
        consumerRunnable.setTopicName(topic);
        if (groupId != null){
            consumerRunnable.setGroupId(groupId);
            poolKey.concat("-").concat(groupId);
        }
        consumerRunnable.setConsumerMessageHelper(consumerMessageHelper);
        kfakaConfig.KFAKACONSUMERRUNNABLE_POOL.put(poolKey,consumerRunnable);
        consumerRunnable.start();
    }


 /*

    //one topic with more group and filter
    @KafkaListener(topics = ORDER_TOPIC, groupId = CONSUMER_GROUP_PREFIX + ORDER_TOPIC, containerFactory = "filterContainerFactoryByIP")
    public void multiReceived(ConsumerRecord consumerRecord) throws Exception {
        log.info("multiReceived Message: {},{},{}", consumerRecord.partition(), consumerRecord.offset(), consumerRecord.value());
    }

    //topic b
    @KafkaListener(topics = SHOP_TOPIC)
    @SendTo(ORDER_TOPIC)
    public String shopReceived(ConsumerRecord consumerRecord) throws Exception {
        log.info("shopReceived Message: {},{},{}", consumerRecord.partition(), consumerRecord.offset(), consumerRecord.value());
        return consumerRecord.value().toString();
    }
*/

/*
    public void initTopicListener(ConsumerMessageHelper consumerMessageHelper) throws Exception{

        ListTopicsResult result = adminClient.listTopics();
        KafkaFuture<Set<String>> names = result.names();
        for (String topic : names.get()) {
            if(!kfakaConfig.KFAKACONSUMERRUNNABLE_POOL.containsKey(topic)) {
                KfakaConsumerRunnable consumerRunnable = new KfakaConsumerRunnable();
                consumerRunnable.setConsumerConfigs(kfakaConfig.consumerConfigs());
                consumerRunnable.setTopicName(topic);
                consumerRunnable.setConsumerMessageHelper(consumerMessageHelper);
                kfakaConfig.KFAKACONSUMERRUNNABLE_POOL.put(topic,consumerRunnable);
                consumerRunnable.start();
            } else {
                log.info("Kafka topic: "+topic+" is existed");
            }
        }
    }
*/


}
