package kr.co.ypbooks.nextshop.domain.book.service;

import kr.co.ypbooks.nextshop.lib.common.util.JsonUtil;
import kr.co.ypbooks.nextshop.domain.book.dto.WriterDto;
import kr.co.ypbooks.nextshop.domain.book.dto.WriterOverviewDto;
import kr.co.ypbooks.nextshop.domain.book.model.Writer;
import kr.co.ypbooks.nextshop.domain.book.repository.WriterCategoryRepository;
import kr.co.ypbooks.nextshop.domain.book.repository.WriterRepository;
import kr.co.ypbooks.nextshop.domain.book.repository.WriterTagRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class WriterService {
    /**
     * 작가 정보 저장소
     */
    private final WriterRepository writerRepository;

    /**
     * 작가 분야 저장소
     */
    private final WriterCategoryRepository writerCategoryRepository;

    /**
     * 작가 태그 단어 저장소
     */
    private final WriterTagRepository writerTagRepository;

    /**
     * 생성자
     * @param writerRepository 작가 정보 저장소
     * @param writerCategoryRepository 작가 분야 저장소
     * @param writerTagRepository 작가 태그 단어 저장소
     */
    public WriterService(WriterRepository writerRepository, WriterCategoryRepository writerCategoryRepository, WriterTagRepository writerTagRepository) {
        this.writerRepository = writerRepository;
        this.writerCategoryRepository = writerCategoryRepository;
        this.writerTagRepository = writerTagRepository;
    }

    private WriterDto convertToWriterDto(Writer writer, List<String> writerCategoryCdList, List<String> writerTagWordList) {
        WriterDto.WriterInfo writerInfo = JsonUtil.jsonToObjectNoException(writer.getWriterInfo(), WriterDto.WriterInfo.class);

        return WriterDto.builder()
                .writerNo(writer.getWriterNo())
                .writerName(writer.getWriterName())
                .domesticFlag(writer.getDomesticFlag())
                .birthPlace(writer.getBirthPlace())
                .writerInfo(writerInfo)
                .writerCategoryCdList(writerCategoryCdList)
                .writerTagWordList(writerTagWordList)
                .regDate(writer.getRegDate())
                .updDate(writer.getUpdDate()).build();
    }

    private WriterOverviewDto convertToWriterOverviewDto(Writer writer, List<String> writerCategoryCdList) {
        WriterDto.WriterInfo writerInfo = JsonUtil.jsonToObjectNoException(writer.getWriterInfo(), WriterDto.WriterInfo.class);

        return WriterOverviewDto.builder()
                .writerNo(writer.getWriterNo())
                .writerName(writer.getWriterName())
                .writerNameLang(writerInfo.getWriterNameLang())
                .writerCategoryCdList(writerCategoryCdList)
                .domesticFlag(writer.getDomesticFlag())
                .birthPlace(writer.getBirthPlace())
                .birthDate(writerInfo.getBirthDate())
                .regDate(writer.getRegDate())
                .updDate(writer.getUpdDate()).build();
    }
}
