package kr.co.ypbooks.nextshop.domain.account.config;

import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import lombok.*;

import java.util.Collections;
import java.util.List;

/**
 * google authorization
 */
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class GoogleAuthorization {
    // applicationName
    private String applicationName;
    // redirectUrl
    private String redirectUrl;
    // application credentials
    private GoogleClientSecrets googleClientSecrets;
    // authorized domain
    private final static List<String> scopes = Collections.singletonList(
            "https://www.googleapis.com/auth/userinfo.email"
    );

    public List<String> getScopes() {
        return scopes;
    }

}
