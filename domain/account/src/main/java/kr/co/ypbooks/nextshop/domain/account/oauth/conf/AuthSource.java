package kr.co.ypbooks.nextshop.domain.account.oauth.conf;


import kr.co.ypbooks.nextshop.domain.account.oauth.enums.AuthResponseStatus;
import kr.co.ypbooks.nextshop.domain.account.oauth.exception.AuthException;

public interface AuthSource {

    String authorize();

    String accessToken();

    String userInfo();

    String profileUrl();

    void setAuthorize(String url);

    void setAccessToken(String url);


    default String revoke() {
        throw new AuthException(AuthResponseStatus.UNSUPPORTED);
    }

    default String refresh() {
        throw new AuthException(AuthResponseStatus.UNSUPPORTED);
    }

    default String getName() {
        if (this instanceof Enum) {
            return String.valueOf(this);
        }
        return this.getClass().getSimpleName();
    }
}
