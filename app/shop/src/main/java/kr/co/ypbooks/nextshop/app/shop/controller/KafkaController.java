package kr.co.ypbooks.nextshop.app.shop.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import kr.co.ypbooks.nextshop.lib.common.kafka.KafkaProducer;
import kr.co.ypbooks.nextshop.lib.common.property.KafkaProperty;
import kr.co.ypbooks.nextshop.domain.shop.service.ShopKafkaService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Api(tags = "kafka")
@RequestMapping("/kafka")
@RestController
public class KafkaController {

    @Autowired
    KafkaProducer kafkaProducer;
    @Autowired
    ShopKafkaService shopKafkaService;

    @ApiOperation(value = "asyncProducer", notes = "asyncProducer")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "topic", paramType = "query", required = true, dataType = "String", example = "shopTopic1"),
            @ApiImplicitParam(name = "key", paramType = "query", required = false, dataType = "String", example = "shop"),
            @ApiImplicitParam(name = "message", paramType = "query", required = true, dataType = "String", example = "asyncProducer send message")
    })
    @RequestMapping(value = "/asyncProducer", method = RequestMethod.PUT)
    public void kafkaAsyncProducer(String topic , String key, String message) {
        try{
            List<KafkaProperty> topicList = new ArrayList<>();
            topicList.add(KafkaProperty.builder().topicName(topic).partition(10).replica((short) 1).build());
            kafkaProducer.asyncProducer(topicList,key,message);

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @ApiOperation(value = "syncProducer", notes = "syncProducer")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "topics", paramType = "query", required = true, dataType = "String", example = "shopTopic2,shopTopic3"),
            @ApiImplicitParam(name = "key", paramType = "query", required = false, dataType = "String", example = "shop"),
            @ApiImplicitParam(name = "message", paramType = "query", required = true, dataType = "String", example = "syncProducer send message")
    })
    @RequestMapping(value = "/syncProducer", method = RequestMethod.PUT)
    public void kafkaSyncProducer(String topics , String key, String message) {
        try{

            List<KafkaProperty> topicList = new ArrayList<>();
            if (StringUtils.isNotBlank(topics)){
                for (String topic : topics.split(",")) {
                    topicList.add(KafkaProperty.builder().topicName(topic).partition(3).replica((short) 1).build());
                }
                List<KafkaProperty> result = kafkaProducer.syncProducer(topicList, key, message);
                for (KafkaProperty p : result){
                    log.info("result ingo : {} , {}", p.getTopicName() , p.isSendResult());
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @ApiOperation(value = "initConsumer", notes = "initConsumer")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "topic", paramType = "query", required = true, dataType = "String", example = "shopTopic1"),
            @ApiImplicitParam(name = "groups", paramType = "query", required = true, dataType = "String", example = "group1,group2"),
            @ApiImplicitParam(name = "quantity", paramType = "query", required = true, dataType = "Integer", example = "1")
    })
    @RequestMapping(value = "/initConsumer", method = RequestMethod.PUT)
    public void initConsumer(String topic , String groups,Integer quantity) {
        try{

            KafkaProperty kpt = KafkaProperty.builder().topicName(topic).build();
            if (StringUtils.isNotBlank(groups)){
                ArrayList<String> groups1 = new ArrayList<>();
                for (String group : groups.split(",")) {
                    groups1.add(group);
                }
                kpt.setGroupIds(groups1);
            }
            ArrayList<KafkaProperty> kafkaProperties = new ArrayList<>();
            kafkaProperties.add(kpt);

            for (int i=0;i<quantity;i++) {
                shopKafkaService.makeShopConsumer(kafkaProperties);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }


    @ApiOperation(value = "sequenTest", notes = "sequenTest")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "topic", paramType = "query", required = true, dataType = "String", example = "shopTopic2"),
            @ApiImplicitParam(name = "key", paramType = "query", required = true, dataType = "String", example = "shop"),
            @ApiImplicitParam(name = "message", paramType = "query", required = true, dataType = "String", example = "sequenTest send message"),
            @ApiImplicitParam(name = "quantity", paramType = "query", required = true, dataType = "Integer", example = "100")
    })
    @RequestMapping(value = "/sequenTest", method = RequestMethod.PUT)
    public void sequenTest(String topic , String key, String message,Integer quantity) {
        try{

            List<KafkaProperty> topicList = new ArrayList<>();
            if (StringUtils.isNotBlank(topic)){
                topicList.add(KafkaProperty.builder().topicName(topic).partition(10).replica((short) 1).build());
                for (int i=0;i<quantity;i++) {
                    kafkaProducer.asyncProducerWithOutCheckTopic(topicList,key,"No."+ i + " : "+ message);
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @ApiOperation(value = "sequenTestWithoutKey", notes = "sequenTestWithoutKey")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "topic", paramType = "query", required = true, dataType = "String", example = "shopTopic2"),
            @ApiImplicitParam(name = "message", paramType = "query", required = true, dataType = "String", example = "sequenTestWithoutKey send message"),
            @ApiImplicitParam(name = "quantity", paramType = "query", required = true, dataType = "Integer", example = "100")
    })
    @RequestMapping(value = "/sequenTestWithoutKey", method = RequestMethod.PUT)
    public void sequenTestWithoutKey(String topic ,String message,Integer quantity) {
        try{

            List<KafkaProperty> topicList = new ArrayList<>();
            if (StringUtils.isNotBlank(topic)){
                topicList.add(KafkaProperty.builder().topicName(topic).partition(10).replica((short) 1).build());
                for (int i=0;i<quantity;i++) {
                    kafkaProducer.asyncProducerWithOutCheckTopic(topicList, null,"No."+ i + " : "+ message);
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }

    }



}
