package kr.co.ypbooks.nextshop.domain.account.service;

import kr.co.ypbooks.nextshop.domain.account.dto.AdminActionHistVo;
import kr.co.ypbooks.nextshop.domain.account.model.adminlog.AdminActionHist;
import kr.co.ypbooks.nextshop.domain.account.repository.AdminActionHistRepository;
import kr.co.ypbooks.nextshop.domain.account.utils.DateUtils;
import kr.co.ypbooks.nextshop.lib.common.jpa.common.pojo.Result;
import kr.co.ypbooks.nextshop.lib.common.jpa.common.service.CommonService;
import kr.co.ypbooks.nextshop.lib.common.util.JsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Date;

@Slf4j
@Service
@Transactional
public class AdminActionHistService extends CommonService<AdminActionHistVo, AdminActionHist, Integer> {

    @Autowired
    AdminActionHistRepository adminActionHistRepository;

    public AdminActionHistVo insertAdminActionHist(AdminActionHistVo vo) throws Exception {

        //로그 타입 (1: 관리자 메뉴 조회, 2: 관리자 로그인, 3: 관리자 페이지 접근, 4: 개인정보 검색, 5: IP 관리, 6: 사용자 권한 설정, 7:  전시 설정)
        String logType = vo.getLogType();
        String s = "";
        LocalDateTime localDateTime = DateUtils.asLocalDateTime(new Date());

        if ("1".equals(logType)) {
            vo.getViewMenuLogInfo().setViewDate(localDateTime);
             s = JsonUtil.beanToJson(vo.getViewMenuLogInfo());
            vo.setLogContent(s);
        } else if ("2".equals(logType)) {
            s = JsonUtil.beanToJson(vo.getLoginLogInfo());
            vo.setLogContent(s);
        } else if ("3".equals(logType)) {
            vo.getAccessLogInfo().setAccessDate(DateUtils.asString("yyyy-MM-dd HH:mm:ss",localDateTime));
            s = JsonUtil.beanToJson(vo.getAccessLogInfo());
            vo.setLogContent(s);
        } else if ("4".equals(logType)) {
            s = JsonUtil.beanToJson(vo.getSearchLogInfo());
            vo.setLogContent(s);
        } else if ("5".equals(logType)) {
            s = JsonUtil.beanToJson(vo.getIpMgmtLogInfo());
            vo.setLogContent(s);
        } else if ("6".equals(logType)) {
            s = JsonUtil.beanToJson(vo.getAuthLogInfo());
            vo.setLogContent(s);
        } else if ("7".equals(logType)) {
            s = JsonUtil.beanToJson(vo.getDisplaySettingLog());
            vo.setLogContent(s);
        }

        Result<AdminActionHistVo> result = this.save(vo);
        return result.getData();
    }
}
