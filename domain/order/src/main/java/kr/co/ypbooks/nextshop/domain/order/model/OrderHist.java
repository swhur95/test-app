package kr.co.ypbooks.nextshop.domain.order.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
@Entity
public class OrderHist {
    /**
     * 주문 고유 번호
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long orderNo;

    /**
     * 주문 상태 코드
     */
    private String orderStateCd;

    /**
     * 제휴 회사 코드
     */
    private String alliCompanyCd;

    /**
     * 회원 유형 코드 (1: 북쿨럽, 2: 계열사)
     */
    private String memberTypeCd;

    /**
     * 회원 고유 코드
     */
    private String memberCd;

    /**
     * 회원 아이디
     */
    private String memberId;

    /**
     * 배송 유형 코드(H:일반택배,Y:지점배송)
     */
    private String delivTypeCd;

    /**
     * 물류 연동 코드 (Y:연동 필요,N:연동 완료,NULL:연동 불필요)
     */
    private Boolean logistIntlkCd;

    /**
     * 주문자명
     */
    private String ordererId;

    /**
     * 주문자 전화번호
     */
    private String ordererPhone;

    /**
     * 주문자 핸드폰
     */
    private String ordererMobile;

    /**
     * 주문자 이메일
     */
    private String ordererEmail;

    /**
     * 수취인 이름
     */
    private String recpName;

    /**
     * 수취인 전화번호
     */
    private String recpPhone;

    /**
     * 수취인 핸드폰 번호
     */
    private String recpMobile;

    /**
     * 주문 정보
     */
    private String orderInfo;

    /**
     * 결제 정보
     */
    private String payInfo;

    /**
     * 배송 정보
     */
    private String delivInfo;

    /**
     * 물류 정보
     */
    private String logistInfo;

    /**
     * 지점 배송 정보
     */
    private String branchDelivInfo;

    /**
     * 등록 일시
     */
    private LocalDateTime regDate;

    /**
     * 수정 일시
     */
    private LocalDateTime updDate;
}

