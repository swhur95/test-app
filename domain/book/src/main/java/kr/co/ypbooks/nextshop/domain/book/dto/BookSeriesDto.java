package kr.co.ypbooks.nextshop.domain.book.dto;

import lombok.Builder;
import lombok.Getter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Builder
public class BookSeriesDto {
    /**
     * 시리즈 고유 번호
     */
    private Long seriesNo;

    /**
     * 시리즈명
     */
    private String seriesName;

    /**
     * 상품 정보
     */
    private List<String> bookCdList;

    /**
     * 시리즈 설명
     */
    private String desc;

    /**
     * 등록 일시
     */
    private LocalDateTime regDate;
}
