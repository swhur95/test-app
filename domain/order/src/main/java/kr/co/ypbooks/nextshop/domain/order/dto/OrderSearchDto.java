package kr.co.ypbooks.nextshop.domain.order.dto;

import lombok.Builder;
import lombok.Getter;

import java.time.LocalDateTime;

@Getter
@Builder
public class OrderSearchDto {
    /**
     * 주문 일시 조건 (시작 일시)
     */
    private LocalDateTime startOrderDate;

    /**
     * 주문 일시 조건 (종료 일시)
     */
    private LocalDateTime endOrderDate;

    /**
     * 주문 번호 조건
     */
    private Long orderNo;

    /**
     * 주문 상태 조건
     */
    private String orderStateCd;

    /**
     * 주문자 Id 조건
     */
    private String ordererId;

    /**
     * 주문자 이름 조건
     */
    private String ordererName;

    /**
     * 주문자 연락처
     */
    private String ordererPhone;

    /**
     * 주문자 핸드폰
     */
    private String ordererMobile;

    /**
     * 주문자 이메일
     */
    private String ordererEmail;

    /**
     * 수령인 이름
     */
    private String recpName;

    /**
     * 수령인 연락처
     */
    private String recpPhone;

    /**
     * 수령인 모바일
     */
    private String recpMobile;

    /**
     * 입금자명
     */
    private String accountName;
}
