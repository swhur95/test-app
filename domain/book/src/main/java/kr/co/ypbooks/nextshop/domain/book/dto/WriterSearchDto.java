package kr.co.ypbooks.nextshop.domain.book.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class WriterSearchDto {
    /**
     * 국내 여부 조건
     */
    @ApiModelProperty(value = "국내 여부 조건", example = "1")
    @JsonProperty("domestic_flag")
    private Boolean domesticFlag;

    /**
     * 작가 분야 코드 조건
     */
    @ApiModelProperty(value = "작가 분야 코드 조건", example = "\"010201\"")
    @JsonProperty("writer_category_cd")
    private String writerCategoryCd;

    /**
     * 작가 분야 코드 조건
     */
    @ApiModelProperty(value = "작가 태그 단어 조건", example = "신인작가")
    @JsonProperty("writer_tag_word")
    private String writerTagWord;

    /**
     * 작가 고유 번호 조건
     */
    @ApiModelProperty(value = "작가 고유 번호 조건", example = "12345678")
    @JsonProperty("writer_no")
    private Long writerNo;

    /**
     * 출생지 조건
     */
    @ApiModelProperty(value = "출생지 조건", example = "한국")
    @JsonProperty("birth_place")
    private String birthPlace;
}
