package kr.co.ypbooks.nextshop.domain.account.oauth.request;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import kr.co.ypbooks.nextshop.domain.account.model.Member;
import kr.co.ypbooks.nextshop.domain.account.oauth.cache.AuthStateCache;
import kr.co.ypbooks.nextshop.domain.account.oauth.conf.AuthConfig;
import kr.co.ypbooks.nextshop.domain.account.oauth.conf.AuthDefaultSource;
import kr.co.ypbooks.nextshop.domain.account.oauth.conf.AuthSource;
import kr.co.ypbooks.nextshop.domain.account.oauth.model.AuthCallback;
import kr.co.ypbooks.nextshop.domain.account.oauth.model.AuthToken;
import kr.co.ypbooks.nextshop.domain.account.oauth.utils.UrlBuilder;
import kr.co.ypbooks.nextshop.domain.account.utils.HttpClientUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.DateUtils;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


@Slf4j
public class AuthAppleRequest extends AuthDefaultRequest {

	private static boolean CHECK_SSO = true;
	private static boolean UNCHECK_SSO = false;

	public AuthAppleRequest(AuthConfig config) {
		super(config, AuthDefaultSource.APPLE);
	}

	public AuthAppleRequest(AuthConfig config, AuthSource source) {
		super(config, source, UNCHECK_SSO);
	}

	public AuthAppleRequest(AuthConfig config, AuthStateCache authStateCache) {
		super(config, AuthDefaultSource.APPLE, authStateCache, CHECK_SSO);
	}

	public Map<String, Object> getFirstRequestToken(AuthCallback authCallback) throws Exception {

		String url = UrlBuilder.fromBaseUrl(source.accessToken()).build();
		Map<String, String> param = new HashMap<>();
		param.put("grant_type", "authorization_code");
		param.put("code", authCallback.getCode());
		param.put("redirect_uri", config.getRedirectUri());
		param.put("client_id", config.getClientId());
		//jwt secret
		String client_secret = this.getClientSecret(authCallback);
		param.put("client_secret", client_secret);

		log.warn("url:*******" + url + "*******");
		String response = null;

		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> dataMap = null;
		response = HttpClientUtil.doPost(url , param);
		try {
			dataMap = mapper.readValue(response, Map.class);
		} catch (Exception e) {
			e.printStackTrace();
		}
		log.warn("response:*******" + response + "*******");
		log.warn("dataMap: " + dataMap);


		//UserInfo

		return dataMap;
	}

	@Override
	public String authorize(String state) {
		return UrlBuilder.fromBaseUrl(source.authorize())
				.queryParam("response_type", "code")
				.queryParam("response_mode", "form_post")
				.queryParam("client_id", config.getClientId())
				.queryParam("redirect_uri", config.getRedirectUri())
				.queryParam("state", getRealState(state))
				.queryParam("scope", "name email")
				.build();
	}

	public String getClientSecret(AuthCallback authCallback) throws Exception{

		JwtBuilder jwtBuilder = Jwts.builder();
		jwtBuilder.setHeaderParam("kid", authCallback.getKeyId());
		jwtBuilder.setIssuer(authCallback.getTeamId());
		jwtBuilder.setIssuedAt(new Date());
		jwtBuilder.setExpiration(DateUtils.addMinutes(new Date(),30));
		jwtBuilder.setAudience("https://appleid.apple.com");
		jwtBuilder.setSubject(authCallback.getClientId());


		byte[] secreKeyBytes = DatatypeConverter.parseBase64Binary(authCallback.getSecretCode());
		jwtBuilder.signWith(new SecretKeySpec(secreKeyBytes, SignatureAlgorithm.ES256.getJcaName()), SignatureAlgorithm.ES256);
		String secretCode = jwtBuilder.compact();

		return secretCode;
	}

	protected String getUserInfoUrl() {
		return UrlBuilder.fromBaseUrl(source.profileUrl()).build();
	}

	@Override
	protected AuthToken getAccessToken(AuthCallback authCallback) {
		return null;
	}

	@Override
	protected Member getUserInfo(AuthToken authToken) {
		return null;
	}

}
