package kr.co.ypbooks.nextshop.app.shop.controller;

import kr.co.ypbooks.nextshop.lib.common.message.EmailOperation;
import kr.co.ypbooks.nextshop.lib.common.operation.QuartzOperation;
import kr.co.ypbooks.nextshop.lib.common.property.EmailProperty;
import kr.co.ypbooks.nextshop.lib.common.property.ReceiveMailAddress;
import kr.co.ypbooks.nextshop.lib.common.property.TaskDefineProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/schedule")
public class ScheduledTaskTestController {

   @Autowired
   private EmailOperation emailOperation;
   @Autowired
   private QuartzOperation quartzOperation;

    @GetMapping(value = "/create/emailTask")
    public Boolean createEmailTask(String jobName, String jobGroup, String cron) {
        boolean sendEmailsRegularly = emailOperation.timingSendEmail(getEmailProperty(), cron, jobName, jobGroup,"Send emails regularly");
        return sendEmailsRegularly;
    }

    @GetMapping(value = "/pause/emailTask")
    public Boolean pauseEmailTask(String jobName, String jobGroup) {
        return quartzOperation.pauseScheduleJob(jobName, jobGroup);
    }

    @GetMapping(value = "/resume/emailTask")
    public Boolean resumeEmailTask(String jobName, String jobGroup) {
        return quartzOperation.resumeScheduleJob(jobName, jobGroup);
    }

    @GetMapping(value = "/modify/emailTask")
    public Boolean modifyEmailTask(String jobName, String jobGroup, String cron) {
        TaskDefineProperty taskDefineProperty = new TaskDefineProperty(jobName, jobGroup, cron, null);
        return quartzOperation.modifyScheduleJob(taskDefineProperty);
    }

    @GetMapping(value = "/delete/emailTask")
    public Boolean deleteEmailTask(String jobName, String jobGroup) {
        return quartzOperation.delScheduleJob(jobName, jobGroup);
    }

    private EmailProperty getEmailProperty() {
        HashMap<String, Object> templateParameter = new HashMap<>();
        templateParameter.put("object1", "value");
        List<ReceiveMailAddress> receiveMailAddressList = new ArrayList<>();
        for (int i = 0; i < 7; i++) {
            ReceiveMailAddress receiveMailAddress = ReceiveMailAddress.builder().receiveMailAddr("receive" + i).receiveName("receiveName" + i).receiveType("type1").build();
            receiveMailAddressList.add(receiveMailAddress);
        }
        EmailProperty emailProperty = EmailProperty.builder()
                .templateId("123")
                .senderAddress("address")
                .senderName("test")
                .templateParameter(templateParameter)
                .receiverList(receiveMailAddressList)
                .build();
        return emailProperty;
    }

}
