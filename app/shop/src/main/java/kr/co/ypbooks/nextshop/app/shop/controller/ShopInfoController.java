package kr.co.ypbooks.nextshop.app.shop.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import kr.co.ypbooks.nextshop.domain.shop.model.ShopInfo;
import kr.co.ypbooks.nextshop.domain.shop.model.ShopInfoUser;
import kr.co.ypbooks.nextshop.domain.shop.service.ShopInfoService;
import kr.co.ypbooks.nextshop.app.shop.exception.ResultBean;
import kr.co.ypbooks.nextshop.app.shop.exception.ResultUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@Api(tags = "ShopInfo")
@RequestMapping("/shopInfo")
@RestController
public class ShopInfoController {
    private final ShopInfoService service;

    public ShopInfoController(ShopInfoService shopInfoService) {
        this.service = shopInfoService;
    }


    @ApiOperation(value = "find", notes = "findShopInfo")
    @ApiImplicitParam(name = "id", paramType = "query", required = true, dataType = "Long", example = "1")
    @GetMapping(value = "/find")
    public ShopInfo findShopInfo(Long id) {

        log.debug("=====> test logging level debug<====");
        log.info("=====> test logging level info <=====");
        log.error("=====> test logging level error <====");
        log.warn("=====> test logging level warn <=====");


        log.info("============ parameter id ============>" + id);
        ShopInfo shopInfo = service.selectById(id);
        log.info("============ return data ============> {}", shopInfo);
        return shopInfo;
    }

    @ApiOperation(value = "findListByKey", notes = "findShopListByKey")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "key", paramType = "query", required = true, dataType = "String"),
            @ApiImplicitParam(name = "value", paramType = "query", required = true, dataType = "String")
    })
    @GetMapping(value = "/findListByKey")
    public List<ShopInfo> findListByKey(String key, String value) {
        List<ShopInfo> list = service.selectByKey(key, value);
        return list;
    }

    @ApiOperation(value = "add", notes = "addShopInfo")
    @PostMapping("/add")
    public ResultBean addShopInfo(String param) {
        ResultBean resultBean = null;
        try {

            if (param == null) {

                resultBean = ResultUtil.error("param is null");

            } else {

                System.out.println(param);
                ShopInfoUser u = ShopInfoUser.builder()
                        .age(77)
                        .adress("77" + param)
                        .chat("id:77" + param)
                        .name("name77" + param)
                        .sex(1)
                        .salary(777L)
                        .build();
                ShopInfo sInfo = service.addShopInfo(ShopInfo.builder().content(u).build());
                resultBean = ResultUtil.success(sInfo);

            }

        } catch (Exception e) {
            resultBean = ResultUtil.exception(e.getMessage());
        }
        return resultBean;
    }

}
