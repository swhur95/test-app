package kr.co.ypbooks.nextshop.domain.order.service;

import kr.co.ypbooks.nextshop.domain.order.repository.OrderHistRepository;
import org.springframework.stereotype.Service;

@Service
public class OrderService {
    private final OrderHistRepository orderHistRepository;

    public OrderService(OrderHistRepository orderHistRepository) {
        this.orderHistRepository = orderHistRepository;
    }
}
