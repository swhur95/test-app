package kr.co.ypbooks.nextshop.domain.product.repository;

import kr.co.ypbooks.nextshop.domain.product.model.ProductId;
import kr.co.ypbooks.nextshop.domain.product.model.ProductManualDiscRate;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductManualDiscRateRepository extends JpaRepository<ProductManualDiscRate, ProductId> {
}
