package kr.co.ypbooks.nextshop.domain.order.dto;

import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.Getter;

import java.time.LocalDateTime;

@ApiModel
@Getter
@Builder
public class OrderOverviewDto {
    /**
     * 주문 번호
     */
    private Long orderNo;

    /**
     * 주문 상태 코드
     */
    private String orderStateCd;

    /**
     * 주문 일시
     */
    private LocalDateTime orderDate;

    /**
     * 발송 일시
     */
    private LocalDateTime delivDate;

    /**
     * 주문인
     */
    private String ordererName;

    /**
     * 수취인
     */
    private String recpName;

    /**
     * 주문 수량
     */
    private Integer quantity;

    /**
     * 결제일
     */
    private LocalDateTime payDate;

    /**
     * 주문 금액
     */
    private Long orderAmt;

    /**
     * 결제 수단 코드
     */
    private String payMethodTypeCd;

    /**
     * 할인 금액
     */
    private Long sumDiscAmt;

    /**
     * 배송 금액
     */
    private Long delivAmt;

    /**
     * 결제 금액
     */
    private Long payAmt;

    /**
     * 취소 금액
     */
    private Long cancelAmt;

    /**
     * 적립금 사용 금액
     */
    private Long reserveUseAmt;

    /**
     * 예치금 사용 금액
     */
    private Long depositUseAmt;

    /**
     * 포인트 사용 금액
     */
    private Long pointUseAmt;

    /**
     * 기프티콘 사용 금액
     */
    private Long giftconUseAmt;

    /**
     * 상담 건수
     */

    /**
     * 나우드림 여부
     */
    private Boolean branchDelivFlag;

    /**
     * 나우드림 수령 확인
     */
    private Boolean branchReceiptFlag;

    /**
     * 문화비 소득 공제 신청 여부
     */
    private Boolean cultureDeducFlag;

    /**
     * 영수증 여부
     */
    private Boolean receiptFlag;

    /**
     * 예상 출고일 (need hour로 일시 계산 필요)
     */
    private LocalDateTime expDelivDate;

    /**
     * 예상 수령일
     */
    private LocalDateTime expReceiptDate;
}
