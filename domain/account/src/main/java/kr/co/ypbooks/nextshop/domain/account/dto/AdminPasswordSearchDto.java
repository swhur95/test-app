package kr.co.ypbooks.nextshop.domain.account.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;

@ApiModel
@Getter
@Builder
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class AdminPasswordSearchDto {
    /**
     * 지점 코드
     */
    @ApiModelProperty(value = "지점 코드", example = "12345678", required = true)
    private String branchCode;

    /**
     * 관리자 ID
     */
    @ApiModelProperty(value = "관리자ID", example = "admin", required = true)
    private String adminId;

    /**
     * 관리자명
     */
    @ApiModelProperty(value = "관리자명", example = "관리자", required = true)
    private String adminName;

    /**
     * 요청자 이름
     */
    @ApiModelProperty(value = "요청자명", example = "매니저", required = true)
    private String reqName;

    /**
     * 요청자 휴대폰 번호
     */
    @ApiModelProperty(value = "요청자 휴대폰", example = "01012345678", required = true)
    private String reqMobile;
}
