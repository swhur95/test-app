insert into display_layout
(display_seq, display_category_cd, default_title, display_product_cnt, layout_sub_info, reg_date, upd_date)
values
(1, '01', '인기 검색어', '3', null, now(), null),
(2, '02', '인기 태그', '3', null, now(), null),
(3, '03', '분야별 베스트', '3', null, now(), null),
(4, '04', '분야별 화제의 신간', '3', null, now(), null),
(5, '05', '오늘의 책', '3', null, now(), null),
(6, '06', 'md 추천 도서', '3', null, now(), null),
(7, '07', '테마 도서', '3', '{ "layout_sub_detail_infos": [{"display_sub_seq": 1, "display_category_cd": "08", "display_split_cnt": 1, "display_sub_product_cnt": 3}, {"display_sub_seq": 2, "display_category_cd": "09", "display_split_cnt": 2, "display_sub_product_cnt": 3}]}', now(), null));

