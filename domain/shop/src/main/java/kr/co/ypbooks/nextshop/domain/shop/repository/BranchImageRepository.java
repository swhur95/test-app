package kr.co.ypbooks.nextshop.domain.shop.repository;

import kr.co.ypbooks.nextshop.domain.shop.model.BranchImage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface BranchImageRepository extends JpaRepository<BranchImage, Long>, JpaSpecificationExecutor<BranchImage> {
}
