package kr.co.ypbooks.nextshop.domain.book.repository;

import kr.co.ypbooks.nextshop.domain.book.model.BookTranslator;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookTranslatorRepository extends JpaRepository<BookTranslator, String> {
}
