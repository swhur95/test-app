package kr.co.ypbooks.nextshop.lib.common.message.job;

import kr.co.ypbooks.nextshop.lib.common.message.ApiResult;
import kr.co.ypbooks.nextshop.lib.common.message.EmailOperation;
import kr.co.ypbooks.nextshop.lib.common.property.EmailProperty;
import lombok.extern.slf4j.Slf4j;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.text.SimpleDateFormat;
import java.util.Date;

@Slf4j
@DisallowConcurrentExecution
public class RegularSendEmailsJob extends QuartzJobBean {

    @Autowired
    EmailOperation emailOperation;

    @Override
    protected void executeInternal(JobExecutionContext context) {
        JobDataMap jobDataMap = context.getTrigger().getJobDataMap();
        EmailProperty emailProperty = (EmailProperty) jobDataMap.get("emailProperty");
        ApiResult apiResult = emailOperation.sendMail(emailProperty);
        // todo insert apiResult's log into db
        log.info(context.getTrigger().getKey() + "Timed task starts successfully " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + " RegularSendEmailsJob apiResult: " + apiResult);
    }
}
