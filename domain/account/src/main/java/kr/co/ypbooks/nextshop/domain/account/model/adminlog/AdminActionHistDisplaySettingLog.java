package kr.co.ypbooks.nextshop.domain.account.model.adminlog;

import io.swagger.annotations.ApiModel;
import lombok.*;

/**
 * 전시 설정 로그
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@ApiModel(value = "로그 타입 (7:  전시 설정)")
public class AdminActionHistDisplaySettingLog {
    /**
     * 관리자 메뉴명
     */
    private String adminMenuName;
    /**
     * 관리자 메뉴 URL
     */
    private String adminMenuUrl;
    /**
     * 전시 설정 액션
     */
    private String displayAction;
}
