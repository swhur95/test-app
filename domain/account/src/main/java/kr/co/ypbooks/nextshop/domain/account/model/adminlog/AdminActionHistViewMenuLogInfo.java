package kr.co.ypbooks.nextshop.domain.account.model.adminlog;

import io.swagger.annotations.ApiModel;
import lombok.*;

import java.time.LocalDateTime;

/**
 * 관리자 메뉴 조회 로그 정보
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter

@ApiModel(value = "로그 타입 (1: 관리자 메뉴 조회)")
public class AdminActionHistViewMenuLogInfo {
    /**
     * 조회 일시
     */
    private LocalDateTime viewDate;
    /**
     * 조회 액션
     */
    private String viewAction;
}
