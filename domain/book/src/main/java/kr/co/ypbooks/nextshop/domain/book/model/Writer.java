package kr.co.ypbooks.nextshop.domain.book.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
@Entity
public class Writer {
    /**
     * 작가 고유 번호
     */
    @Id
    private Long writerNo;

    /**
     * 작가 명
     */
    private String writerName;

    /**
     * 국내 여부
     */
    private Boolean domesticFlag;

    /**
     * 출생지
     */
    private String birthPlace;

    /**
     * 작가 정보
     */
    private String writerInfo;

    /**
     * 등록 일시
     */
    private LocalDateTime regDate;

    /**
     * 수정 일시
     */
    private LocalDateTime updDate;
}
