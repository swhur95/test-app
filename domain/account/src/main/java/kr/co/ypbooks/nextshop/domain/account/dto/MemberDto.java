package kr.co.ypbooks.nextshop.domain.account.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter

public class MemberDto {

    //params
    private String startRegDate;
    private String endRegDate;
    private String memberState;

    private String name;
    private String memberId;
    private String memberCd;
    private String phone;
    private String email;

    //page
    private Integer pageNum;
    private Integer pageSize;
    private Integer totalSize;

    //result data
    private Object data;
}
