package kr.co.ypbooks.nextshop.domain.shop.model;

import com.vladmihalcea.hibernate.type.json.JsonStringType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Entity

@Table(name = "DISPLAY_LAYOUT")
@DynamicUpdate
@TypeDef(name = "json", typeClass = JsonStringType.class)
public class DisplayLayout {
    /**
     * 전시 순번
     */
    @Id
    private Byte displaySeq;

    /**
     * 전시 분류 (0: 인기 검색어, 1: 인기 태그, 2: 분야별 베스트)
     */
    private String displayCategoryCd;

    /**
     * 기본 제목
     */
    private String defaultTitle;

    /**
     * 전시 상품 개수
     */
    private Byte displayProductCnt;

    /**
     * 추가 정보	JSON
     */
    @Type(type = "json")
    @Column(columnDefinition = "json")
    private String layoutSubInfo;

    /**
     * 등록 일시
     */
    private LocalDateTime regDate;

    /**
     * 수정 일시
     */
    private LocalDateTime updDate;
}
