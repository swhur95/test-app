package kr.co.ypbooks.nextshop.lib.common.property;

import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
public class KafkaProperty {

    String topicName;
    int partition;
    short replica;

    List<String> groupIds;

    @Builder.Default
    boolean sendResult = true;

}
