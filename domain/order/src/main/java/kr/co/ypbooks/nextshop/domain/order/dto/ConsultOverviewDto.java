package kr.co.ypbooks.nextshop.domain.order.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;

import java.time.LocalDateTime;

@ApiModel
@Getter
@Builder
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ConsultOverviewDto {
    /**
     * 문의 번호
     */
    @ApiModelProperty(value = "문의 번호", example = "12345")
    private Long inquiryNo;

    /**
     * 회원명
     */
    @ApiModelProperty(value = "회원명", example = "회원명")
    private String memberName;

    /**
     * 회원 ID
     */
    @ApiModelProperty(value = "회원 ID", example = "회원 ID")
    private String memberId;

    /**
     * 상담 제목
     */
    @ApiModelProperty(value = "상담 제목", example = "상담 제목")
    private String consultSubject;

    /**
     * 상담 상태 코드
     */
    @ApiModelProperty(value = "상담 상태 코드", example = "상담 상태 코드")
    private String consultStateCd;

    /**
     * 주문 번호
     */
    @ApiModelProperty(value = "주문 번호", example = "123456")
    private Long orderNo;

    /**
     * 등록 일시
     */
    @ApiModelProperty(value = "등록 일시", example = "2022-03-22 09:00:00")
    private LocalDateTime regDate;
}
