package kr.co.ypbooks.nextshop.lib.common.property;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JwtProperty {
    private String secretKey;
}
