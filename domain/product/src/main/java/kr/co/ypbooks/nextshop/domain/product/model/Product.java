package kr.co.ypbooks.nextshop.domain.product.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import java.time.LocalDate;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Product {
    /**
     * 상품 고유 Id (상품 타입, 상품 코드)
     */
    @EmbeddedId
    private ProductId productId;

    /**
     * 상품명
     */
    private String productName;

    /**
     * 상품 식별 코드
     */
    private String productIdCd;

    /**
     * 카테고리 코드1 (대분류)
     */
    private String categoryCd1;

    /**
     * 카테고리 코드2 (중분류)
     */
    private String categoryCd2;

    /**
     * 카테고리 코드3 (소분류)
     */
    private String categoryCd3;

    /**
     * 상품 제조일
     */
    private LocalDate mfgDate;

    /**
     * 상품 제조사 코드
     */
    private String mfgCompanyCd;

    /**
     * 상품 가격
     */
    private Long productPrice;

    /**
     * 통화 유형 코드
     */
    private String ccyTypeCd;

    /**
     * 상품 매입율
     */
    private Byte productPurRate;

    /**
     * 유통사 매입율
     */
    private Byte logistPurRate;

    /**
     * 상품 상태 코드
     */
    private String productStateCd;

    /**
     * 상품 수
     */
    private Integer quantity;

    /**
     * 상품 정보
     */
    private String productInfo;

    /**
     * 상품 상태 정보
     */
    private String productStateInfo;

    /**
     * 등록 일시
     */
    private LocalDateTime regDate;

    /**
     * 수정 일시
     */
    private LocalDateTime updDate;
}
