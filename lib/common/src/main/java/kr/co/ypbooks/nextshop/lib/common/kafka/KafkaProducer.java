package kr.co.ypbooks.nextshop.lib.common.kafka;

import kr.co.ypbooks.nextshop.lib.common.property.KafkaProperty;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.kafka.clients.admin.*;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.KafkaFuture;
import org.apache.kafka.common.errors.TimeoutException;
import org.apache.kafka.common.errors.TopicExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
public class KafkaProducer {

    @Autowired
    KafkaTemplate kafkaTemplate;

    @Autowired
    private AdminClient adminClient;

    /**
     * create new topic
     * @param topicList(topicName,partition,replica)
     */
    protected boolean createTopic(List<KafkaProperty> topicList) throws Exception {

        List<NewTopic> newTopics = new ArrayList<>();
        for(KafkaProperty property: topicList){
            newTopics.add(new NewTopic(property.getTopicName(), property.getPartition(), property.getReplica()));
        }

        CreateTopicsResult topics = adminClient.createTopics(newTopics);
        Thread.sleep(1000);
        //Use the following to determine the results of creating a topic
        for (Map.Entry<String, KafkaFuture<Void>> entry : topics.values().entrySet()) {
            try {
                entry.getValue().get();
                log.info("topic "+entry.getKey()+" created");

            } catch (InterruptedException | ExecutionException e) {
                if (ExceptionUtils.getRootCause(e) instanceof TopicExistsException) {
                    log.info("topic "+entry.getKey()+" existed");
                }
            }
        }
        return true;
    }

    /**
     * return topic info
     * @param topicName
     * @return
     */
    public KafkaFuture<Map<String, TopicDescription>> SelectTopicInfo(String topicName) {
        DescribeTopicsResult result = adminClient.describeTopics(Arrays.asList(topicName));
        KafkaFuture<Map<String, TopicDescription>> all = result.all();
        return all;
    }


    /**
     * add NewPartitions （add only）
     * @param topicName
     * @param number
     */
    public void edit(String topicName,Integer number){
        Map<String, NewPartitions> newPartitions=new HashMap<String, NewPartitions>();
        newPartitions.put(topicName,NewPartitions.increaseTo(number));
        adminClient.createPartitions(newPartitions);
    }

    /**
    * asyncProducer
    * @param topicList , key , message
    */
    public void asyncProducer(List<KafkaProperty> topicList, String key, String message) throws Exception {
        if(this.createTopic(topicList)){
            for(KafkaProperty property: topicList) {
                ListenableFuture producer;
                if(StringUtils.isNotBlank(key)){
                    producer = kafkaTemplate.send(property.getTopicName(), key, message);
                }else {
                    producer = kafkaTemplate.send(property.getTopicName(), message);
                }
                producer.addCallback(new ListenableFutureCallback<SendResult<Integer, String>>() {
                    public void onSuccess(SendResult<Integer, String> result) {
                        log.info("msg OK." + result.toString());
                    }
                    public void onFailure(Throwable ex) {
                        log.info("msg send failed: " + ex.getMessage());
                    }
                });
            }
        }
    }

    /**
     * syncProducer
     * @param topicList , key , message
     */
    public List<KafkaProperty> syncProducer(List<KafkaProperty> topicList, String key, String message) throws Exception {

        if(this.createTopic(topicList)) {
            for (KafkaProperty property : topicList) {
                ProducerRecord<String, String> record = null;
                if(StringUtils.isNotBlank(key)){
                    record = new ProducerRecord<>(property.getTopicName(), key, message);
                }else {
                    record = new ProducerRecord<>(property.getTopicName(), message);
                }

                try{
                    ListenableFuture<SendResult<String, String>> future = kafkaTemplate.send(property.getTopicName(), message);
                    future.get(Long.parseLong("10"), TimeUnit.SECONDS);
                }catch (InterruptedException | ExecutionException | TimeoutException e){
                    log.error("waiting for kafka send finish failed!", e);
                    property.setSendResult(false);
                }
            }
        }
        return topicList;
    }

    /**
     * asyncProducerWithOutCheckTopic
     * @param topicList , key , message
     */
    public void asyncProducerWithOutCheckTopic(List<KafkaProperty> topicList, String key, String message) throws Exception {
        for(KafkaProperty property: topicList) {
            ListenableFuture producer;
            if(StringUtils.isNotBlank(key)){
                producer = kafkaTemplate.send(property.getTopicName(), key, message);
            }else {
                producer = kafkaTemplate.send(property.getTopicName(), message);
            }
            producer.addCallback(new ListenableFutureCallback<SendResult<Integer, String>>() {
                public void onSuccess(SendResult<Integer, String> result) {
                    log.info("msg OK." + result.toString());
                }
                public void onFailure(Throwable ex) {
                    log.info("msg send failed: " + ex.getMessage());
                }
            });
        }
    }


/*




    public void kafakaSend(Map map) throws Exception{

        log.info(" unicast  test ==============>");

        ListenableFuture<SendResult<Integer, String>> send = kafkaTemplate.send(ORDER_TOPIC, JsonUtil.objectToJson(map));

        send.addCallback(new ListenableFutureCallback<SendResult<Integer, String>>() {
            public void onSuccess(SendResult<Integer, String> result) {
                log.info("msg OK." + result.toString());
            }

            public void onFailure(Throwable ex) {
                log.info("msg send failed: " + ex.getMessage());
            }
        });

    }


    public void kafakaMultiSendByIP(Map map) throws Exception{
       String IP = InetAddress.getLocalHost().getHostAddress();

        log.info(" multicast  test kafakaMultiSendByIP==============>");
        log.info("IP : " + IP);

        map.put("IP" , IP);

        ListenableFuture<SendResult<Integer, String>> send = kafkaTemplate.send(ORDER_TOPIC, JsonUtil.objectToJson(map));

        send.addCallback(new ListenableFutureCallback<SendResult<Integer, String>>() {
            public void onSuccess(SendResult<Integer, String> result) {
                log.info("msg OK." + result.toString());
            }

            public void onFailure(Throwable ex) {
                log.info("msg send failed: " + ex.getMessage());
            }
        });

    }

    public void kafakaMultiSendNoIP(Map map) throws Exception{

        log.info(" multicast  test kafakaMultiSendNoIP==============>");

        map.put("IP" , "xxx.xxx.xxx.xxx");
        ListenableFuture<SendResult<Integer, String>> send = kafkaTemplate.send(ORDER_TOPIC, JsonUtil.objectToJson(map));

        send.addCallback(new ListenableFutureCallback<SendResult<Integer, String>>() {
            public void onSuccess(SendResult<Integer, String> result) {
                log.info("msg OK." + result.toString());
            }

            public void onFailure(Throwable ex) {
                log.info("msg send failed: " + ex.getMessage());
            }
        });

    }


    // topic A to B
    public void kafakaASendToB(Map map) throws Exception{
        ListenableFuture<SendResult<Integer, String>> send = kafkaTemplate.send(SHOP_TOPIC, JsonUtil.objectToJson(map));

        send.addCallback(new ListenableFutureCallback<SendResult<Integer, String>>() {
            public void onSuccess(SendResult<Integer, String> result) {
                log.info("kafakaASendToB msg OK." + result.toString());
            }

            public void onFailure(Throwable ex) {
                log.info("kafakaASendToB msg send failed: " + ex.getMessage());
            }
        });
    }
*/

}
