package kr.co.ypbooks.nextshop.domain.product.repository;

import kr.co.ypbooks.nextshop.domain.product.model.ProductSeries;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductSeriesRepository extends JpaRepository<ProductSeries, Long> {
}
