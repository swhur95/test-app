package kr.co.ypbooks.nextshop.lib.common.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDateTime;
import java.util.Map;

public class JsonUtil {


    public static Object jsonToObject(String str, Object obj) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        Object result = mapper.readValue(str, obj.getClass());
        return result;
    }

    public static String objectToJson(Object obj) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        String result = mapper.writeValueAsString(obj);
        return result;
    }

    public static Map<String, String> jsonToMap(String json) throws Exception{
        if (StringUtils.isEmpty(json)) {
            return null;
        }
        ObjectMapper mapper = new ObjectMapper();
        return  mapper.readValue(json, Map.class);
    }

    public static String beanToJson(Object obj){
        Gson gson = new GsonBuilder()
                .setPrettyPrinting()
                .registerTypeAdapter(LocalDateTime.class,new LocalDateAdapter()).create();
        String s = gson.toJson(obj);
        return s;
    }

    public static <T> T jsonToObjectNoException(String content, Class<T> valueType) {
        try {
            if(content != null) {
                ObjectMapper mapper = new ObjectMapper();
                return mapper.readValue(content, valueType);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static String objectToJsonNoException(Object obj) {
        try {
            if(obj != null) {
                ObjectMapper mapper = new ObjectMapper();
                return mapper.writeValueAsString(obj);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }


}
