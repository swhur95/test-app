package kr.co.ypbooks.nextshop.app.shop.controller;

import kr.co.ypbooks.nextshop.lib.common.annotation.RedisLock;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/redisLock")
@Slf4j
@Scope("prototype")
public class RedisLockTestController {

    private static Integer totalNum = 10;

    @GetMapping("/lock")
    @RedisLock(key = "testLock")
    public void get() {
        if (totalNum > 0) {
            totalNum -= 1;
        }
        log.info("current totalNum is: " + totalNum);
        log.info("object" + this);
    }

}
