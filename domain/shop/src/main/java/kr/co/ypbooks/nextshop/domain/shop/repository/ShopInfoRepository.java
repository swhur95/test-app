package kr.co.ypbooks.nextshop.domain.shop.repository;

import kr.co.ypbooks.nextshop.domain.shop.model.ShopInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ShopInfoRepository extends JpaRepository<ShopInfo, Long> {

    @Query(value = "SELECT * FROM shopinfo WHERE JSON_EXTRACT(content, CONCAT('$.',?1,'')) = ?2", nativeQuery = true)
    List<ShopInfo> selectByKey(String key, String value);
}

