package kr.co.ypbooks.nextshop.domain.book.dto;

import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.Getter;

import java.time.LocalDate;

@ApiModel
@Getter
@Builder
public class BookOverviewDto {
    /**
     * 도서 코드
     */
    private String bookCd;

    /**
     * 도서명
     */
    private String bookName;

    /**
     * 출판사
     */
    private String pubCompanyName;

    /**
     * ISBN
     */
    private String isbn;

    /**
     * 도서 가격
     */
    private Long bookPrice;

    /**
     * 매입율
     */
    private Byte bookPurRate;

    /**
     * 출판일
     */
    private LocalDate bookPubDate;

    /**
     * 수량
     */
    private Integer quantity;

    /**
     * 물류 수량
     */
    private Integer logistQuantity;

    /**
     * 할인율
     */
    private Byte discRate;

    /**
     * 적립율
     */
    private Byte reserveRate;

    /**
     * 전월 판매 수량
     */
    private Integer saleQuantityLastMonth;
}
