package kr.co.ypbooks.nextshop.domain.shop.service;

import kr.co.ypbooks.nextshop.domain.shop.dto.ShopDto;
import kr.co.ypbooks.nextshop.domain.shop.model.Shop;
import kr.co.ypbooks.nextshop.domain.shop.repository.ShopRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ShopService {

    @Autowired
    private ShopRepository shopRepository;

    public Optional<ShopDto> getShop(Long id) {
        return convert(shopRepository.findById(id));
    }

    /**
     * selectById
     *
     * @param id
     */
    public Shop selectById(Long id) {
        Optional<Shop> optional = shopRepository.findById(id);
        Shop shop = optional.get();
        return shop;
    }

    @Transactional
    public ShopDto addShop(ShopDto shopDto) {
        Shop shop = shopRepository.save(Shop.builder().name(shopDto.getName()).address(shopDto.getAddress()).build());

        return ShopDto.builder().id(shop.getId()).name(shop.getName()).address(shop.getAddress()).build();
    }

    @Transactional
    public void delShop(Long id) {
        shopRepository.deleteById(id);
    }

    /**
     * selectAll and paging
     *
     * @param pageNum
     * @param pageSize
     */
    public Page<Shop> findShopsByPage(int pageNum, int pageSize, ShopDto dto) {

        List<Sort.Order> list = new ArrayList<>();
        Sort.Order order1 = new Sort.Order(Sort.Direction.ASC, "id");
        list.add(order1);
        Sort sort = Sort.by(list);
        Pageable pageable = PageRequest.of(pageNum - 1, pageSize, sort);

        Shop shop = Shop.builder().name(dto.getName()).build();

        Page<Shop> page = shopRepository.findShopsByPage(shop, pageable);
        return page;
    }


    private Optional<ShopDto> convert(Optional<Shop> shop) {
        Optional<ShopDto> shopDto = Optional.empty();

        if (shop.isPresent()) {
            Shop shopObj = shop.get();
            shopDto = Optional.of(ShopDto.builder().id(shopObj.getId()).name(shopObj.getName()).address(shopObj.getAddress()).build());
        }

        return shopDto;
    }
}
