package kr.co.ypbooks.nextshop.domain.order.dto;

import lombok.Builder;
import lombok.Getter;

import java.time.LocalDateTime;

@Getter
@Builder
public class DepositOverviewDto {
    /**
     * 예치금 고유 번호
     */
    private Long depositNo;

    /**
     * 회원 코드
     */
    private String memberCd;

    /**
     * 예치금 유형 코드
     */
    private String depositTypeCd;

    /**
     * 예치금 변경 전 금액
     */
    private Long beforeDepositAmt;

    /**
     * 예치금 변경 후 금액
     */
    private Long afterDepositAmt;

    /**
     * 예치금 변경 금액
     */
    private Long depositChgAmt;

    /**
     * 등록 일시
     */
    private LocalDateTime regDate;
}
