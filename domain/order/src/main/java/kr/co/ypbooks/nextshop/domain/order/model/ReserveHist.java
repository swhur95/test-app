package kr.co.ypbooks.nextshop.domain.order.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
@Entity
public class ReserveHist {
    /**
     * 적립금 이력 고유 번호
     */
    @Id
    private Long reserveNo;

    /**
     * 주문 번호
     */
    private Long orderNo;

    /**
     * 회원 코드
     */
    private String memberCd;

    /**
     * 적립금 유형 코드
     */
    private String reserveTypeCd;

    /**
     * 적립금 반영 전 금액
     */
    private Long beforeReserveAmt;

    /**
     * 적립금 반영 후 금액
     */
    private Long afterReserveAmt;

    /**
     * 적립금 반영 금액
     */
    private Long reserveAmt;

    /**
     * 등록 일시
     */
    private LocalDateTime regDate;
}
