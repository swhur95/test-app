package kr.co.ypbooks.nextshop.app.admin.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import kr.co.ypbooks.nextshop.domain.book.dto.TranslatorDto;
import kr.co.ypbooks.nextshop.domain.book.dto.TranslatorSearchDto;
import kr.co.ypbooks.nextshop.domain.book.service.TranslatorService;
import kr.co.ypbooks.nextshop.lib.common.dto.ResultDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@Slf4j
@Api(tags = {"Translator"})
@RequestMapping("/admin/translator")
@RestController
public class TranslatorController {
    private final TranslatorService translatorService;

    public TranslatorController(TranslatorService translatorService) {
        this.translatorService = translatorService;
    }

    @ApiOperation(value = "역자 정보 개요 목록" , notes = "역자 정보 개요 목록을 얻어옵니다.")
    @GetMapping("")
    public ResultDto getTranslatorOverview(@RequestBody TranslatorSearchDto translatorSearchDto) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    @ApiOperation(value = "역자 정보 조회" , notes = "역자 정보를 얻어옵니다.")
    @GetMapping("/{trans_no}")
    public ResultDto getTranslator(@PathVariable("trans_no") Long transNo) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    @ApiOperation(value = "역자 정보 추가" , notes = "역자 정보를 추가합니다.")
    @PostMapping("")
    public ResultDto insertTranslator(@RequestBody TranslatorDto translatorDto) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    @ApiOperation(value = "역자 정보 수정" , notes = "역자 정보를 수정합니다.")
    @PutMapping("/{trans_no}")
    public ResultDto updateTranslator(@PathVariable("trans_no") Long transNo, @RequestBody TranslatorDto translatorDto) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    @ApiOperation(value = "역자 정보 삭제" , notes = "역자 정보를 삭제합니다.")
    @DeleteMapping("/{trans_no}")
    public ResultDto deleteTranslator(@PathVariable("trans_no") Long transNo) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }
}
