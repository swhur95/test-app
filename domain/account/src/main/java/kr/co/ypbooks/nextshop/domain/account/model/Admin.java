package kr.co.ypbooks.nextshop.domain.account.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
@Entity
public class Admin {
    /**
     * 관리자 계정
     */
    @Id
    private String adminId;

    /**
     * 관리자 비밀번호
     */
    private String adminPwd;

    /**
     * 관리자명
     */
    private String adminName;

    /**
     * 메뉴 권한 개수
     */
    private Integer menuAuthCnt;

    /**
     * 패스워드 갱신 일시
     */
    private LocalDateTime pwdUpdDate;

    /**
     * 마지막 접속 일시
     */
    private LocalDateTime lastConnectDate;

    /**
     * 회사 정보
     */
    private String companyInfo;

    /**
     * 등록 일시
     */
    private LocalDateTime regDate;

    /**
     * 수정 일시
     */
    private LocalDateTime updDate;
}
