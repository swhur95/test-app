package kr.co.ypbooks.nextshop.app.shop.service;

import kr.co.ypbooks.nextshop.domain.shop.dto.ShopDto;
import kr.co.ypbooks.nextshop.domain.shop.model.Shop;
import kr.co.ypbooks.nextshop.domain.shop.model.ShopInfo;
import kr.co.ypbooks.nextshop.domain.shop.service.ShopInfoService;
import kr.co.ypbooks.nextshop.domain.shop.service.ShopService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;


@SpringBootTest
class ShopServiceTest {

    @Autowired
    ShopService shopService;

    @Autowired
    ShopInfoService shopInfoService;

    @Test
    @Transactional
    void addShop() {
        double a = Math.random();
        String name = "test" + a;
        String address = "address" + a;
        ShopDto result = shopService.addShop(ShopDto.builder().name(name).address(address).build());
        Assert.notNull(result,"addShop fail");
    }

    @Test
    @Transactional
    void shopList(){
        ShopDto sp = ShopDto.builder().name("a").address("a").build();
        Page<Shop> resultList = shopService.findShopsByPage(1,10,sp);

        Assert.isTrue(resultList.getTotalElements() < 1, "nodate");
        System.out.println(resultList.getTotalElements());
    }

    @Test
    @Transactional
    void shopInfoTest(){

        ShopInfo info = shopInfoService.selectById(1L);

        Assert.isTrue(info.getId() < 0, "nodate");
    }




}