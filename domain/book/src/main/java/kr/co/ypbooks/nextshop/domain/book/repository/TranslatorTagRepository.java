package kr.co.ypbooks.nextshop.domain.book.repository;

import kr.co.ypbooks.nextshop.domain.book.model.TranslatorTag;
import kr.co.ypbooks.nextshop.domain.book.model.TranslatorTagId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TranslatorTagRepository extends JpaRepository<TranslatorTag, TranslatorTagId> {
}
