package kr.co.ypbooks.nextshop.domain.book.repository;

import kr.co.ypbooks.nextshop.domain.book.model.Translator;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TranslatorRepository extends JpaRepository<Translator, Long> {
}
