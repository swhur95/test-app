package kr.co.ypbooks.nextshop.domain.order.dto;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class OrderProductSearchDto {
    /**
     * 주문 상태 조건
     */
    private String orderStateCd;

    /**
     * 상품 타입
     */
    private String productTypeCd;

    /**
     * 상품 코드
     */
    private String productCd;
}
