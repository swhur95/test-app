package kr.co.ypbooks.nextshop.domain.book.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

@ApiModel
@Getter
public class TranslatorSearchDto {
    /**
     * 역자 분야 코드 조건
     */
    @ApiModelProperty(value = "역자 분야 코드 조건", example = "\"010201\"")
    @JsonProperty("trans_category_cd")
    private String transCategoryCd;

    /**
     * 역자 태그 단어 조건
     */
    @ApiModelProperty(value = "역자 태그 단어 조건", example = "신인작가")
    @JsonProperty("trans_tag_word")
    private String transTagWord;

    /**
     * 역자 고유 번호 조건
     */
    @ApiModelProperty(value = "역자 고유 번호 조건", example = "12345678")
    @JsonProperty("trans_no")
    private Long transNo;
}
