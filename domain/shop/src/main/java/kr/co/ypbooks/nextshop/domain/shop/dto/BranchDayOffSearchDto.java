package kr.co.ypbooks.nextshop.domain.shop.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

import java.time.LocalDate;
import java.util.List;

@ApiModel
@Getter
public class BranchDayOffSearchDto {
    /**
     * 휴무일 조건 (시작 일시)
     */
    @ApiModelProperty(value = "휴무일 조건 (시작일)", example = "2022-03-01")
    @JsonProperty("start_day_off_date")
    private LocalDate startDayOffDate;

    /**
     * 휴무일 조건 (종료 일시)
     */
    @ApiModelProperty(value = "휴무일 조건 (종료일)", example = "2022-04-01")
    @JsonProperty("end_day_off_date")
    private LocalDate endDayOffDate;

    /**
     * 요일들 조건
     */
    @ApiModelProperty(value = "휴무일 조건들 (종료일)", example = "2022-04-01")
    @JsonProperty("day_of_week_list")
    private List<Byte> dayOfWeekList;

    /**
     * 지점 코드 조건
     */
    @ApiModelProperty(value = "지점 코드 조건", example = "12345678")
    @JsonProperty("branch_cd")
    private String branchCd;

    /**
     * 지점명 조건
     */
    @ApiModelProperty(value = "지점명 조건", example = "종로")
    @JsonProperty("branch_name")
    private String branchName;

    /**
     * 휴무 사유 조건
     */
    @ApiModelProperty(value = "휴무 사유 조건", example = "연휴")
    @JsonProperty("day_off_reason")
    private String dayOffReason;

    /**
     * 등록한 관리자명 조건
     */
    @ApiModelProperty(value = "등록한 관리자명 조건", example = "관리자")
    @JsonProperty("reg_admin_name")
    private String regAdminName;
}
