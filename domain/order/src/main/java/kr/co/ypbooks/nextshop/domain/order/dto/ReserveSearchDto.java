package kr.co.ypbooks.nextshop.domain.order.dto;

import lombok.Getter;

import java.time.LocalDateTime;

@Getter
public class ReserveSearchDto {
    /**
     * 회원 코드 조건
     */
    private String memberCd;

    /**
     * 등록 일시 조건 (시작)
     */
    private LocalDateTime startRegDate;

    /**
     * 등록 일시 조건 (종료)
     */
    private LocalDateTime endRegDate;
}
