package kr.co.ypbooks.nextshop.domain.account.service;

import kr.co.ypbooks.nextshop.domain.account.dto.AccountDto;
import kr.co.ypbooks.nextshop.domain.account.model.Account;
import kr.co.ypbooks.nextshop.domain.account.repository.AccountRepository;
import kr.co.ypbooks.nextshop.lib.common.operation.RedisOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
public class AccountService {
    private final AccountRepository accountRepository;

    public AccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public Optional<AccountDto> getAccountById(Long id) {
        return Convert(accountRepository.findById(id));
    }

    public Optional<AccountDto> getAccountByName(String name) {
        return Convert(accountRepository.findByName(name));
    }

    @Transactional
    public AccountDto addAccount(String userId, String name, String passwd) {
        Account account = new Account(userId, name, passwd);
        account = accountRepository.save(account);
        return new AccountDto(account.getId(), account.getUserId(), account.getName());
    }

    @Transactional
    public void delAccount(Long id) {
        accountRepository.deleteById(id);
    }

    private Optional<AccountDto> Convert(Optional<Account> account) {
        Optional<AccountDto> accountDto = Optional.empty();

        if (account.isPresent()) {
            Account accountObj = account.get();
            accountDto = Optional.of(new AccountDto(accountObj.getId(), accountObj.getUserId(), accountObj.getName()));
        }

        return accountDto;
    }

    @Autowired
    private RedisOperation redisOperation;

    public AccountDto addAccountMock(Account account) throws Exception {
        String userId = this.generateUserId(account.getCountryCd());
        account.setUserId(userId);
        System.out.println("save account to db...");
        Map<String, Object> map = new HashMap<>();
        map.put("userId", userId);
        map.put("userName", account.getName());
        map.put("countryCd", account.getCountryCd());
        redisOperation.setHashFieldsToValues(userId, map);
        return new AccountDto(null, account.getUserId(), account.getName());
    }

    private String generateUserId(String countryCd) throws Exception {
        return countryCd + InetAddress.getLocalHost().getHostName() + System.currentTimeMillis() + redisOperation.getIncrement("user", "userId");
    }
}
