package kr.co.ypbooks.nextshop.domain.shop.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;

import java.time.LocalDateTime;
import java.util.List;

@ApiModel
@Getter
@Builder
public class DisplayLayoutDto {
    /**
     * 전시 순서
     */
    @ApiModelProperty(value = "전시 순번", example = "1", required = true)
    @JsonProperty("display_seq")
    private Byte displaySeq;

    /**
     * "전시 분류 (01: 인기 검색어, 02: 인기 태그, 03: 분야별 베스트,
     * 04: 분야별 화제의 신간, 05: 오늘의 상품, 06: 테마 상품,
     * 07: MD 추천 상품)"
     */
    @ApiModelProperty(value = "전시 분류 코드", example = "01", required = true)
    @JsonProperty("display_category_cd")
    private String displayCategoryCd;

    /**
     * 기본 전시 제목 (설정하지 않을 경우 사용되는 제목)
     */
    @ApiModelProperty(value = "전시 제목", example = "전시 제목", required = true)
    @JsonProperty("default_title")
    private String defaultTitle;

    /**
     * 전시 상품 개수 (화면에 전시되는 상품 총 개수)
     */
    @ApiModelProperty(value = "전시 상품 개수", example = "3", required = true)
    @JsonProperty("display_product_cnt")
    private Byte displayProductCnt;

    /**
     * layout 서브 정보 (서브 전시 정보가 존재할 경우 사용됨)
     */
    @ApiModelProperty(value = "서브 전시 정보")
    @JsonProperty("layout_sub_info")
    private LayoutSubInfo layoutSubInfo;

    /**
     * 등록 일시
     */
    @ApiModelProperty(value = "등록 일시", example = "2022-03-22 09:00:00", required = true)
    @JsonProperty("reg_date")
    private LocalDateTime regDate;

    /**
     * 수정 일시
     */
    @ApiModelProperty(value = "수정 일시", example = "2022-03-22 09:00:00")
    @JsonProperty("upd_date")
    private LocalDateTime updDate;

    public static class LayoutSubInfo {
        /**
         * 서브 전시 정보들
         */
        @ApiModelProperty(value = "서브 전시 정보들", required = true)
        @JsonProperty("layout_sub_detail_infos")
        private List<LayoutSubDetailInfo> layoutSubInfos;

        private static class LayoutSubDetailInfo {
            /**
             * 서브 표시 순번
             */
            @ApiModelProperty(value = "서브 표시 순번", example = "1", required = true)
            @JsonProperty("display_sub_seq")
            private Byte    displaySubSeq;

            /**
             * 서브 분류 코드 (전시 분류 코드)
             */
            @ApiModelProperty(value = "서브 분류 코드", example = "01", required = true)
            @JsonProperty("display_category_cd")
            private String  displayCategoryCd;

            /**
             * 전시 분할 수
             */
            @ApiModelProperty(value = "전시 분할 수", example = "2", required = true)
            @JsonProperty("display_split_cnt")
            private Byte    displaySplitCnt;

            /**
             * 서브 전시 상품 개수
             */
            @ApiModelProperty(value = "서브 전시 상품 개수", example = "2", required = true)
            @JsonProperty("display_sub_product_cnt")
            private Byte    displaySubProductCnt;
        }
    }


}
