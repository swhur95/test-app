package kr.co.ypbooks.nextshop.domain.account.model.adminlog;

import io.swagger.annotations.ApiModel;
import lombok.*;

import java.time.LocalDateTime;

/**
 * 로그인 로그 정보
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter

@ApiModel(value = "로그 타입 (2: 관리자 로그인)")
public class AdminActionHistLoginLogInfo {
    /**
     * 로그인 일시
     */
    private LocalDateTime loginDate;
    /**
     * 브라우저 정보
     */
    private String userAgent;
    /**
     * 프록시 연결 정보
     */
    private String proxyConnect;
    /**
     * 접속 서버 URL
     */
    private String serverUrl;
    /**
     * 로그인 결과
     */
    private String loginResult;
    /**
     * 로그아웃 일시
     */
    private LocalDateTime logoutDate;
}
