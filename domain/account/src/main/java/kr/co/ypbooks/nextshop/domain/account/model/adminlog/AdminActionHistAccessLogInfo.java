package kr.co.ypbooks.nextshop.domain.account.model.adminlog;

import io.swagger.annotations.ApiModel;
import lombok.*;

/**
 * 관리자 페이지 접근 로그 정보
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@ApiModel(value = "로그 타입 (3: 관리자 페이지 접근)")
public class AdminActionHistAccessLogInfo {
    /**
     * 접속 일자
     */
    private String accessDate;
    /**
     * 접속 서버 URL
     */
    private String serverUrl;
    /**
     * 관리자페이지 접근 URL
     */
    private String accessUrl;
    /**
     * 접근 결과
     */
    private String accessResult;
    /**
     * 체크 유형
     */
    private String checkType;
}
