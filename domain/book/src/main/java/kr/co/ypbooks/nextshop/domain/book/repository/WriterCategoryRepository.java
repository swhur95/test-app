package kr.co.ypbooks.nextshop.domain.book.repository;

import kr.co.ypbooks.nextshop.domain.book.model.WriterCategory;
import kr.co.ypbooks.nextshop.domain.book.model.WriterCategoryId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WriterCategoryRepository extends JpaRepository<WriterCategory, WriterCategoryId> {
}
