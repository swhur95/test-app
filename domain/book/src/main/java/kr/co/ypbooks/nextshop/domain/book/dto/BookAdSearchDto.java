package kr.co.ypbooks.nextshop.domain.book.dto;

import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.Getter;

import java.time.LocalDateTime;

@ApiModel
@Getter
@Builder
public class BookAdSearchDto {
    /**
     * 도서 코드 조건
     */
    private String bookCd;

    /**
     * 시작 일시 조건
     */
    private LocalDateTime startAdDate;

    /**
     * 종료 일시 조건
     */
    private LocalDateTime endAdDate;
}
