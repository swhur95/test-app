package kr.co.ypbooks.nextshop.domain.order.repository;


import kr.co.ypbooks.nextshop.domain.order.model.OrderHist;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderHistRepository extends JpaRepository<OrderHist, Long> {
}
