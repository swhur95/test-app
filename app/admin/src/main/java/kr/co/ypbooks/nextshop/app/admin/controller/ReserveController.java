package kr.co.ypbooks.nextshop.app.admin.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import kr.co.ypbooks.nextshop.lib.common.dto.ResultDto;
import kr.co.ypbooks.nextshop.domain.order.dto.ReserveSearchDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@Slf4j
@Api(tags = {"Reserve"})
@RequestMapping("/admin/reserve")
@RestController
public class ReserveController {
    @ApiOperation(value = "적립금 개요 목록 조회", notes = "적립금 개요 목록 정보를 얻어옵니다.")
    @GetMapping("")
    public ResultDto getDepositOverview(@RequestBody ReserveSearchDto reserveSearchDto) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    @ApiOperation(value = "적립금 정보 조회", notes = "적립금 정보를 얻어옵니다.")
    @GetMapping("/{reserve_no}")
    public ResultDto getDeposit(@PathVariable("reserve_no") Long depositNo) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }
}
