package kr.co.ypbooks.nextshop.app.shop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = {"kr.co.ypbooks.nextshop.domain.order",
        "kr.co.ypbooks.nextshop.domain.shop",
        "kr.co.ypbooks.nextshop.app.shop",
        "kr.co.ypbooks.nextshop.lib.common"})

@EnableJpaRepositories(basePackages = {"kr.co.ypbooks.nextshop.domain.order.repository",
        "kr.co.ypbooks.nextshop.domain.shop.repository"})
@EntityScan(basePackages = {"kr.co.ypbooks.nextshop.domain.order.model",
        "kr.co.ypbooks.nextshop.domain.shop.model"})


public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
