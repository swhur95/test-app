package kr.co.ypbooks.nextshop.domain.product.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
@Entity
public class ProductSeries {
    /**
     * 시리즈 고유 번호
     */
    @Id
    private Long seriesNo;

    /**
     * 시리즈명
     */
    private String seriesName;

    /**
     * 상품 정보
     */
    private String seriesProductInfo;

    /**
     * 시리즈 설명
     */
    private String desc;

    /**
     * 등록 일시
     */
    private LocalDateTime regDate;
}
