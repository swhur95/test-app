package kr.co.ypbooks.nextshop.domain.account.model;

import com.vladmihalcea.hibernate.type.json.JsonStringType;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@Entity

@Table(name = "member")
@DynamicUpdate
@TypeDef(name = "json", typeClass = JsonStringType.class)
public class Member{

    /**
     * 회원번호
     */
    @Id
    private String memberCd;

    /**
     * 아이디
     */
    private String memberId;

    /**
     * 비밀번호
     */
    private String memberPwd;

    /**
     * 등록/가입일자
     */
    private LocalDateTime regDate;

    /**
     * 수정일자
     */
    private LocalDateTime updDate;

    /**
     * 회원구분(북클럽:1,계열사:2)
     */
    private String memberType;

    /**
     * 카드발행여부(미발행:0,발행:1)
     */
    private String cardFlag;

    /**
     * 회원등급(A:일반,B:골드,C:플래티넘)
     */
    private String memberLevel;

    /**
     * 마일리지 적립여부(미적립:0,적립:1)
     */
    private String mileageFlag;

    /**
     * 인터넷인증 여부 (온라인 인증 여부)
     */
    private String onlineCertFlag;

    /**
     * 회원 상태(0:탈퇴,1:일반,4:휴먼,null:오프라인회원)
     */
    private String memberState;

    /**
     * 회원 가입구분
     */
    private String memberRegFlag;

    /**
     * 법인회원구분(0:일반,1:법인)
     */
    private String legalFlag;

    /**
     * 인증번호
     */
    private String confirmNo;

    /**
     * dm 발송구분(1:동의,0:미동의)
     */
    private String dmFlag;

    /**
     * 개인 정보	JSON
     */
    @Type(type = "json")
    @Column(columnDefinition = "json")
    private MemberPersonalInfo personalInfo;

    /**
     * 회사 정보	JSON
     */
    @Type(type = "json")
    @Column(columnDefinition = "json")
    private MemberCompanyInfo companyInfo;

    /**
     * 활동 이력 정보	JSON
     */
    @Type(type = "json")
    @Column(columnDefinition = "json")
    private MemberActHistInfo actHistInfo;

    /**
     * 추가 정보	JSON
     */
    @Type(type = "json")
    @Column(columnDefinition = "json")
    private MemberAddInfo addInfo;

}
