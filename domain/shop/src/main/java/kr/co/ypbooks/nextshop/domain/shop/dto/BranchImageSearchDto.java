package kr.co.ypbooks.nextshop.domain.shop.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

import java.time.LocalDate;

@ApiModel
@Getter
public class BranchImageSearchDto {
    /**
     * 지점 코드 조건
     */
    @ApiModelProperty(value = "지점 코드 조건", example = "12345678")
    @JsonProperty("branch_cd")
    private String branchCd;

    /**
     * 지점명 조건
     */
    @ApiModelProperty(value = "지점명 조건", example = "종로")
    @JsonProperty("branch_name")
    private String branchName;

    /**
     * 지점 이미지 내용 조건
     */
    @ApiModelProperty(value = "지점 이미지 내용 조건", example = "내부")
    @JsonProperty("branch_img_content")
    private String branchImgContent;

    /**
     * 등록 관리자명 조건
     */
    @ApiModelProperty(value = "등록 관리자명 조건", example = "관리자")
    @JsonProperty("reg_admin_name")
    private String regAdminName;

    /**
     * 등록 일시 조건 (시작 일시)
     */
    @ApiModelProperty(value = "등록일 조건 (시작일)", example = "2022-03-01 09:00:00")
    @JsonProperty("start_reg_date")
    private LocalDate startRegDate;

    /**
     * 등록 일시 조건 (종료 일시)
     */
    @ApiModelProperty(value = "등록일 조건 (종료일)", example = "2022-04-01 09:00:00")
    @JsonProperty("end_reg_date")
    private LocalDate endRegDate;
}
