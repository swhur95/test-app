package kr.co.ypbooks.nextshop.domain.account.model;

import lombok.*;

/**
 * ADD_INFO(추가 정보)
 */

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class MemberAddInfo {

    /**
     *계열사 이름
     */
    private String afName;
    /**
     *계열사 부서명
     */
    private String afPostName;
    /**
     *계열사 직책명
     */
    private String afRespName;
    /**
     *계열사 사번
     */
    private String employNo;
    /**
     *지점코드
     */
    private String branchCd;
    /**
     *법인담당자
     */
    private String legalName;
    /**
     *사업자번호
     */
    private String legalNo;
}
