package kr.co.ypbooks.nextshop.domain.account.repository;

import kr.co.ypbooks.nextshop.domain.account.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, Long> {
    Optional<Account> findByName(String name);
}
