package kr.co.ypbooks.nextshop.app.admin.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import kr.co.ypbooks.nextshop.domain.account.dto.AdminDto;
import kr.co.ypbooks.nextshop.domain.account.dto.AdminLoginDto;
import kr.co.ypbooks.nextshop.domain.account.dto.AdminPasswordSearchDto;
import kr.co.ypbooks.nextshop.lib.common.dto.ResultDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@Slf4j
@Api(tags = {"Account"})
@RequestMapping("/admin/account")
@RestController
public class AccountController {
    @ApiOperation(value = "관리자 정보 등록 요청" , notes = "관리자 정보를 등록합니다.")
    @PostMapping("")
    public ResultDto insertAdmin(@RequestBody AdminDto adminDto) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    @ApiOperation(value = "관리자 정보 수정 요청" , notes = "관리자 정보를 수정합니다.")
    @PutMapping("")
    public ResultDto updateAdmin(@RequestBody AdminDto adminDto) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    @ApiOperation(value = "관리자 로그인" , notes = "관리자 로그인 진행합니다.")
    @PostMapping("/login")
    public ResultDto procAdminLogin(@RequestBody AdminLoginDto adminLoginDto) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    @ApiOperation(value = "관리자 로그아웃" , notes = "관리자 로그아웃을 진행합니다.")
    @DeleteMapping("/logout")
    public ResultDto procAdminLogout(@RequestBody AdminLoginDto adminLoginDto) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    @ApiOperation(value = "관리자 패스워드 찾기" , notes = "관리자 패스워드 찾기를 진행합니다.")
    @PostMapping("/password")
    public ResultDto procAdminSearchPassword(@RequestBody AdminPasswordSearchDto adminPasswordSearchDto) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }

    @ApiOperation(value = "관리자 중복 체크" , notes = "관리자 ID 중복 체크")
    @GetMapping("/property")
    public ResultDto checkAdminId(@RequestParam("admin_id") String adminId) {
        ResultDto result = new ResultDto();
        result.setResult("SUCCESS");
        result.setResultMessage("성공");

        return result;
    }
}
