package kr.co.ypbooks.nextshop.domain.product.repository;

import kr.co.ypbooks.nextshop.domain.product.model.ProductAdList;
import kr.co.ypbooks.nextshop.domain.product.model.ProductId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductAdListRepository extends JpaRepository<ProductAdList, ProductId> {
}
