package kr.co.ypbooks.nextshop.app.login.controller;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import kr.co.ypbooks.nextshop.domain.account.dto.AccountDto;
import kr.co.ypbooks.nextshop.domain.account.dto.Auth;
import kr.co.ypbooks.nextshop.domain.account.oauth.conf.AuthConfig;
import kr.co.ypbooks.nextshop.domain.account.oauth.conf.AuthDefaultSource;
import kr.co.ypbooks.nextshop.domain.account.oauth.model.AuthCallback;
import kr.co.ypbooks.nextshop.domain.account.oauth.request.AuthAppleRequest;
import kr.co.ypbooks.nextshop.domain.account.oauth.request.AuthKakaoRequest;
import kr.co.ypbooks.nextshop.domain.account.oauth.request.AuthNaverRequest;
import kr.co.ypbooks.nextshop.domain.account.service.AccountService;
import kr.co.ypbooks.nextshop.domain.account.service.AppleService;
import kr.co.ypbooks.nextshop.domain.account.service.GoogleService;
import kr.co.ypbooks.nextshop.lib.common.operation.RedisOperation;
import kr.co.ypbooks.nextshop.lib.common.property.JwtProperty;
import kr.co.ypbooks.nextshop.lib.common.util.JwtUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@Slf4j
@RequestMapping("/account")
@RestController
public class LoginController {
    private final AccountService accountService;

    public LoginController(AccountService accountService) {
        this.accountService = accountService;
    }

    @Autowired
    private GoogleService googleService;
    @Autowired
    private AppleService appleService;
    @Autowired
    private JwtProperty jwtProperty;
    @Autowired
    private RedisOperation redisOperation;

    @PostMapping("/add")
    public AccountDto addAccount(@RequestBody AccountParam param) {
        return accountService.addAccount(param.getUserId(), param.getName(), param.getPasswd());
    }

    // ============ googleAuth ============>>>>>>>

    @ApiOperation(value = "googleAuth", notes = "googleAuth")
    @GetMapping(value = "/googleAuth")
    public String googleAuth() {
        System.out.println("================googleAuth=============");
        String authorizingUrl = "";
        try {
            authorizingUrl = googleService.authorizingUrl();
            return authorizingUrl;
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("===authorizingUrl=====>" + authorizingUrl);
        return authorizingUrl;
    }

    @ApiOperation(value = "googleAuthorizing", notes = "googleAuthorizing")
    @ApiImplicitParam(name = "code", paramType = "query", required = true, dataType = "String")
    @GetMapping(value = "/googleAuthorizing")
    public Auth authorizing(String code) {
        System.out.println("================googleAuthorizing=============");
        Auth auth = null;
        try {
            auth = googleService.authorizing(code);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return auth;
    }

    // <<<<<<<============ googleAuth ============


    // ============ naverAuth ============>>>>>>>

    @Value("${auth.naver.cas.redirect_uri}")
    private String loginCallbackUrl;
    @Value("${auth.naver.cas.client_id}")
    private String clientId;
    @Value("${auth.naver.cas.client_secret}")
    private String clientSecret;

    @GetMapping(value = "/naverAuth")
    public ModelAndView naverAuth() {
        System.out.println("================naverAuth=============");
        AuthNaverRequest request = new AuthNaverRequest(AuthConfig.builder()
                .redirectUri(loginCallbackUrl)
                .clientId(clientId)
                .clientSecret(clientSecret)
                .build(),
                AuthDefaultSource.NAVER);
        System.out.println("================RedirectView=============>" + request.authorize(null));
        return new ModelAndView(new RedirectView(request.authorize(null)));
    }

    @RequestMapping(value = "/naverCallback", method = RequestMethod.PUT)
    public Auth naverCallback(String code, String state, String error, String error_description) {
        System.out.println("================naverCallback=============");
        log.warn("code: " + code);
        log.warn("state: " + state);
        log.warn("error: " + error);
        log.warn("error_description: " + error_description);
        Auth auth = null;
        try {
            if (StringUtils.isNotEmpty(error)) {
                auth.setError(error);
                auth.setError_description(error_description);
            } else {
                AuthNaverRequest request = new AuthNaverRequest(AuthConfig.builder()
                        .redirectUri(loginCallbackUrl)
                        .clientId(clientId)
                        .clientSecret(clientSecret)
                        .build(),
                        AuthDefaultSource.NAVER);
                Map<String, Object> tokenMap = request.getFirstRequestToken(AuthCallback.builder().code(code).state(state).build());

                log.warn("tokenMap: " + tokenMap);
                log.warn("tokenMap: " + tokenMap.get("response"));

            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        return auth;
    }

    // <<<<<<<============ naverAuth ============


    // ============ kakaoAuth ============>>>>>>>

    @Value("${auth.kakao.cas.redirect_uri}")
    private String kakao_loginCallbackUrl;
    @Value("${auth.kakao.cas.client_id}")
    private String kakao_clientId;
    @Value("${auth.kakao.cas.client_secret}")
    private String kakao_clientSecret;

    @GetMapping(value = "/kakaoAuth")
    public ModelAndView kakaoAuth() {
        log.warn("================kakaoAuth=============");
        AuthKakaoRequest request = new AuthKakaoRequest(AuthConfig.builder()
                .redirectUri(kakao_loginCallbackUrl)
                .clientId(kakao_clientId)
                .clientSecret(kakao_clientSecret)
                .build(),
                AuthDefaultSource.KAKAO);
        log.warn("================RedirectView=============>" + request.authorize(null));
        return new ModelAndView(new RedirectView(request.authorize(null)));
    }

    @RequestMapping(value = "/kakaoCallback", method = RequestMethod.PUT)
    public Auth kakaoCallback(String code, String state, String error, String error_description) {
        log.warn("================kakaoCallback=============");
        log.warn("code: " + code);
        log.warn("state: " + state);
        log.warn("error: " + error);
        log.warn("error_description: " + error_description);
        Auth auth = null;
        try {
            if (StringUtils.isNotEmpty(error)) {
                auth.setError(error);
                auth.setError_description(error_description);
            } else {
                AuthKakaoRequest request = new AuthKakaoRequest(AuthConfig.builder()
                        .redirectUri(kakao_loginCallbackUrl)
                        .clientId(kakao_clientId)
                        .clientSecret(kakao_clientSecret)
                        .build(),
                        AuthDefaultSource.KAKAO);
                Map<String, Object> tokenMap = request.getFirstRequestToken(AuthCallback.builder().code(code).state(state).build());

                log.warn("tokenMap: " + tokenMap);
                log.warn("tokenMap: " + tokenMap.get("response"));

            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        return auth;
    }

    // <<<<<<<============ kakaoAuth ============


    // ============ AppleAuth ============>>>>>>>

    @Value("${auth.apple.cas.redirect_uri}")
    private String apple_loginCallbackUrl;
    @Value("${auth.apple.cas.client_id}")
    private String apple_clientId;
    @Value("${auth.apple.cas.client_secret}")
    private String apple_clientSecret;
    @Value("${auth.apple.cas.team_id}")
    private String apple_teamId;
    @Value("${auth.apple.cas.key_id}")
    private String apple_keyId;

    @GetMapping(value = "/appleAuth")
    public ModelAndView appleAuth() {
        log.warn("================appleAuth=============");
        AuthAppleRequest request = new AuthAppleRequest(AuthConfig.builder()
                .redirectUri(apple_loginCallbackUrl)
                .clientId(apple_clientId)
                .clientSecret(apple_clientSecret)
                .build(),
                AuthDefaultSource.APPLE);
        log.warn("================RedirectView=============>" + request.authorize(null));
        return new ModelAndView(new RedirectView(request.authorize(null)));
    }

    @RequestMapping(value = "/appleCallback", method = RequestMethod.PUT)
    public Auth appleCallback(String code, String id_token, String state, String error, String user) {
        log.warn("================appleCallback=============");
        log.warn("code: {}, id_token: {} , state: {}" , code , id_token , state);
        log.warn("error: " + error);
        log.warn("user: " + user);
        Auth auth = null;
        try {
            if (StringUtils.isNotEmpty(error)) {
                auth.setError(error);
            } else {
                AuthAppleRequest request = new AuthAppleRequest(AuthConfig.builder()
                        .redirectUri(apple_loginCallbackUrl)
                        .clientId(apple_clientId)
                        .clientSecret(apple_clientSecret)
                        .build(),
                        AuthDefaultSource.APPLE);
                Map<String, Object> tokenMap = request.getFirstRequestToken(AuthCallback.builder()
                        .code(code).state(state)
                        .keyId(apple_keyId).teamId(apple_teamId).secretCode(apple_clientSecret).clientId(apple_clientId)
                        .build());

                log.warn("tokenMap: " + tokenMap);
                log.warn("tokenMap: " + tokenMap.get("response"));

            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        return auth;
    }

    // <<<<<<<============ AppleAuth ============

    /**
     * Access token
     * @param request get clentId
     * @param response set Authorization
     */
    @GetMapping(value = "/accessToken")
    @ApiOperation(value = "accessToken", notes = "accessToken")
    public void accessToken(HttpServletRequest request, HttpServletResponse response) {
        log.debug("================accessToken=============");
        if (request.getHeader("clientId") != null) {
            String token = JwtUtil.generateToken(null, 0, request, jwtProperty);
            log.debug("create token: ", token);
            if (token != null && redisOperation.set(token, "30", 1800)) {
                response.setHeader("Authorization", token);
            }
        }
    }

    /**
     * Refresh token
     * @param request get clentId and Authorization
     * @param response set Authorization
     */
    @PostMapping(value = "/refreshToken")
    @ApiOperation(value = "refreshToken", notes = "refreshToken")
    public void refreshToken(HttpServletRequest request, HttpServletResponse response) {
        log.debug("================refreshToken=============");
        String oldToken = request.getHeader("Authorization");
        if (request.getHeader("clientId") != null && oldToken != null) {
            Boolean isCertifiedToken = (Boolean) JwtUtil.parseToken(oldToken, request, jwtProperty).get("isCertifiedToken");
            if (isCertifiedToken) {
                log.debug("================oldToken is uncertified=============");
                String newToken = JwtUtil.generateToken(null, 0, request, jwtProperty);
                if (newToken != null && redisOperation.set(newToken, "30", 1800)) {
                    response.setHeader("Authorization", newToken);
                    redisOperation.del(oldToken);
                }
            }
        }
    }
}
