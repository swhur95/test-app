package kr.co.ypbooks.nextshop.domain.book.repository;

import kr.co.ypbooks.nextshop.domain.book.model.WriterTag;
import kr.co.ypbooks.nextshop.domain.book.model.WriterTagId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WriterTagRepository extends JpaRepository<WriterTag, WriterTagId> {
}
