package kr.co.ypbooks.nextshop.domain.order.repository;

import kr.co.ypbooks.nextshop.domain.order.model.OrderProductHist;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderProductHistRepository extends JpaRepository<OrderProductHist, Long> {
}
