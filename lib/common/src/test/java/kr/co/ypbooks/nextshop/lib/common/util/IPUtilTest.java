package kr.co.ypbooks.nextshop.lib.common.util;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@ActiveProfiles("alpha")
public class IPUtilTest {

    @Test
    public void getClientIp() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        String ip = IPUtil.getClientIp(request);
        assertNotNull(ip);
    }
}
